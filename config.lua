require "functions"

Languages={{"en","es","fr"},{"es","en","fr"},{"ru","en","es","fr"},{"fr","en","es"},{"ar","en","es","fr"},{"id","en","es","fr"}}

RepoDir = RepoDir or os.getenv('PWD')
Host = "127.0.0.1"
Port = 80
ServerAdmin = "webmaster@localhost"
ServerName = "localhost"
ServerAlias = "localhost"
ProxyModule = true -- set to false if you would like to use rewrite rules
ProxyURL = "http://iridl.ldeo.columbia.edu"
MacSetup = false

if ProxyModule then
ProxyRules = [[
   ProxyPass /SOURCES ]]..ProxyURL..[[/SOURCES nocanon
   ProxyPass /expert ]]..ProxyURL..[[/expert nocanon
   ProxyPass /ds: ]]..ProxyURL..[[/ds: nocanon
   ProxyPass /home ]]..ProxyURL..[[/home nocanon
   ProxyPass /openrdf-sesame ]]..ProxyURL..[[/openrdf-sesame nocanon
   ProxyPass /servicestatus ]]..ProxyURL..[[/servicestatus nocanon
]]
else
ProxyRules = [[
   RewriteRule ^/SOURCES/(.*) ]]..ProxyURL..[[/SOURCES/$1
   RewriteRule ^/expert/(.*) ]]..ProxyURL..[[/expert/$1
   RewriteRule ^/ds:/(.*) ]]..ProxyURL..[[/ds:/$1
   RewriteRule ^/home/(.*) ]]..ProxyURL..[[/home/$1
   RewriteRule ^/openrdf-sesame/(.*) ]]..ProxyURL..[[/openrdf-sesame/$1
   RewriteRule ^/servicestatus/ ]]..ProxyURL..[[/servicestatus/
]]
end

--[[ This starts the section of parameters for the Tutorials.--]]

--[[Parameters for Tutoria/Datasets/Location.--]]

--[[ Data by Source --]]
LocationText1 = "the National Oceanic and Atmospheric Administration (NOAA)"
LocationText2 = "NOAA"
LocationText3 = "the National Oceanic Data Center (NODC), National Geophysical Data Center (NGDC), National Centers for Environmental Prediction (NCEP), NCEP/NCAR Reanalysis Project (NCEP-NCAR), and the National Climatic Data Center (NCDC)"
LocationText4 = "NCEP CPC GSOD MONTHLY"
LocationText5 = "Select the <i>NCEP</i> link.<br />Select the <i>CPC</i> link.<br />Select the <i>GSOD</i> link.<br />Select the <i>MONTHLY</i> link."
LocationText8 = "station"
LocationHref1 = "/SOURCES/.NOAA/.NCEP/.CPC/.GSOD/.MONTHLY/"
--[[ Data By Searching --]]
LocationText9 = "NOAA NCDC DAILY FSOD TMAX"
LocationHref2 = "/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/.TMAX/"
LocationText10 = "FSOD"
LocationHref3 = "/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/"
LocationText11 = "NOAA NCDC DAILY FSOD"

--[[Parameters for Tutoria/Datasets/Structure.--]]

StructureText1 = "NOAA NCDC DAILY FSOD"
StructureHref1 = "/SOURCES/.NOAA/.NCDC/.DAILY/.FSOD/"
StructureText2 = "FSOD"
--[[NB: /localconfig/figures/StructureFig2.png need be replaced.--]]
StructureText3 = "NCDC"
StructureText4 = "daily NCDC"
StructureText5 = "DAILY"
StructureText6 = "pressure"
StructureText7 = "each station"
StructureText8 = "julian days"
StructureText9 = "1 Jan 1869"
StructureText10 = "31 Dec 1999"
StructureText11 = "47846"
StructureHref2 = "/SOURCES/.NOAA/.NCEP/.CPC/.GLOBAL/.daily/"
StructureText12 = "NOAA NCEP CPC GLOBAL daily"

--[[Parameters for Tutorial/Selection/Station_Regions--]]

--[[ Gridded Data --]]
Stations_RegionsHref1 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.climatology/"
Stations_RegionsText1 = "NOAA NCEP EMC CMB GLOBAL Reyn_SmithOIv1 climatology"
Stations_RegionsText2 = "120°E, 25°N"
Stations_RegionsText3 = "climatological SST"
Stations_RegionsText4 = "Sea Surface Temperature"
Stations_RegionsHref2 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.climatology/.sst/"
Stations_RegionsText5 = "120E"
Stations_RegionsText6 = "25N"
Stations_RegionsHref3 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.climatology/.sst/"
Stations_RegionsText7 = "in the Formosa Strait"
Stations_RegionsText8 = "Reyn_Smith climatology"
Stations_RegionsHref4 = "/expert/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.climatology/"
Stations_RegionsText9 = ".sst"
Stations_RegionsHref5 = "/expert/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.climatology/.sst/"
Stations_RegionsText10 = "70°-20°W, 10°S-10°N"
Stations_RegionsText11 = "10N"
Stations_RegionsText12 = "10S"
Stations_RegionsText13 = "70W"
Stations_RegionsText14 = "20W"
Stations_RegionsText15 = "the Atlantic Ocean off of northeastern South America"
Stations_RegionsHref6 = "/.sst/"
Stations_RegionsText16 = "10"
Stations_RegionsText17 = "-10"
Stations_RegionsText18 = "-70"
Stations_RegionsText19 = "-20"

--[[Parameters for Tutorial/Selection/Data_Variables--]]

--[[ Single --]]
Data_VariablesText1 = "GLOBALSOD"
Data_VariablesHref1 = "/SOURCES/.NOAA/.NCDC/.DAILY/.GLOBALSOD/"
Data_VariablesText2 = "maximum wind gust"
Data_VariablesText3 = "Select the <i>maximum</i> link under the Datasets and variables heading.<br />Select the <i>gust</i> link under the Datasets and variables heading.<br />Select the <i>Wind Speed</i> link under the Datasets and variables heading."
Data_VariablesText4 = "maximum gust Wind Speed"
Data_VariablesHref2 = ".maximum/.gust/.wspeed/"
Data_VariablesText6 = ".maximum .gust .wspeed"
Data_VariablesHref3 = "/expert"
--[[ Multiple --]]
Data_VariablesText7 = "Reyn_Smith"
Data_VariablesHref4 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/"
Data_VariablesText8 = "monthly climatological SST"
Data_VariablesText9 = "monthly observed SST"
Data_VariablesHref5 = ".climatology/.sst/"
Data_VariablesHref6 = ".monthly/.sst"
Data_VariablesText10 = "SOURCES .NOAA .NCEP .EMC .CMB .GLOBAL .Reyn_SmithOIv1 .climatology .sst"

--[[Parameters for Tutorial/Selection/Time_Period--]]

--[[ Continuous --]]
Time_PeriodHref1 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/.weekly/"
Time_PeriodText1 = "Reyn_Smith weekly"
Time_PeriodText2 = "November 17, 1982 - December 29, 2001"
Time_PeriodText3 = "SST"
Time_PeriodHref2 = ".sst/"
Time_PeriodText4 = "17 Nov 1982 to 29 Dec 2001"
Time_PeriodHref3 = "17+Nov+1982+to+29+Dec+2001"
Time_PeriodText5 = "Note that the limits of the time grid in the Data Selection box match the closest Wednesday to your selected time limits as weekly data are stored on the date of the Wednesday of each week."
Time_PeriodHref4 = "%2817%20Nov%201982%29%2829%20Dec%202001%29RANGE/"
Time_PeriodText6 = ".sst"
Time_PeriodText7 = "T (17 Nov 1982) (29 Dec 2001) RANGE"
--[[ Discontinuous --]]
Time_PeriodHref5 = "/SOURCES/.NOAA/.NCDC/.DAILY/.GLOBALSOD/"
Time_PeriodText8 = "GLOBALSOD"
Time_PeriodHref6 = "IWMO+466960+VALUE/.mean/.temp/"

--[[Parameters for Tutorial/MVD/Manipulation--]]

--[[ Arithmetic --]]
MVDparameterText1 = "F"
MVDparameterText2 = "Bismarck, ND"
MVDparameterHref1 = "/SOURCES/.NOAA/.NCDC/.DAILY/.GLOBALSOD/"
MVDparameterText3 = "GLOBALSOD"
MVDparameterText4 = "the station in"
MVDparameterHref2 = "IWMO+727640+VALUE/"
MVDparameterHref3 = ".minimum/.temp/"
MVDparameterText5 = "SST"
MVDparameterHref4 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/"
MVDparameterText6 = "Reyn_Smith"
MVDparameterText7b = " (months since 01-Jan) periodic Jan to Dec by 1. N= 12 pts"
MVDparameterText7 = " (months since 1960-01-01) ordered Nov 1981 to Mar 2003 by 1. N=257 pts"
MVDparameterText8 = "SSTA"
MVDparameterHref5 = ".climatology/.sst"
MVDparameterHref6 = ".monthly/.ssta/"
MVDparameterText9 = "150°W, 15°N"
MVDparameterText10 = "15N"
MVDparameterText11 = "150W"
MVDparameterHref7 = ".monthly/.sst"
MVDparameterText12 = "mean sea level pressure"
MVDparameterText13 = "mb"
MVDparameterText14 = "Pa"
MVDparameterText15 = "100"
MVDparameterHref8 = ".mean/.sea_level/.pressure/"
MVDparameterText16 = "inches"
MVDparameterText17 = "cm"
MVDparameterText18 = "2.54"
MVDparameterHref4b = "/SOURCES/.NOAA/.NCDC/.DAILY/.GLOBALSOD/"
MVDparameterText6b = "GLOBALSOD"
MVDparameterHref7b = ".prcp/"
--[[ Limits --]]
MVDparameterText19 = "0"
MVDparameterText20 = "Fahrenheit"
MVDparameterText21 = "32."
MVDparameterText22 = "station (Vienna, WMO ID:110360)"
MVDparameterHref9 = "IWMO/%28110360%29VALUE/"
MVDparameterText23 = "100."
MVDparameterHref10 = ".maximum/.temp/"
MVDparameterText24 = "station (Damascus, WMO ID: 400800)"
MVDparameterHref11 = "IWMO/%28400800%29VALUES/"
MVDparameterText25 = "June 15"
MVDparameterText26 = "snow depth"
MVDparameterText27 = "1"
MVDparameterText28 = "meter"
MVDparameterHref12 = ".snow/"
MVDparameterText29 = "inches"
MVDparameterText30 = "39.4"
MVDparameterText31 = "station (Pellston, MI, WMO ID: 727347)"
MVDparameterHref13 = "IWMO/727347/VALUE/"
--[[ Averages --]]
MVDparameterText32 = "the Gulf of Mexico defined by 83°-97°W, 21°-30°N"
MVDparameterHref14 = "Y/%2821N%29%2830N%29RANGE/X/%2897W%29%2883W%29RANGE/"
MVDparameterText41 = "precipitation"
MVDparameterHref3b = ".prcp/"
MVDparameterText33 = "Jan 8, 1994 - Dec 25, 1999"
MVDparameterText34 = "Jan 15, 1994 - Dec 18, 1999"
MVDparameterText35 = "station (Barcelona, Spain, WMO ID: 81810)"
MVDparameterHref15 = "IWMO+81810+VALUE/"
--[[ Statistical --]]
MVDparameterText36 = "Chimbote, Peru"
MVDparameterText8c = "SSTA"
MVDparameterText37 = "NOAA NCDC GHCN v2beta"
MVDparameterHref16 = "/SOURCES/.NOAA/.NCDC/.GHCN/.v2beta/"
MVDparameterText38 = "Chimbote, Peru (WMO ID: 84531000)"
MVDparameterHref17 = "IWMO+84531000+VALUE/"
MVDparameterHref18 = ".prcp/"
MVDparameterText39 = "SOURCES .NOAA .NCEP .EMC .CMB .GLOBAL .Reyn_SmithOIv1 .monthly .ssta"
MVDparameterHref4c = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/"
MVDparameterHref6c = ".monthly/.ssta/"
MVDparameterText40 = "weekly SST"
MVDparameterHref19 = ".weekly/.sst/"

--[[Parameters for Tutorial/MVD/Visualization--]]

--[[ Color Map --]]
VisualizationHref1 = "/SOURCES/.NOAA/.NCEP/.EMC/.CMB/.GLOBAL/.Reyn_SmithOIv1/"
VisualizationText1 = "Reyn_Smith"
VisualizationHref2 = ".monthly/.ssta/"
--[[ Contour Map --]]
VisualizationText2 = "February"
VisualizationText3 = "surface potential temperature"
VisualizationText4 = "We also introduce a new dataset, Levitus94, for this example. The Levitus dataset contains gridded oceanic data on the monthly, seasonal, and annual time scales."
VisualizationText5 = "Levitus94"
VisualizationHref3 = ".LEVITUS94/"
VisualizationText6 = "<br />Select the data for the surface (Z=0)."
VisualizationHref4 = ".MONTHLY/.theta/T/%28Feb%29VALUES/Z/%280%29VALUE/"
VisualizationText7 = "land"
VisualizationText8 = "10"
VisualizationText8b = "˚C"
--[[ 1D plots --]]
VisualizationText9 = "MONTHLY mixed layer depth"
VisualizationText10 = "80°E, 20°S"
VisualizationHref5 = ".MONTHLY/.Zmix/"
VisualizationHref6 = "Y/%2820S%29VALUES/X/%2880E%29VALUES/"
VisualizationText11 = "olr_anomaly"
VisualizationHref7 = "/SOURCES/.Indices/.india/.rainfall/dup/mean/sub/SOURCES/.KAPLAN/.Indices/.NINO3/.avOS/T/-1236.5/383.5/12/RANGESTEP/startcolormap/-3/3/RANGEEDGES/white/black/navy/-3/value/blue/-2.4/bandmax/DeepSkyBlue/-1.8/bandmax/aquamarine/-0.9/bandmax/SeaGreen/-0.5/bandmax/PaleGreen/dup/0/bandmax/moccasin/dup/0.5/bandmax/yellow/DarkOrange/1./bandmax/red/2.1/bandmax/DarkRed/3./bandmax/brown/endcolormap/T/-4.5/shiftdata/T/fig-/colorbars2/-fig//plotaxislength/432/psdef//plotborder/72/psdef/#expert"
VisualizationText12 = "summer monsoon rainfall and the color of the bars indicates SSTA"
VisualizationHref8 = "/SOURCES/.NOAA/.NCDC/.GCPS/.MONTHLY/.STATION/a%3A/.lon/%3Aa%3A/.lat/%3Aa%3A/.mean/.prcp/%3Aa/T/(Dec 1901)/(Mar 1994)/RANGE/T/4/boxAverage/T/12/STEP/SOURCES/.KAPLAN/.Indices/.NINO3/.avOS/T/(Dec 1856)/(Dec 1991)/12/RANGESTEP/T//pointwidth/4/def/1.5/shiftGRID/[T]correlate/fig-/scattercolor/coasts/-fig/#expert"
VisualizationText13 = "latitude and longitude to scatter the data points while the color of each point represents the correlation of precipitation and Nino3 indices"
--[[ Vectors plots --]]
--[[ Cross Section plots --]]
--[[ Animation --]]
VisualizationText14 = "weekly"
VisualizationHref9 = ".weekly/.sst/"
--[[ Colorscale --]]
VisualizationText15 = "these same"
VisualizationText16 = "CAMS"
VisualizationHref10 = ".NOAA/.NCEP/.CPC/.CAMS/.anomaly/.temp/"
--[[ Land Mask --]]
--[[ MJO --]]

--[[Parameters for Tutorial/MVD/Download--]]
DownloadText1 = "Reanalysis"
DownloadHref1 = ".NOAA/.NCEP-NCAR/.CDAS-1/.mc6190/.Intrinsic/.MSL/.pressure/"
DownloadText2 = "climatological mean sea level pressure Reanalysis"
DownloadText3 = "504,576"
