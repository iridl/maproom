<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="en">
 
  <head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0;"/>
    <meta xml:lang="" property="maproom:Entry_Id" content="Climatology_Monitoring"/>
    <meta xml:lang="" property="maproom:Sort_Id" content="a3"/>

    <title>Climate Monitoring</title>
    
    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
    <link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
    <link rel="stylesheet" type="text/css" href="/maproom/ACToday/actoday.css" />

    <link class="share" rel="canonical" href="index.html" />
    <link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
    <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
    
    <!--     Other Language -->
    <link class="altLanguage" rel="alternate" hreflang="vi" href="index.html?Set-Language=vi" />
    
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rainfall"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#rainfall_rate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Climate_Indices"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standardized"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#StandardizedRainfallIndex"/>
    
    <!-- <link rel="icon" href="/localconfig/icons/Logo_NCHMF.png" sizes="any" type="png"/> -->
    
    <link rel="term:icon" href="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/%28rainfall%29//monitAna/parameter/%28rainfall%29/eq/%7B.rainfall/.rfe_merged%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/%28Cumulative%20Rainfall%29/def/DATA/0/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/%28Cumulative%20Rainfall%20Anomaly%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%2821-31%20Jul%202019%29/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/%28Cumulative%20Rainfall%20Anomaly%20in%20%25%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B.rainfall/.rfe_mergeddiff%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B.rainfall/.rfe_mergeddiff_percent//long_name/%28Rainfall%20Anomaly%20Expressed%20as%20%25%20of%20Long-Term%20Average%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B.rainfall/.SPI-rfe_merged_1-dekad/DATA/-3.0/3.0/RANGE%7Dif//name/%28monitAna%29/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/102/110.5/RANGE/Y/8.5/24/RANGE/1/index/SOURCES/.Features/.Political/.Vietnam/.Provinces/.the_geom//name//Provinces/def/X/Y/fig-/colors/colors/||/colors/grey/thinnish/solid/stroke/black/thinnish/solid/countries_gaz/-fig//monitAna/get_parameter/%28rainfall%29/eq/%7B//framelabelstart/%28Rainfall%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20in%20Percent%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20in%20Percent%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B//framelabelstart/%28Standard%20Rainfall%20Index%201-dekad%20on%20%29/psdef%7Dif//plotaxislength/500/psdef//antialias/true/psdef//T/last/plotvalue//layers%5B//monitAna//countries_gaz//Provinces%5Dpsdef+//framelabelstart+%28Rainfall%20on%20%29+psdef//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif"/>

    <script type="text/javascript" src="/uicore/uicore.js"></script>
    <script type="text/javascript" src="/localconfig/ui.js"></script>
    <script type="text/javascript" src="/uicore/insertui.js"></script>    

    <style>
      div {
      padding-left:15px;
      padding-right:15px;
      text-align: justify;
      }
      
      .valid.change {
      float:right; 
      padding-right:20%;
      padding-bottom:10px; 
      text-align: center; 
      }

      .dlimage,
      .dlimage.wide {
      width: 46%;
      display: inline;
      }
      .dlimgts {
      width: 100%;
      }
      @media only all and (max-width: 800px) {
      .dlimage,
      .dlimage.wide {
      width: 95%;
      }
      }
      .dlauximg.bis {
      padding-left:32px;
      width:93%;
      }
      .dlimage.withMap  {
      margin-left:20px;
      margin-top:25px;
      padding-top:20px;
      padding-bottom:10px;
      margin-bottom:25px;
      }
      .dlimg.bis  {
      width: 94%;
      }
      .dlimgtsbox {
      width: 85%;
      display: inline-block
      }
      .regionwithinbbox.dlimage.bis {
      margin-top:25px;
      margin-bottom:15px;
      padding-right:30px;
      padding-bottom:25px;
      }

      body[layers] #auxvar {
      display: none;
      }
      body[layers] #auxtopo {
      display:none;
      }
      body[layers~="monitAna"] #auxvar {
      display: inline;
      }
      body[layers~="bath"] #auxtopo {
      display: inline;
      }
      body[layers~="monitAna"] #auxtopo {
      display: none;
      }

      body[resolution="irids:SOURCES:Features:Political:Vietnam:Provinces:ds"] #notgridbox {
      display: none;
      }
      body[resolution="irids:SOURCES:Features:Political:Vietnam:Districts:ds"] #notgridbox {
      display: none;
      }
      
      .monitAna #startDay {
      display: none;    }
      .monitAnarainfall #startDay {
      display: none;
      }
      .monitAnaanom #startDay {
      display: none;
      }
      .monitAnaspi #startDay  {
      display: none;
      }
      .monitAnaanomper #startDay {
      display: none;
      }
    </style>
  </head>

  <body xml:lang="en">
    <form id="pageform" name="pageform" class="carryLanguage carryup carry dlimg dlauximg share">
      <input name="bbox" type="hidden" class="carry dlimg share dlimgloc dlimglocclick admin"/>
      <input name="monitAna" type="hidden" data-default="rainfall" class="dlimg dlauximg share cumulOpt bodyClass"/>
      <input name="T" type="hidden" class="dlimg share"/>
      <input name="plotaxislength" type="hidden" class="dlimg dlauximg"/>
      <input name="region" type="hidden" class="carry share dlimgts dlimgloc dlimglocclick"/>
      <input name="clickpt" type="hidden" class="transformRegion dlimglocclick"/>
      <input name="resolution" type="hidden" data-default="0.05" class="carry pickarea share admin"/>
      <input name="startDay" type="hidden" data-default="1-10 Jan 2019" class="share dlimg dlauximg cumulOpt"/>
      <!--input.share.dlimg.dlauximg.cumulOpt(name='endDay', type='hidden', data-default='last')-->

      <input name="layers" type="checkbox" value="monitAna" checked="checked" class="share bodyAttribute dlimg"/>
      <input name="layers" type="checkbox" value="bath" class="share bodyAttribute dlimg"/>
      <input name="layers" value="Provinces" checked="checked" type="checkbox" class="dlimg share bodyAttribute"/>
      <input name="layers" value="Districts" type="checkbox" class="dlimg share bodyAttribute"/>
      <input name="layers" value="countries_gaz" checked="checked" type="checkbox" class="dlimg share bodyAttribute"/>      
    </form>

    <div class="controlBar">
      <fieldset id="toSectionList" class="navitem">
        <legend>Maproom</legend><a rev="section" href="/maproom/ACToday/Vietnam/Climatology/" class="navlink carryup">Climate</a>
      </fieldset>

      <fieldset id="chooseSection" class="navitem">
        <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Monitoring"><span property="term:label">Climate Monitoring</span></legend>
      </fieldset>

      <!-- <fieldset class="navitem">
        <legend>Region</legend><a rel="iridl:hasJSON" href="/maproom/ethiopiaregions.json" class="carryLanguage"></a>
        <select name="bbox" class="RegionMenu">
          <option value="">Ethiopia</option>
          <optgroup label="Region" class="template">
            <option class="irigaz:hasPart irigaz:id@value term:label"></option>
          </optgroup>
        </select>
      </fieldset> -->

      <fieldset class="navitem">
        <legend>Spatially Average Over</legend><span class="selectvalue"></span>
        <select name="resolution" class="pageformcopy">
          <option value="0.05">0.05° gridpoint</option>
          <option value="irids:SOURCES:Features:Political:Vietnam:Provinces:ds">Province</option>
          <option value="irids:SOURCES:Features:Political:Vietnam:Districts:ds">District</option>
        </select>

        <link rel="iridl:hasJSON" href="/expert/(0.05)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A102%3A8.5%3A110.5%3A24%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/(bb%3A105%3A21%3A106%3A22%3Abb)//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json" class="admin"/>
        <select name="region" class="pageformcopy">
          <optgroup label="Label" class="template">
            <option class="label iridl:values region@value label"></option>
          </optgroup>
        </select>
      </fieldset>

      <fieldset class="navitem">
        <legend>Analysis</legend>
        <select name="monitAna" class="pageformcopy">
          <option value="rainfall">Rainfall Amounts</option>
          <option value="anom">Rainfall Anomalies</option>
          <option value="anomper">Rainfall Anomalies in Percent</option>
          <option value="cumulTot">Cumulative Rainfall</option>
          <option value="cumul">Cumulative Anomalies</option>
          <option value="cumulper">Cumulative Anomalies in Percent</option>
          <option value="spi">SPI</option>
        </select>
      </fieldset>
      <fieldset id="startDay" class="navitem">
        <legend>Cumulative Rainfall Anomaly</legend>    From 
        <input name="startDay" type="text" value="1-10 Jan 2019" size="15" maxlength="15" class="pageformcopy"/>
        <!--|  to-->
        <!--input.pageformcopy(name='endDay', type='text', value='last', size='15', maxlength='15')-->
      </fieldset>
    </div>

    <div class="ui-tabs">
      <ul class="ui-tabs-nav">
        <li><a href="#tabs-1">Description</a></li>
        <li><a href="#tabs-2">Dataset Documentation</a></li>
        <li><a href="#tabs-3">Instructions</a></li>
        <li><a href="#tabs-4">Contact Us</a></li>
      </ul>
      <fieldset about="" class="regionwithinbbox dlimage bis"><img src="/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A102%3A8.5%3A110.5%3A24%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(bb%3A106%2C21%2C107%2C22)//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/coasts/red/fill/red/smallpushpins/-fig//antialias/true/psdef//plotaxislength/240/psdef//plotborder/0/psdef/+.gif" class="dlimgloc"/><br/>

        <div align="center" id="notgridbox" class="valid change"><a rel="iridl:hasJSON" href="/expert/%28bb:100:15:101:20:bb%29//region/parameter/geoobject/info.json" class="dlimgloc"></a>
          <div align="center" class="template">Merged Station-Satellite Rainfall for <span class="bold iridl:long_name"></span></div>
        </div><br/><br/>

        <div class="dlimgtsbox">
          <div>a)</div><img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/.rainfall/.rfe_merged/(bb%3A105%3A21%3A106%3A22%3Abb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/last/365.0/3.0/mul/sub/last/RANGE//long_name/(Dekadal%20Rainfall)/def/DATA/0/AUTO/RANGE/toNaN/%5BT%5DREORDER/dup/0/mul/T/fig%3A/deltabars/%3Afig//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//XOVY/null/psdef/+.gif" class="dlimgts regionwithinbbox"/>
        </div>

        <div class="dlimgtsbox">
          <div>b)</div><img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/.rainfall/.rfe_merged/(bb%3A105%3A21%3A106%3A22%3Abb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/T/last/365.0/3.0/mul/sub/last/RANGE//scale_symmetric/true/def//long_name/(Rainfall%20%20Anomalies)/def/dup/T/fig-/colorbars2/-fig//plotborder/72/psdef//XOVY/null/psdef/+.gif" class="dlimgts regionwithinbbox"/>
        </div>

        <div class="dlimgtsbox">
          <div>c)</div><img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/.rainfall/.rfe_merged/(bb%3A105%3A21%3A106%3A22%3Abb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/a%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/3/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/2/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/interp/1/sub/s%3D%3D/VALUES/%3Aa/4/%7B4/-1/roll/T/last/cvsunits/(%20)/rsearch/pop/nip/nip/nip//fullname/exch/def%7Drepeat/3/index/.fullname/interp/1/Jan/2/index/julian_day/31/Dec/4/-1/roll/julian_day/dekadGRID/3/%7B4/-1/roll/T/2/index/replaceGRID/exch%7Drepeat/pop/4/-1/roll/1/index/.T/fig%3A/solid/verythin/grey/line/magenta/line/blue/line/medium/black/line/%3Afig//plotbordertop/25/psdef//plotborderbottom/62/psdef//antialias/true/def/+.gif" class="dlimgts regionwithinbbox"/>
        </div>

        <div class="dlimgtsbox">
          <div>d)</div>
          <!-- Takes into account new cumulative start day and plots a plume of percentiles.--><img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/.rainfall/.rfe_merged//long_name/%28Cumulative%20Rainfall%29def/%28bb:105:21:106:22:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/dup/T/last/dup/365/sub/exch/RANGE/T/%281%20Jan%29//startDay/parameter/last/RANGE/dup/.T/exch/T/differential_div/T/integral/3/-1/roll/dup/T/%281%20Jan%201981%29%2831%20Dec%201981%29RANGE/exch/T/%281%20Jan%201981%29%2831%20Dec%202010%29RANGE/T/name/npts/NewIntegerGRID/replaceGRID/T/36/splitstreamgrid/T/2/index/T/dekadaledgesgrid/partialgrid/2/1/roll/pop/replaceGRID/T/%28days%20since%201981-01-01%29streamgridunitconvert/T/T/dekadaledgesgrid/first/secondtolast/subgrid//calendar//365/def/gridS/365/store/modulus/pop/periodic/setgridtype/partialgrid/replaceGRID/nip/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/2/index/.T//pointwidth/11/def/replaceGRID/dup%5BT2%5D0.05/0.95/0/replacebypercentile/DATA/0/AUTO/RANGE/a:/percentile/0.95/VALUE/percentile/removeGRID/:a:/percentile/0.05/VALUE/percentile/removeGRID/:a/3/-1/roll%5BT2%5Daverage//fullname/%281981-2010%20Mean%29def/4/-1/roll//fullname/%28Cumulative%20Rainfall%29def/DATA/0/AUTO/RANGE/T/fig-/grey/deltabars/medium/solid/black/line/blue/line/-fig//antialias/true/psdef//framelabel/%28Grey%20shade%20covers%20area%20between%20the%205th%20and%2095th%20percentiles%29psdef//plotborder/100/psdef//antialias/true/psdef//fntsze/12/psdef/+.gif" class="dlimgts regionwithinbbox"/>
        </div>
        <br/><br/>

        <p>a) Dekadal (i.e., ~10-daily) rainfall for the selected region over the last 3 years.</p>
        <p>b) Dekadal rainfall differences (from 1981-2010 mean) for the selected region over the last 3 years.</p>
        <p>c) Smoothed dekadal rainfall for the current year (thick black line) compared to previous years (blue-1 yr from present; magenta- 2 yrs from present; grey-3 yrs from present).</p>
        <p>d) Cumulative dekadal rainfall (solid blue line) and the cumulative long-term average rainfall (solid black line) from user-chosen calendar day in the selected region. The grey plume indicates the range of the 5th and 95th cumulative rainfall percentiles.</p>
      </fieldset>

      <fieldset id="content" about="" class="dlimage withMap"><a rel="iridl:hasFigure" href="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/%28rainfall%29//monitAna/parameter/%28rainfall%29/eq/%7B.rainfall/.rfe_merged%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/%28Cumulative%20Rainfall%29/def/DATA/0/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/%28Cumulative%20Rainfall%20Anomaly%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%2821-31%20Jul%202019%29/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/%28Cumulative%20Rainfall%20Anomaly%20in%20%25%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B.rainfall/.rfe_mergeddiff%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B.rainfall/.rfe_mergeddiff_percent//long_name/%28Rainfall%20Anomaly%20Expressed%20as%20%25%20of%20Long-Term%20Average%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B.rainfall/.SPI-rfe_merged_1-dekad/DATA/-3.0/3.0/RANGE%7Dif//name/%28monitAna%29/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/102/110.5/RANGE/Y/8.5/24/RANGE/1/index/SOURCES/.Features/.Political/.Vietnam/.Provinces/.the_geom//name//Provinces/def/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/solid/stroke/black/thinnish/solid/countries_gaz/-fig//monitAna/get_parameter/%28rainfall%29/eq/%7B//framelabelstart/%28Rainfall%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20in%20Percent%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20in%20Percent%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B//framelabelstart/%28Standard%20Rainfall%20Index%201-dekad%20on%20%29/psdef%7Dif//plotaxislength/500/psdef//antialias/true/psdef//T/last/plotvalue//layers%5B//monitAna//countries_gaz//Provinces%5Dpsdef/"></a>

      <img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/%28rainfall%29//monitAna/parameter/%28rainfall%29/eq/%7B.rainfall/.rfe_merged%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/%28Cumulative%20Rainfall%29/def/DATA/0/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/%28Cumulative%20Rainfall%20Anomaly%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%2821-31%20Jul%202019%29/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/%28Cumulative%20Rainfall%20Anomaly%20in%20%25%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B.rainfall/.rfe_mergeddiff%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B.rainfall/.rfe_mergeddiff_percent//long_name/%28Rainfall%20Anomaly%20Expressed%20as%20%25%20of%20Long-Term%20Average%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B.rainfall/.SPI-rfe_merged_1-dekad/DATA/-3.0/3.0/RANGE%7Dif//name/%28monitAna%29/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/102/110.5/RANGE/Y/8.5/24/RANGE/1/index/SOURCES/.Features/.Political/.Vietnam/.Provinces/.the_geom//name//Provinces/def/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/solid/stroke/black/thinnish/solid/countries_gaz/-fig//monitAna/get_parameter/%28rainfall%29/eq/%7B//framelabelstart/%28Rainfall%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20in%20Percent%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20in%20Percent%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B//framelabelstart/%28Standard%20Rainfall%20Index%201-dekad%20on%20%29/psdef%7Dif//plotaxislength/500/psdef//antialias/true/psdef//T/last/plotvalue//layers%5B//monitAna//countries_gaz//Provinces%5Dpsdef/+.gif" border="0" alt="image" class="dlimg bis"/><br/>

      <span id="auxvar">
        <img rel="iridl:hasFigureImage" src="/SOURCES/.Vietnam/.ENACTS/.MON/.dekadal/%28rainfall%29//monitAna/parameter/%28rainfall%29/eq/%7B.rainfall/.rfe_merged%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID//long_name/%28Cumulative%20Rainfall%29/def/DATA/0/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/sub//long_name/%28Cumulative%20Rainfall%20Anomaly%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/AUTO/AUTO/RANGE%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7Ba:/.rainfall/.rfe_merged/T/%281-10%20Jan%202019%29//startDay/parameter/%2821-31%20Jul%202019%29/%28last%29/RANGE/dup/.T/exch/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/replaceGRID/:a:/.climatologies/.rfe_merged/:a/T/4/-1/roll/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/differential_div/T/integral/T/second/last/RANGE/T/2/index/.T/replaceGRID/dup/3/1/roll/sub/2/1/roll/1/add/div/100/mul//long_name/%28Cumulative%20Rainfall%20Anomaly%20in%20%25%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B.rainfall/.rfe_mergeddiff%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B.rainfall/.rfe_mergeddiff_percent//long_name/%28Rainfall%20Anomaly%20Expressed%20as%20%25%20of%20Long-Term%20Average%29/def//scale_symmetric/true/def/prcp_anomaly/DATA/-300/300/RANGE%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B.rainfall/.SPI-rfe_merged_1-dekad/DATA/-3.0/3.0/RANGE%7Dif//name/%28monitAna%29/def/a-/-a/SOURCES/.WORLDBATH/.bath/X/102/110.5/RANGE/Y/8.5/24/RANGE/1/index/SOURCES/.Features/.Political/.Vietnam/.Provinces/.the_geom//name//Provinces/def/X/Y/fig-/colors/colors/%7C%7C/colors/grey/thinnish/solid/stroke/black/thinnish/solid/countries_gaz/-fig//monitAna/get_parameter/%28rainfall%29/eq/%7B//framelabelstart/%28Rainfall%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28cumulTot%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumul%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28cumulper%29/eq/%7B//framelabel/%28Cumulative%20Rainfall%20Anomaly%20in%20Percent%20on%20%28%25=%5BT%5D%29%20from%20%29/lpar/append//startDay/get_parameter/append/rpar/append/psdef%7Dif//monitAna/get_parameter/%28anom%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28anomper%29/eq/%7B//framelabelstart/%28Rainfall%20Anomaly%20in%20Percent%20on%20%29/psdef%7Dif//monitAna/get_parameter/%28spi%29/eq/%7B//framelabelstart/%28Standard%20Rainfall%20Index%201-dekad%20on%20%29/psdef%7Dif//plotaxislength/500/psdef//antialias/true/psdef//T/last/plotvalue//layers%5B//monitAna//countries_gaz//Provinces%5Dpsdef/+.auxfig/+.gif" class="dlauximg bis"/>
      </span>
      <span id="auxtopo">
        <img rel="iridl:hasFigureImage" src="/SOURCES/.WORLDBATH/.bath/X/102/110.5/RANGE/Y/8.5/24/RANGE/X/Y/fig-/colors/-fig//antialias/true/psdef/.auxfig/+.gif" class="dlauximg"/>
      </span>
    </fieldset>

    <div id="tabs-1" about="" class="ui-tabs-panel">
        <h2 property="term:title" class="titre1">Climate Monitoring</h2>
        <p><b>Rainfall</b></p>
        <p><span property="term:description">This Analysis tool allows users to view different presentations of the the most recent dekad. </span></p>
        <p>The default map on this page displays dekadal (approximately 10-day) rainfall amounts over the country. The default map shows rainfall totals for the most recently available dekad, but totals for previous dekads can be displayed as well.</p>
        <p>By clicking a location on the map, the user can generate four time series graphs that provide analyses of recent rainfall averaged over an administrative district, with respect to that of recent years and the long-term mean.</p>

        <p><b>Rainfall Anomaly</b></p>
        <p>The rainfall anomalies map displays the difference between the most recent dekadal rainfall and the long-term average (1981-2010). Positive (negative) values indicate dekadal rainfall that are above (below) the long-term mean or climatology.</p>
        
        <p><b>Rainfall Anomaly in Percent</b></p>
        <p>The rainfall anomalies in percent map displays the difference between the most recent dekadal rainfall and the long-term average (1981-2010) expressed as a percentage of the climatology. Positive (negative) values indicate dekadal rainfall that are above (below) the long-term mean or climatology.</p>
        
        <p><b>Cumulative Rainfall</b></p>
        <p>The cumulative rainfall map displays the accumulated rainfall over a selected period.</p>
        
        <p><b>Cumulative Anomaly</b></p>
        <p>The cumulative anomaly map displays the difference between the accumulated rainfall over a selected period and the long-term average (1981-2010) for the same period. Positive (negative) values indicate accumulated rainfall that are above (below) the long-term mean or climatology.</p>
        
        <p><b>Cumulative Anomaly in Percent</b></p>
        <p>The cumulative anomaly map displays the difference between the accumulated rainfall over a selected period and the long-term average (1981-2010) for the same period expressed as a percentage of the climatology. Positive (negative) values indicate accumulated rainfall that are above (below) the long-term mean or climatology.</p>
        
        <p><b>Standard Precipitation Index (SPI)</b></p>
        <p>The SPI map displays the standard rainfall index of the most recent dekadal rainfall (using 1981 to the latest complete year as base period). The SPI (McKee 1993) is the number of standard deviations that observed cumulative rainfall deviates from the climatological average. To compute the index, a long-term time series of rainfall accumulations over dekads are used to estimate an appropriate probability density function. The analyses shown here are based on the Pearson Type III distribution (i.e., 3-parameter gamma) as suggested by Guttman (1999). The associated cumulative probability distribution is then estimated and subsequently transformed to a normal distribution. The result is the SPI, which can be interpreted as a probability using the standard normal distrubtion (i.e., users can expect the SPI be within one standard deviation about 68% of the time, two standard deviations about 95% of the time, etc.) The analyses shown here utilize the FORTRAN code made available by Guttman (1999). Places where the dekadal climatology is less than 2 mm are masked out.</p>

          <table border="1" cols="1">
            <tr>
              <td>
                <center>
                  <pre></pre>
                  <table border="1">
                    <tr align="center">
                      <td>
                        <pre>&nbsp;&nbsp;&nbsp;&nbsp;</pre>
                      </td>
                      <td><b>&nbsp;&nbsp;&nbsp;&nbsp; SPI Values &nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                      <td align="center"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Category &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td>
                    </tr>
                    <tr>
                      <td bgcolor="#14713D">
                        <pre></pre>
                      </td>
                      <td align="center">&ge; 2.00</td>
                      <td align="center">Extremely Wet</td>
                    </tr>
                    <tr>
                      <td bgcolor="#3CB371">
                        <pre></pre>
                      </td>
                      <td align="center">1.50 to 1.99</td>
                      <td align="center">Severely Wet</td>
                    </tr>
                    <tr>
                      <td bgcolor="#98FB98">
                        <pre></pre>
                      </td>
                      <td align="center">1.00 to 1.49</td>
                      <td align="center">Moderately Wet</td>
                    </tr>
                    <tr>
                      <td>
                        <pre></pre>
                      </td>
                      <td align="center">-0.99 to 0.99</td>
                      <td align="center">Near Normal</td>
                    </tr>
                    <tr>
                      <td bgcolor="#F5DEB3">
                        <pre></pre>
                      </td>
                      <td align="center">-1.00 to -1.49</td>
                      <td align="center">Moderately Dry</td>
                    </tr>
                    <tr>
                      <td bgcolor="#D2691E">
                        <pre></pre>
                      </td>
                      <td align="center"> -1.50 to -1.99</td>
                      <td align="center">Severely Dry</td>
                    </tr>
                    <tr>
                      <td bgcolor="#B22222">
                        <pre></pre>
                      </td>
                      <td align="center">&le; -2.00</td>
                      <td align="center">Extremely Dry</td>
                    </tr>
                  </table>
                </center>
              </td>
            </tr>
            <tr>
              <td>
                <table>
                  <tr>
                    <td>
                      <center><font size="-2">Table adapted from McKee <i>et al.</i> (1993)</font></center>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
      </div>
      
      <div id="tabs-2" class="ui-tabs-panel">
        <h2 class="titre1">Dataset Documentation</h2>
        <dl class="datasetdocumentation">Reconstructed rainfall over land areas on a 0.05&ring; x 0.05&ring; lat/lon grid (about 5km). The rainfall times series (1981 to present) were created by combining quality-controlled stations observations with satellite rainfall estimates.</dl>
      </div>
      
      <div id="tabs-3" class="ui-tabs-panel">
        <h2 class="titre1">How to use this interactive map</h2>
        <p style="padding-right:25px;">
          <div class="buttonInstructions bis"></div>
        </p>
      </div>
      
      <div id="tabs-4" class="ui-tabs-panel">
        <h2 class="titre1">Helpdesk</h2>
        <p class="p1">Contact <a href="mailto:help@iri.columbia.edu?subject=Climate Monitoring - Vietnam">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.</p>
      </div>
    </div><br/>
    
    <div class="optionsBar">
      <fieldset id="share" class="navitem">
        <legend>Share</legend>
      </fieldset>
    </div>

  </body>
</html>
