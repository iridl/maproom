<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="en">

  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0;"/>
    <meta property="maproom:Entry_Id" content="Health_Regional_Africa_Malaria_MDG"/>
    <meta property="maproom:Sort_Id" content="e07"/>
    <title>Vietnam Administrative-Average WASP Index</title>


    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
    <link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
    <link rel="stylesheet" type="text/css" href="/maproom/ACToday/actoday.css" />

    <link rel="canonical" href="index.html" class="share"/>
    <link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
    <link rel="home alternate" href="/maproom/navmenu.json" class="carryLanguage"/>
    
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Malaria"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Africa"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average_resolution"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#health"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_Malaria_term"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#Africa_3327_1"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standardized"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#WASP"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Epidemic_Malaria"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Historical_Analysis"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
  
    <link rel="term:icon" href="/SOURCES/.Vietnam/.ENACTS/.ALL/.monthly/.WASP/.12-month/.wasp8119/%28irids:SOURCES:Features:Political:World:Countries_GAUL:adm0_code%40264:ds%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T//pointwidth/1/def/pop//fullname/%2812-month%20WASP%29def/dup/T/12/STEP/%281981%29//baseyear/parameter/dup/%28%20%29rsearch/%7Bpop/pop/pop/T/exch/mark/exch/interp/counttomark/array/astore/nip/%7Bs==%7Dforall/VALUES%7D%7Bpop/T/exch/VALUES%7Difelse%5BT%5Daverage/T/0.0/mul/add//fullname/%28BaseLine%29def/2/copy/maskgt/1/index/T/fig-/green/deltabars/brown/deltabars/%7C/black/thin/line/black/medium/line/-fig//framelabel/%28precipitation%20relative%20to%20years%20%29//baseyear/get_parameter/append/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef//color_smoothing+1+psdef//plotborder+0+psdef+.gif"/>

    <script type="text/javascript" src="/uicore/uicore.js"></script>
    <script type="text/javascript" src="/localconfig/ui.js"></script>
    <script type="text/javascript" src="/uicore/insertui.js"></script>
  </head>


  <body xml:lang="en">
    <form id="pageform" name="pageform">
      <input name="Set-Language" type="hidden" class="carryup carryLanguage"/>
      <input name="plotaxislength" type="hidden" class="dlimg"/>
      <input name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:World:Countries_GAUL:ds" class="dlimg share admin"/>
      <input name="region" type="hidden" class="share dlimg dlimgloc"/>
      <input name="baseyear" type="hidden" data-default="1981" class="dlimg share"/>
    </form>

    <div class="controlBar">
      <fieldset id="toSectionList" class="navitem">
        <legend>Maproom</legend><a rev="section" href="/maproom/ACToday/Vietnam/Health/" class="navlink carryup">Climate and Health</a>
      </fieldset>
    
      <fieldset id="chooseSection" class="navitem">
        <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Malaria"><span property="term:label">Malaria</span></legend>
      </fieldset>

      <fieldset class="navitem">
        <legend>Baseline Years</legend>
        <input name="baseyear" type="text" value="1981" size="9" maxlength="" class="pageformcopy"/>
      </fieldset>

      <fieldset class="navitem">
        <legend>Spatially Average Over</legend><span class="selectvalue"></span>
        <select name="resolution" class="pageformcopy">
          <option value="irids:SOURCES:Features:Political:World:Countries_GAUL:ds">Country</option>
          <option value="irids:SOURCES:Features:Political:Vietnam:Provinces:ds">Province</option>
          <option value="irids:SOURCES:Features:Political:Vietnam:Districts:ds">District</option>
        </select>


        <link rel="iridl:hasJSON" href="/(irids%3ASOURCES%3AFeatures%3APolitical%3AWorld%3ACountries_GAUL%3Ads)//resolution/parameter/(Countries_GAUL%3Ads)/search/%7Bpop/pop/pop/(irids%3ASOURCES%3AFeatures%3APolitical%3AWorld%3ACountries_GAUL%3Aadm0_code%40264%3Ads)/%7Dif/geoobject/(bb%3A102%3A8.5%3A110.5%3A24%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects/info.json" class="admin"/>
        <select name="region" class="pageformcopy">
          <optgroup label="Label" class="template">
            <option class="iridl:values region@value label"></option>
          </optgroup>
        </select>
      </fieldset>
    </div>

    <div class="ui-tabs">
      <ul class="ui-tabs-nav">
        <li><a href="#tabs-1">Description</a></li>
        <li><a href="#tabs-2">Dataset Documentation</a></li>
        <li><a href="#tabs-3">Instructions</a></li>
        <li><a href="#tabs-4">Contact Us</a></li>
      </ul>

      <fieldset id="content" about="" class="dlimage"><a rel="iridl:hasFigure" href="/SOURCES/.Vietnam/.ENACTS/.ALL/.monthly/.WASP/.12-month/.wasp8119/%28irids:SOURCES:Features:Political:World:Countries_GAUL:adm0_code%40264:ds%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T//pointwidth/1/def/pop//fullname/%2812-month%20WASP%29def/dup/T/12/STEP/%281981%29//baseyear/parameter/dup/%28%20%29rsearch/%7Bpop/pop/pop/T/exch/mark/exch/interp/counttomark/array/astore/nip/%7Bs==%7Dforall/VALUES%7D%7Bpop/T/exch/VALUES%7Difelse%5BT%5Daverage/T/0.0/mul/add//fullname/%28BaseLine%29def/2/copy/maskgt/1/index/T/fig-/green/deltabars/brown/deltabars/%7C/black/thin/line/black/medium/line/-fig//framelabel/%28precipitation%20relative%20to%20years%20%29//baseyear/get_parameter/append/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef/"></a>

      <img src="/SOURCES/.Vietnam/.ENACTS/.ALL/.monthly/.WASP/.12-month/.wasp8119/%28irids:SOURCES:Features:Political:World:Countries_GAUL:adm0_code%40264:ds%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/T//pointwidth/1/def/pop//fullname/%2812-month%20WASP%29def/dup/T/12/STEP/%281981%29//baseyear/parameter/dup/%28%20%29rsearch/%7Bpop/pop/pop/T/exch/mark/exch/interp/counttomark/array/astore/nip/%7Bs==%7Dforall/VALUES%7D%7Bpop/T/exch/VALUES%7Difelse%5BT%5Daverage/T/0.0/mul/add//fullname/%28BaseLine%29def/2/copy/maskgt/1/index/T/fig-/green/deltabars/brown/deltabars/%7C/black/thin/line/black/medium/line/-fig//framelabel/%28precipitation%20relative%20to%20years%20%29//baseyear/get_parameter/append/psdef//plotbordertop/40/psdef//plotborderbottom/40/psdef//XOVY/null/psdef/+.gif" border="0" alt="image" class="dlimg"/>
      </fieldset>

      <div id="tabs-1" about="" class="ui-tabs-panel">
        <h2 align="center" property="term:title">Vietnam Administrative-Average WASP Index</h2>
        <p align="left" property="term:description">This plot shows the time series of 12-month Weighted Anomaly Standardization Precipitation (WASP) index relative to a baseline period. The purpose of this tool is to provide a simple visual means of relating averaged precipitation to a reference period of interest.</p>
        <p align="left">To compute the WASP index, monthly precipitation departures from the long-term (30-year) average are obtained and then standardized by dividing by the standard deviation of monthly precipitation. The standardized monthly anomalies are then weighted by multiplying by the fraction of the average annual precipitation for the given month. These weighted anomalies are then summed over a 12-month time period.</p>
        <p>Precipitation, especially in dry lands (warm semi-arid and desert fringe areas), is one of the factors responsible for creating the conditions which lead to the formation of sufficient surface water and moisture for mosquito breeding sites.</p>
        <p>For WASP index values above (below) the baseline, the area between the index and the baseline value is shaded in green (brown). A baseline above (below) 0 indicates the selected year or period recorded precipitation above (below) the long-term average.</p>
        <p>WASP index values are derived from precipitation estimates from the ENACTS merged rainfall dataset.</p>
        
        <h4>References</h4>
        <p>Lyon, B., and A. G. Barnston. <a href="http://journals.ametsoc.org/doi/abs/10.1175/JCLI3598.1">ENSO and the spatial extent of interannual precipitation extremes in tropical land areas</a>. <i>Journal of Climate</i>, 2005, <b>18</b>:5095-5109.</p>
        <p>Lyon, B. <a href="http://journals.ametsoc.org/doi/abs/10.1175/JCLI3598.1">The strength of El Ni&ntilde;o and the spatial extent of tropical drought.</a><i>Geophys. Res. Lett.</i>, <b>31</b>, 2004, L21204.</p>
      </div>

      <div id="tabs-2" class="ui-tabs-panel">
        <h2 align="center">Dataset Documentation</h2>
        <dl class="datasetdocumentation">
          <dt>Data Source</dt>
          <dd>ENACTS on a 0.05&deg; x 0.05&deg; lat/lon grid.</dd>
          <dt>Base Period for Anomalies</dt>
          <dd>1981 to 2010</dd>
        </dl>
      </div>

      <div id="tabs-3" class="ui-tabs-panel">
        <h2 class="titre1">How to use this interactive map</h2>
        <dl class="instructions">
          <p>The default plot on this page displays the WASP values spatially averaged over the country (Vietnam) against a base period (1981). Users can select to work at province or district levels and baseline year by using the menus at the top of the page.</p>
          <p>The baseline can be a single year or consist of a range of years. For multiple years, enter the 4 digits of the desired years separated by one blank. For instance the entry 1985 1990 2004 will select those 3 single-year baselines and average them to form a new baseline. For a range of years, enter the 4 digits of the first and the last year (inclusive) separated by a &quot;-&quot;. For instance 1990-1999 will select the 10 single-year baselines of the 90s and average them to form a new baseline.</p>
        </dl>
        <p style="padding-right:25px;">
          <div class="buttonInstructions bis"></div>
        </p>
      </div>

      <div id="tabs-4" class="ui-tabs-panel">
        <h2 class="titre1">Helpdesk</h2>
        <p class="p1">Contact <a href="mailto:help@iri.columbia.edu?subject= Health WASP - Vietnam">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.</p>
      </div>
    </div><br/>

    <div class="optionsBar">
      <fieldset id="share" class="navitem">
        <legend>Share</legend>
      </fieldset>
    </div>

  </body>
</html>