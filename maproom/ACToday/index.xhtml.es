<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="es">
  <head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0;"/>
    <meta property="maproom:Sort_Id" content="a011"/>
    <title>ACToday</title>
    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css"/>
    <link rel="stylesheet" type="text/css" href="/localconfig/ui.css"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#agriculture"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#FoodSecurity"/>
    <link rel="home" href="https://www.iri.columbia.edu/" title="IRI" class="carryLanguage"/>
    <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
    <link rel="canonical" href="index.html" class="share"/>
    <link rel="alternate" hreflang="en" href="index.html?Set-Language=en" class="altLanguage"/>
    <link rel="alternate" hreflang="fr" href="index.html?Set-Language=fr" class="altLanguage"/>
    <link rel="term:icon" href="FF-7523.jpeg"/>
    <script type="text/javascript" src="/uicore/uicore.js"></script>
    <script type="text/javascript" src="/localconfig/ui.js"></script>
  </head>
  <body xml:lang="es">
    <form id="pageform" name="pageform">
      <input name="Set-Language" type="hidden" class="carryLanguage carryup"/>
    </form>
    <div class="controlBar">
      <fieldset class="navitem">
        <legend>Datoteca</legend><a rev="section" href="/maproom/" class="navlink carryup">Maproom</a>
      </fieldset>
      <fieldset class="navitem">
        <legend>Maproom</legend><span class="navtext">ACToday</span>
      </fieldset>
    </div>
    <div>
      <div id="content" class="searchDescription">
        <h2 property="term:title">ACToday</h2>
      <div><img src="PartnersSmall.jpg" /></div>
        <p align="left" property="term:description">Adaptando Agricultura al Clima Hoy, para Mañana ; un Columbia World Project</p>
        <p>El objetivo de ACToday es crear soluciones basadas en servicios climáticos para terminar con el hambre, alcanzar la seguridad alimentaria, y  mejorar la nutrición, así como promover la agricultura sostenible (Meta de Desarrollo Sostenible número 2, MDS-2).
        <ul>
        <li>Estamos incorporando los mejores servicios climáticos posibles en las agendas y programas de instituciones nacionales y de socios internacionales que trabajan en el área de desarrollo, en un primer grupo de países seleccionados como prioritarios</li>
    <li>Estamos trabajando con nuestros socios para identificar impactos que sean medibles, así como evidencias de la utilidad de los servicios climáticos para reducir los riesgos en los sistemas alimentarios, y los beneficios que esto tiene sobre la MDS-2</li>
    <li>Vamos a identificar formas de llevar buenas prácticas a una escala mayor y, en colaboración con nuestros socios globales, identificaremos países que tienen las mayores necesidades.</li>
    </ul>
    </p>
      </div>
      <div about="/maproom/ACToday/" class="rightcol tabbedentries"></div>
    </div>
    <div class="optionsBar">
      <fieldset id="share" class="navitem">
        <legend>Compartir</legend>
      </fieldset>
    </div>
  </body>
</html>