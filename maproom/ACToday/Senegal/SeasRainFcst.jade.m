<<<includes>>>
=== canonical = "SeasRainFcst.html"
=== title = "<<<en:Seasonal Rainfall Forecast|||fr:Prévisions des Pluies Saisonnières>>>"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')

  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    title <<<title>>>
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='stylesheet', type='text/css', href='/maproom/ACToday/actoday.css')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link.share(rel='canonical', href='<<<canonical>>>')
    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Climate_Forecast')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rainfall')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Classification')

    link(rel='term:icon', href='http://iridl.ldeo.columbia.edu/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/Forecasts/.mu/%28Rainfall%29//field/parameter/%28Anomaly%29/eq/%7BHistory/.CrossValHind/%5BS%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29/eq/%7BHistory/.CrossValHind/%5BS%5D0.5/0.0/replacebypercentile/1.0/exch/div/exch/mul//units//unitless/def//percent/unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian//long_name/%28Percent%20of%20Median%29/def%7Dif//field/get_parameter/dup/%28exceeding%29/eq/exch/%28non-exceeding%29/eq/or/%7B-1/mul/%28Percentile%29//var/parameter/%28Rainfall%29/eq/%7B450.0//threshold2/parameter%7D%7BHistory/.obs/%5BT%5D50.0//percentile/parameter/100.0/div/0/replacebypercentile%7Difelse/add/Forecasts/.var/sqrt/div/Forecasts/.dof/poestudnt//long_name/%28probability%20of%20%29//field/get_parameter/append/def//field/get_parameter/%28non-exceeding%29/eq/%7B-1/mul/1/add%7Dif//var/get_parameter/%28Rainfall%29/eq/%7Bcorrelationcolorscale%7D%7B//field/get_parameter/%28non-exceeding%29/eq/%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/interp/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/interp/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7Difelse%7Difelse//units//unitless/def/%28/percent%29//poeunits/parameter/dup/%28/unitless%29/eq/%7Bpop/10.0/mul/DATA/0/10/RANGE%7D%7Binterp/unitconvert/DATA/0/100/RANGE%7Difelse%7Dif/History/.Pearson/Forecasts/.target_date/%7Bforecast/Skill/target_date%7Dds//name//Fcst/def/a-/.forecast/-a-/.Skill/-a-/.target_date/-a/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Commune/.the_geom/:a:/.Arrondissement/.the_geom/:a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/colors/plotlabel/grey/verythin/solid/stroke/thin/stroke/black/stroke/thinnish/stroke/coasts_gaz/-fig//framelabel/%28%25=%5Btarget_date%5D%20Fcst%20from%20%25b%5BS%5D%20%25Y%5BS%5D%20%29/psdef//layers%5B//forecast//Department//Region//coasts_gaz%5Dpsdef/S/last/plotvalue+//color_smoothing+1+psdef//antialias+true+psdef//plotborder+0+psdef//plotaxislength+432+psdef+.gif')

    meta(xml:lang='', property='maproom:Entry_Id', content='SeasRainFcst')
    meta(xml:lang='', property='maproom:Sort_Id', content='z00')
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    script(type='text/javascript', src='/uicore/insertui.js')
    style.
      sup { vertical-align:super; font-size:50%; }
      .varPrecipitation #percThresh {
      display: none;
      }
      .var #physThresh {
      display: none;
      }
      .varRainfall #percThresh {
      display: none;
      }
      .varPercentile #physThresh {
      display: none;
      }
      .field #PoEV {
      display: none;
      }
      .fieldRainfall #PoEV {
      display: none;
      }
      .fieldAnomaly #PoEV {
      display: none;
      }
      .fieldPoM #PoEV {
      display: none;
      }
      body[layers] #auxtopo {
      display: none;
      }
      body[layers~="forecast"] #auxvar {
      display: inline;
      }
      body[layers~="Skill"] #auxtopo {
      display: inline;
      }
      body[layers~="Skill"] #auxvar {
      display: none;
      }
      .dlcontrol.L {display: none !important;}

  body
    form#pageform(name='pageform')
      input.carryLanguage.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.dlimgloc.share(name='bbox', type='hidden')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.dlimg.dlimgts.share.starttime.startleadtime(name='S', type='hidden')
      input.carry.dlimgts.dlimgloc.share(name='region', type='hidden')
      input.carry.share.bodyAttribute(name='resolution', type='hidden', data-default='0.0375')
      input.dlimg.dlauximg.auxvar.share.threshold.poe.bodyClass(name='field', type='hidden', data-default='Rainfall')
      input.dlimg.dlauximg.auxvar.share.threshold.poe.bodyClass(name='var', type='hidden', data-default='Percentile')
      input.carry.dlimg.dlauximg.auxvar.share.threshold.aux2(name='percentile', type='hidden', data-default='50.0')
      input.carry.dlimg.auxvar.share.threshold.aux2(name='threshold2', type='hidden', data-default='450.0')
      input.dlimgts.share(name='plotrange1', type='hidden', data-default='0')
      input.dlimgts.share(name='plotrange2', type='hidden', data-default='1250')
      input.dlimg.dlauximg.carry.dlimgts.share(name='poeunits', type='hidden', data-default='/percent')
      input.dlimg.share.bodyAttribute(name='layers', value='forecast', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Skill', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='coasts_gaz', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Region', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Department', checked='checked', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Arrondissement', type='checkbox')
      input.dlimg.share.bodyAttribute(name='layers', value='Commune', type='checkbox')
    .controlBar
      fieldset#toSectionList.navitem
        legend ACToday
        a.navlink.carryup(rev='section', href='/maproom/ACToday/Senegal/') <<<en:Senegal|||fr:Sénégal>>>
      fieldset.navitem
        legend <<<en:Senegal|||fr:Sénégal>>>
        span.navtext <<<en:Precipitation Forecast|||fr:Prévisions des Pluies>>>
      fieldset.navitem
        legend <<<en:Precipitation Forecast|||fr:Prévisions des Pluies>>>
        select.pageformcopy(name='field')
          option(value='Rainfall') <<<en:Rainfall|||fr:Précipitations>>>
          option(value='Anomaly') <<<en:Anomaly|||fr:Anomalies>>>
          option(value='PoM') <<<en:Percent of Median|||fr: Pourcentage de la Médiane>>>
          option(value='exceeding') <<<en:Probability of exceeding|||fr:Probabilité de dépassement>>>
          option(value='non-exceeding') <<<en:Probability of non-exceeding|||fr:Probabilité de non-dépassement>>>
        span#PoEV
          select.pageformcopy(name='var')
            option(value='Rainfall') <<<en:Rainfall|||fr:Précipitations>>>
            option(value='Percentile') Percentile
          span#percThresh
            link.threshold(rel='iridl:hasJSON', href='/expert//percentile//unitless/ordered/10/5/90/NewEvenGRID/0/add//name//threshold/def//long_name//threshold/def//fullname//threshold/def/info.json')
            select.pageformcopy(name='percentile')
              optgroup.template
                option(class='iridl:values percentile@value threshold')
            | %-ile
          span#physThresh
            input.pageformcopy(name='threshold2', type='text', value='450.0', size='5', maxlength='5')
            | <<<en: mm|||fr: mm>>>
      fieldset.navitem
        legend <<<en:Probabilities Units|||fr:Unités des Probabilités>>>
        select.pageformcopy(name='poeunits')
          option(value='/unitless') fraction
          option(value='/percent') %
      fieldset.navitem(style='float: right;')
        legend <<<en:Local Plots Range|||fr:Limites des Graphes Locaux>>>
        input.pageformcopy(name='plotrange1', type='text', size='5', maxlength='5', value='0')
        |  <<<en:to|||fr:à>>>
        input.pageformcopy(name='plotrange2', type='text', size='5', maxlength='5', value='1250')
        |  <<<en: mm|||fr: mm>>>

    .ui-tabs
      ul.ui-tabs-nav
        li: a(href='#tabs-1') <<<en:Description|||fr:Déscription>>>
        li: a(href='#tabs-2') <<<en:Dataset Documentation|||fr:Documentation des Données>>>
        li: a(href='#tabs-3') Instructions
        li: a(href='#tabs-4') <<<en:Contact Us|||fr:Contact>>>

      fieldset.regionwithinbbox.dlimage(about='')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb%3A-14.0375%3A14%3A-14%3A14.0375%3Abb%29//region/parameter/geoobject/info.json')
          .template
            | <<<en:Forecast for |||fr:Prévisions pour >>>
            span.bold(class='iridl:long_name')
        #notgridbox.valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3ASenegal_Adm%3ACommune%3Ads)/geoobject/%28bb%3A-14.0375%3A14%3A-14%3A14.0375%3Abb%29//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | <<<en:located in or near |||fr:situé dans ou près de >>>
            span.bold(class='iridl:value')
        p
          table
            tr
              td(rowspan='2')
                img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/%28bb%3A-17.5875%3A12.225%3A-11.2125%3A16.8%3Abb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb%3A-14.0375%3A14%3A-14%3A14.0375%3Abb%29/dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/SOURCES/.Features/.Political/.Senegal_Adm/a%3A/.Commune/.the_geom/%3Aa%3A/.Arrondissement/.the_geom/%3Aa%3A/.Department/.the_geom/%3Aa%3A/.Region/.the_geom/%3Aa/X/Y/fig-/lightgrey/mask/red/fill/red/smallpushpins/grey/verythin/solid/stroke/thin/stroke/black/stroke/thinnish/stroke/blue/thin/rivers_gaz/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/160/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
              td
                span.valid
                  a.startleadtime(rel='iridl:hasJSON', href='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/.Forecasts/.target_date/S/last/cvsunits//S/parameter/VALUE/S//pointwidth/1/def/0.5/shiftGRID/info.json')
                  span.template
                    table(bgcolor='f0f0f0', border='1')
                      tr
                        | <<<en:Precipitation Forecast for|||fr:Prévisions des Pluies pour>>>
                        td <<<en:Target Date|||fr:Saison>>>
                        td <<<en:Issue Date|||fr:Publié en>>>
                      tr
                        th(class='iridl:value')
                        th(class='iridl:hasIndependentVariables')
                          span(class='iridl:value')
        br
        h4 <<<en:Probability of Exceedance|||fr:Probabilité de Dépassement>>>
        img.dlimgts(rel='iridl:hasFigureImage', src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/History/.obs/(bb:-14.0375:14:-14:14.0375:bb)//region/parameter/geoobject[X/Y]weighted-average/dup/DATA/0//plotrange1/parameter/1250//plotrange2/parameter/2/copy/exch/sub/500.0/div/RANGESTEP/integrateddistrib1D/obs//long_name/(Seasonal Rainfall)def//prcp/renameGRID/-1/mul/1/add/exch/dup[T]stddev/exch[T]average/-1/mul/prcp/add/exch/div/2/sqrt/div/erfc/2/div/Forecasts/.var//region/get_parameter/geoobject[X/Y]weighted-average/sqrt/Forecasts/.mu//region/get_parameter/geoobject[X/Y]weighted-average/-1/mul/prcp/add/exch/div/Forecasts/.dof/poestudnt/3/{3/-1/roll//units//unitless/def/(/percent)//poeunits/parameter/dup/(/unitless)eq/{pop/10.0/mul/DATA/0/10/RANGE}{interp/unitconvert/DATA/0/100/RANGE}ifelse//long_name/(probability of exceedance)def}repeat/Forecasts/.target_date/{obs/obsp/forecast/target_date}ds//name//poefcst/def/a-/.obs//fullname//Observed/def/-a-/.obsp//fullname//Smoothed/def/-a-/.forecast//fullname//Forecast/def/-a-/.target_date/-a/prcp/fig-/medium/solid/blue/line/black/line/red/line/plotlabel/black/thin/10/10/90//poeunits/get_parameter/(/unitless)eq/{3/{3/-1/roll/10.0/div}repeat}if/horizontallines/-fig//antialias/true/psdef//framelabel/(%25=[target_date] Forecast issued %25B[S] %25Y[S] at )//region/get_parameter/geoobject/.long_name/append/psdef//antialias/true/psdef/S/last/plotvalue/+.gif')
        h4 <<<en:Probability Distribution|||fr:Distribution des Probabilités>>>
        img.dlimgts(rel='iridl:hasFigureImage', src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/History/.obs/(bb%3A-14.0375%3A14%3A-14%3A14.0375%3Abb)//region/parameter/geoobject/[X/Y]weighted-average/dup/DATA/0//plotrange1/parameter/1250//plotrange2/parameter/2/copy/exch/sub/500.0/div/RANGESTEP/integrateddistrib1D/obs//long_name/(probability of exceedance)/def//prcp/renameGRID/DATA/0/AUTO/RANGE/exch/dup/[T]stddev/exch/[T]average/-1/mul/prcp/add/exch/div/2/sqrt/div/erfc/2/div/-1.0/mul/1.0/add//units//unitless/def/(/percent)//poeunits/parameter/dup/(/unitless)/eq/{pop/10.0/mul/DATA/0/10/RANGE}{interp/unitconvert/DATA/0/100/RANGE}ifelse/[prcp]partial/DATA/0/AUTO/RANGE//long_name/(probability of exceedance)/def/exch/Forecasts/.var//region/get_parameter/geoobject/[X/Y]weighted-average/sqrt/Forecasts/.mu//region/get_parameter/geoobject/[X/Y]weighted-average/-1/mul/prcp/add/exch/div/Forecasts/.dof/poestudnt/-1/mul/1/add//units//unitless/def//poeunits/get_parameter/dup/(/unitless)/eq/{pop/10.0/mul/DATA/0/10/RANGE}{interp/unitconvert/DATA/0/100/RANGE}ifelse/[prcp]partial/DATA/0/AUTO/RANGE//long_name/(probability of exceedance)/def/Forecasts/.target_date/{obsp/obs/forecast/target_date}ds//name//pdffcst/def/a-/.obsp//fullname//Observed/def/-a-/.forecast//fullname//Forecast/def/2/copy/max/DATA/0/AUTO/RANGE/-a-/.target_date/-a/exch/4/1/roll/prcp//long_name/(Seasonal Rainfall)/def/fig-/verythin/dotted/white/line/medium/solid/black/line/red/line/plotlabel/-fig//antialias/true/psdef//framelabel/(%25%3D[target_date] Forecast issued %25B[S] %25Y[S] at )//region/get_parameter/geoobject/.long_name/append/psdef/S/last/plotvalue/+.gif')

      fieldset#content.dlimage(about='')
        link(rel='iridl:hasFigure', href='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/Forecasts/.mu/%28Rainfall%29//field/parameter/%28Anomaly%29/eq/%7BHistory/.CrossValHind/%5BS%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29/eq/%7BHistory/.CrossValHind/%5BS%5D0.5/0.0/replacebypercentile/1.0/exch/div/exch/mul//units//unitless/def//percent/unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian//long_name/%28Percent%20of%20Median%29/def%7Dif//field/get_parameter/dup/%28exceeding%29/eq/exch/%28non-exceeding%29/eq/or/%7B-1/mul/%28Percentile%29//var/parameter/%28Rainfall%29/eq/%7B450.0//threshold2/parameter%7D%7BHistory/.obs/%5BT%5D50.0//percentile/parameter/100.0/div/0/replacebypercentile%7Difelse/add/Forecasts/.var/sqrt/div/Forecasts/.dof/poestudnt//long_name/%28probability%20of%20%29//field/get_parameter/append/def//field/get_parameter/%28non-exceeding%29/eq/%7B-1/mul/1/add%7Dif//var/get_parameter/%28Rainfall%29/eq/%7Bcorrelationcolorscale%7D%7B//field/get_parameter/%28non-exceeding%29/eq/%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/interp/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/interp/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7Difelse%7Difelse//units//unitless/def/%28/percent%29//poeunits/parameter/dup/%28/unitless%29/eq/%7Bpop/10.0/mul/DATA/0/10/RANGE%7D%7Binterp/unitconvert/DATA/0/100/RANGE%7Difelse%7Dif/History/.Pearson/Forecasts/.target_date/%7Bforecast/Skill/target_date%7Dds//name//Fcst/def/a-/.forecast/-a-/.Skill/-a-/.target_date/-a/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Commune/.the_geom/:a:/.Arrondissement/.the_geom/:a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/colors/plotlabel/grey/verythin/solid/stroke/thin/stroke/black/stroke/thinnish/stroke/coasts_gaz/-fig//antialias/true/psdef//framelabel/%28%25=%5Btarget_date%5D%20Fcst%20from%20%25b%5BS%5D%20%25Y%5BS%5D%20%29/psdef//layers%5B//forecast//Department//Region//coasts_gaz%5Dpsdef/S/last/plotvalue/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', alt='image', src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/Forecasts/.mu/%28Rainfall%29//field/parameter/%28Anomaly%29/eq/%7BHistory/.CrossValHind/%5BS%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29/eq/%7BHistory/.CrossValHind/%5BS%5D0.5/0.0/replacebypercentile/1.0/exch/div/exch/mul//units//unitless/def//percent/unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian//long_name/%28Percent%20of%20Median%29/def%7Dif//field/get_parameter/dup/%28exceeding%29/eq/exch/%28non-exceeding%29/eq/or/%7B-1/mul/%28Percentile%29//var/parameter/%28Rainfall%29/eq/%7B450.0//threshold2/parameter%7D%7BHistory/.obs/%5BT%5D50.0//percentile/parameter/100.0/div/0/replacebypercentile%7Difelse/add/Forecasts/.var/sqrt/div/Forecasts/.dof/poestudnt//long_name/%28probability%20of%20%29//field/get_parameter/append/def//field/get_parameter/%28non-exceeding%29/eq/%7B-1/mul/1/add%7Dif//var/get_parameter/%28Rainfall%29/eq/%7Bcorrelationcolorscale%7D%7B//field/get_parameter/%28non-exceeding%29/eq/%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/interp/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/interp/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7Difelse%7Difelse//units//unitless/def/%28/percent%29//poeunits/parameter/dup/%28/unitless%29/eq/%7Bpop/10.0/mul/DATA/0/10/RANGE%7D%7Binterp/unitconvert/DATA/0/100/RANGE%7Difelse%7Dif/History/.Pearson/Forecasts/.target_date/%7Bforecast/Skill/target_date%7Dds//name//Fcst/def/a-/.forecast/-a-/.Skill/-a-/.target_date/-a/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Commune/.the_geom/:a:/.Arrondissement/.the_geom/:a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/colors/plotlabel/grey/verythin/solid/stroke/thin/stroke/black/stroke/thinnish/stroke/coasts_gaz/-fig//antialias/true/psdef//framelabel/%28%25=%5Btarget_date%5D%20Fcst%20from%20%25b%5BS%5D%20%25Y%5BS%5D%20%29/psdef//layers%5B//forecast//Department//Region//coasts_gaz%5Dpsdef/S/last/plotvalue/+.gif')
        span#auxvar
          img.dlauximg.auxvar(rel='iridl:hasFigureImage', src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/Forecasts/.mu/%28Rainfall%29//field/parameter/%28Anomaly%29/eq/%7BHistory/.CrossValHind/%5BS%5Daverage/sub/prcp_anomaly/DATA/null/null/RANGE%7Dif//field/get_parameter/%28PoM%29/eq/%7BHistory/.CrossValHind/%5BS%5D0.5/0.0/replacebypercentile/1.0/exch/div/exch/mul//units//unitless/def//percent/unitconvert/Infinity/maskge/prcp_anomaly_25max500_colors2_percentmedian//long_name/%28Percent%20of%20Median%29/def%7Dif//field/get_parameter/dup/%28exceeding%29/eq/exch/%28non-exceeding%29/eq/or/%7B-1/mul/%28Percentile%29//var/parameter/%28Rainfall%29/eq/%7B450.0//threshold2/parameter%7D%7BHistory/.obs/%5BT%5D50.0//percentile/parameter/100.0/div/0/replacebypercentile%7Difelse/add/Forecasts/.var/sqrt/div/Forecasts/.dof/poestudnt//long_name/%28probability%20of%20%29//field/get_parameter/append/def//field/get_parameter/%28non-exceeding%29/eq/%7B-1/mul/1/add%7Dif//var/get_parameter/%28Rainfall%29/eq/%7Bcorrelationcolorscale%7D%7B//field/get_parameter/%28non-exceeding%29/eq/%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/interp/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/interp/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7Difelse%7Difelse//units//unitless/def/%28/percent%29//poeunits/parameter/dup/%28/unitless%29/eq/%7Bpop/10.0/mul/DATA/0/10/RANGE%7D%7Binterp/unitconvert/DATA/0/100/RANGE%7Difelse%7Dif/History/.Pearson/Forecasts/.target_date/%7Bforecast/Skill/target_date%7Dds//name//Fcst/def/a-/.forecast/-a-/.target_date/-a/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Commune/.the_geom/:a:/.Arrondissement/.the_geom/:a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/plotlabel/grey/verythin/solid/stroke/thin/stroke/black/stroke/thinnish/stroke/coasts_gaz/-fig//antialias/true/psdef//framelabel/%28%25=%5Btarget_date%5D%20Fcst%20from%20%25b%5BS%5D%20%25Y%5BS%5D%20%29/psdef//layers%5B//forecast//Department//Region//coasts_gaz%5Dpsdef/S/last/plotvalue/+.auxfig/+.gif')
        span#auxtopo
          img.dlauximg.auxtopo(rel='iridl:hasFigureImage', src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/.History/.Pearson/fig-/colorscale/-fig//antialias/true/psdef/+.gif')
      span#auxtopo
        fieldset#content.dlimage.regionwithinbbox
          h4 <<<en: Forecast Verification|||fr: Vérification des Prévisions>>>
          img.dlimgts(src='/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/History/.hindcast_date/History/.obs/History/.CrossValHind/2/array/astore/{/(bb%3A-14.0375%3A14%3A-14%3A14.0375%3Abb)//region/parameter/geoobject[X/Y]weighted-average}forall/3/-1/roll/use_as_grid/exch/T//hindcast_date/renameGRID/{CrossValPred/Input}ds//name//hindcast/def/a-/.Input//fullname//Observed/def/-a-/.CrossValPred//fullname//Hindcast/def/-a/hindcast_date/fig-/medium/solid/blue/line/red/line/-fig//antialias/true/psdef//framelabel/(Jul-Sep hindcast issued in April)/psdef/+.gif')

      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') <<<title>>>
<<<en:
        p(align='left', property='term:description') This Maproom shows seasonal rainfall forecast.
        p
          | The default map shows, for the latest forecast made, the more likely rainfall amount (in mm) in the season. Users can use the Precipitation Forecast menu to express the forecast in different ways, as follows:
          ul
            li
              b Rainfall
              | : most likely seasonal rainfall amount (in mm)
            li
              b Anomaly
              | : deviation in mm of the most likely seasonal rainfall amount from yearly average of the most likely seasonal rainfall amount predicted by the hindcast (1993-2016)
            li
              b Percent of Median
              | : deviation in percentages of the most likely seasonal rainfall amount from yearly median of the most likely seasonal rainfall amount predicted by the hindcast (1993-2016)
            li
              b Probability of non-/exceeding a Percentile
              | : forecast probability of seasonal total rainfall to be below/above the historically observed (1993-2016) chosen percentile
            li
              b Probability of non-/exceeding a Precipitation amount (in mm)
              | : forecast probability of seasonal total rainfall to be below/above the chosen rainfall amount (in mm)
        p
          | The Layers button, showing when mousing over the map, will reveal in/active layers on the map. The Skill map is inactive by default and can be activated here, to be overlaid over the forecast map.
        p
          | Clicking on the map will reveal information about the location clicked, as well as the full forecast distribution at that given location, compared with the historical distribution. Cumulative full distribution of the forecast (red) together with the climatological distribution (blue and black) for the forecast in view on the map shows under
          b Probability of Exceedance
          | , as well as the full probability distribution under
          b Probability Distribution
          | . If the Skill Layer is activated, a 3rd graph will show to present the comparison of hindcast performance versus actually observed seasonal rainfall.
|||fr:
        p(align='left', property='term:description') Cette Maproom présente les prévisions des pluies saisonnières.
        p
          | La carte par défaut présente, pour la plus récente prévision, les précipitations saisonnières (en mm/mois) les plus probables pour la saison. Les utilisateurs peuvent changer le menu Prévisions des Pluies pour représenter les prévisions de différentes manières :
          ul
            li
              b Précipitations
              | : précipitations saisonnières (en mm/mois) les plus probables
            li
              b Anomalies
              | : déviation en mm/mois des précipitations saisonnières les plus probables par rapport à la moyenne inter-annuelle des précipitations saisonnières les plus probables du hindcast (1993-2016)
            li
              b Percent of Median
              | : déviation en pourcentages des précipitations saisonnières les plus probables par rapport à la médiane inter-annuelle des précipitations saisonnières les plus probables du hindcast (1993-2016)
            li
              b Probabilité de non-/dépassement d'un Percentile
              | : probabilité des précipitations saisonnières d'être en-dessous/au-dessus d'un percentile des observations historiques (1993-2016)
            li
              b Probabilité de non-/dépassement d'un seuil de Précipitations (en mm/mois)
              | : probabilité des précipitations saisonnières d'être en-dessous/au-dessus d'un seuil de Précipitations (en mm/mois)
        p
          | Le bouton Layers, apparaîssant parmis les contrôles en passant la souris sur la carte, liste les couches actives (ou non) de la carte. La couche Skill est inactive par défaut et peut-être activée ici, et ainsi apparaître par-dessus la carte des prévisions.
        p
          | Cliquer la carte révèlera des informations sur la zone cliquée, ainsi que la distribution complète de la prévision, comparée avec la distribution historique. La distribution cumulative de la prévision (en rouge), associée à la distribution climatologique (en bleu et noir) pour la prévision visualisée sur la carte sont présentées sous 
          b Probabilité de Dépassement 
          | , ainsi que la distribution des probabilités sous 
          b Distribution des Probabilités
          | . Si la couche Skill est activée, un 3ème graphe apparaît comparant la performance du hindcast vis-à-vis des précipitations saisonnières veeritablement observées.
>>>
      #tabs-2.ui-tabs-panel
        h2(align='center') <<<en:Dataset Documentation|||fr:Documentation des Donnéees>>>
        dl.datasetdocumentation
<<<en:
          p All analyses are derived from 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/') ANACIM NextGen seasonal forecast.
          p
            | Forecast result from the calibration of an ensemble of dynamical models from the NMME et Copernicus suites, calibrated against 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.ANACIM/.ENACTS/.version3/.ALL/') ANACIM ENACTS observational data.
|||fr:
          p Toutes les analyses sont dérivées des 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.ANACIM/.CPT/.NextGen/.PRCPPRCP_CCAFCST/.NextGen/') prévisions saisonnières NextGen de l'ANACIM.
          p
            | Les prévisions résultent du calibrage d'un ensemble de modèles dynamiques issus des suites NMME et Copernicus, calibrées sur 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.ANACIM/.ENACTS/.version3/.ALL/') les données d'observations ENACTS de l'ANACIM.

>>>
      #tabs-3.ui-tabs-panel
        h2(align='center') Instructions
        p
          .buttonInstructions

      #tabs-4.ui-tabs-panel
        h2(align='center') <<<en:Helpdesk|||fr:Aide>>>
<<<en:
        p Contact 
          a(href='mailto:anacim@anacim.sn?subject=Seasonal Forecast') anacim@anacim.sn
          |  with any technical questions or problems with this Map Room.
|||fr:
        p Contactez 
          a(href='mailto:anacim@anacim.sn?subject=Prévisions Saisonnières') anacim@anacim.sn
          |  pour toute question ou problème concernant cette Maproom.
>>>
    .optionsBar
      fieldset#share.navitem
        legend <<<en:Share|||fr:Partager>>>
      fieldset#contactus.navitem.langgroup
        legend <<<en:Contact Us|||fr:Contact>>>
