<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="fr">
  <head>
    <meta name="viewport" content="width=device-width; initial-scale=1.0;"/>
    <meta property="maproom:Sort_Id" content="a011"/>
    <title>Sénégal</title>
    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css"/>
    <link rel="stylesheet" type="text/css" href="/localconfig/ui.css"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#agriculture"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#FoodSecurity"/>
    <link rel="home" href="https://www.iri.columbia.edu/" title="IRI" class="carryLanguage"/>
    <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
    <link rel="canonical" href="index.html" class="share"/>
    <link rel="alternate" hreflang="en" href="index.html?Set-Language=en" class="altLanguage"/>
    <link rel="alternate" hreflang="es" href="index.html?Set-Language=es" class="altLanguage"/>
    <link rel="term:icon" href="http://213.154.77.59/SOURCES/.ANACIM/.ENACTS/.version3/.ALL/.dekadal/.rainfall/.rfe/precip_colors/DATA/0/300/RANGE/SOURCES/.Features/.Political/.Senegal_Adm/a%3A/.Department/.the_geom/%3Aa%3A/.Region/.the_geom/%3Aa/X/Y/fig-/colors/thinnish/stroke/thin/stroke/-fig//antialias/true/psdef//plotborder/0/psdef/T/last/plotvalue/+.gif"/>
    <script type="text/javascript" src="/uicore/uicore.js"></script>
    <script type="text/javascript" src="/localconfig/ui.js"></script>
  </head>
  <body xml:lang="fr">
    <form id="pageform" name="pageform">
      <input name="Set-Language" type="hidden" class="carryLanguage carryup"/>
    </form>
    <div class="controlBar">
      <fieldset class="navitem">
        <legend>Maproom</legend><a rev="section" href="/maproom/ACToday/" class="navlink carryup">ACToday</a>
      </fieldset>
      <fieldset class="navitem">
        <legend>ACToday</legend><span class="navtext">Sénégal</span>
      </fieldset>
    </div>
    <div>
      <div id="content" class="searchDescription">
      <div><img src="PartnersSmall.jpg" /></div>
        <h2 property="term:title">Sénégal</h2>
        <p align="left" property="term:description">Conditions climatiques historiques, actuelles et prévisionaires dans le pays.</p>
      </div>

      <div about="/maproom/ACToday/Senegal/" class="rightcol tabbedentries">

        <a rel="section" href="http://213.154.77.59/maproom/"></a>
  <div about="http://213.154.77.59/maproom/?Set-Language=fr">
    	<meta property="maproom:Sort_Id" content="a03" />
  <a rel="canonical" href="http://213.154.77.59/maproom/"></a>
  <p property="term:title">Sénégal</p>
  <p property="term:description">Maprooms d'ANACIM</p>
  <a rel="term:icon" href="http://213.154.77.59/SOURCES/.ANACIM/.ENACTS/.version3/.ALL/.dekadal/.rainfall/.rfe/precip_colors/DATA/0/300/RANGE/SOURCES/.Features/.Political/.Senegal_Adm/a%3A/.Department/.the_geom/%3Aa%3A/.Region/.the_geom/%3Aa/X/Y/fig-/colors/thinnish/stroke/thin/stroke/-fig//antialias/true/psdef//plotborder/0/psdef/T/last/plotvalue/+.gif"></a>
</div>

</div>

    </div>
    <div class="optionsBar">
      <fieldset id="share" class="navitem">
        <legend>Partager</legend>
      </fieldset>
    </div>
  </body>
</html>
