<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
 xml:lang="es"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>ENSO Cuarto de Mapas</title>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />

<link rel="canonical" href="index.html" />
<link class="altLanguage" rel="alternate" hreflang="en" href="Diagnostics.html?Set-Language=en" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<meta property="maproom:Sort_Id" content="a04" />

<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy"
href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#enso" />
<link rel="term:icon" href="http://iridl.ldeo.columbia.edu/filters/.NINO/SOURCES/.Indices/.india/.rainfall/T/first/%281870%29RANGE/SOURCES/.IITM/.All_India/.Rainfall/.PCPN/T/%28Jun%201871%29last/RANGE/T/4/boxAverage/4/mul/T/12/STEP/appendstream%5BT%5DREORDER/dup/mean/sub/SOURCES/.KAPLAN/.EXTENDED/.v2/.ssta/NINO3/T/%28Oct%201856%29last/RANGE/T/4/boxAverage/T/12/STEP/T/-4/shiftGRID/2/T/1/MATCH/correlationcolorscale/DATA/-2.5/2.5/RANGE/T+fig-+colorbars2+thin+grey+dotted+-200+50+150+horizontallines++|+colorbars2+-fig+//rainfall/-241.5632/174.4369/plotrange//NINO3/-3/3/plotrange/T/-1243./last/plotrange//XOVY+null+psdef//plotborder+0+psdef+.gif" />
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>
<body>

<form name="pageform" id="pageform">
<input class="carry carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem"> 
                <legend>Datoteca</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/">Maproom</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Maproom</legend> 
                     <span class="navtext">ENSO</span>
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Mundial</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

 </div>
<div>
 <div id="content" class="searchDescription">
     <h2 property="term:title">El Niño, La Niña y la Oscilación del Sur (ENSO)</h2>
     <p>
     El "ENSO" se refiere a El Niño-Oscilación del Sur, un ciclo de eventos de calentamiento y enfriamiento del Océano Pacífico ecuatorial y su atmósfera que ocurre con una frecuencia aproximada de 2 a 7 años. Esta variabilidad anual o multi-anual en condiciones oceanográficas y atmosféricas tienen impactos de gran alcance, llamado "teleconexiones", sobre la precipitación estacional y los patrones de temperatura en muchos lugares del mundo. 
     </p> 
     <p align="left" property="term:description"> Este conjunto de datos incluye mapas y análisis útiles para el monitoreo del ENSO, para entender su impacto y para acceder boletines preparados por diferentes organizaciones activas en el monitoreo y el pronóstico del ENSO. 
     
     </p>
</div>



  
  <div class="rightcol tabbedentries" about="/maproom/ENSO/index.html" >
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
