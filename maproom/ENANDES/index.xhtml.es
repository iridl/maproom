<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Pronósticos Subestacionales para ENANDES</title>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" />
<link rel="term:icon" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7Bpop%7D%7Bname//target_date/eq/%7Bpop%7D%7BS//S/get_parameter/VALUE/dup/null/eq/%7Bpop%7Dif%7Difelse%7Dforalldatasets2/counttomark/3/div/%7B3/array/astore/counttomark/1/roll%7Drepeat/counttomark/array/astore/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7Bdup/0/get/aload/pop/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a/50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul%7D%7B15.0//threshold2/parameter%7Difelse/exch/%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/4/-1/roll/mul/add/exch/counttomark/1/sub/index/mul/add/eexp/dup/1.0/add/div%7Dforall/counttomark/1/sub/dup/1/add/1/roll/counttomark/3/sub/%7Badd%7Drepeat/exch/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse//var/get_parameter/%28Percentile%29/ne/%7Bnip/correlationcolorscale/DATA/0/1/RANGE/dup%7Dif//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/3/-1/roll/pop/%7Btarget_date/percentile_val/forecast%7Dds//name//flexfcst/def/a-/.forecast/-a-/.percentile_val/-a-/.target_date/-a/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/grey/contours/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//layers%5B//forecast//percentile_val//countries//lakes%5Dpsdef//S/last/plotvalue+//color_smoothing+1+psdef//antialias+true+psdef//plotborder+0+psdef//plotaxislength+432+psdef+.gif" />
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>
<body xml:lang="es">

<form name="pageform" id="pageform">
<input class="carryLanguage carryup" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>Data Library</legend> 
                    <a rev="section" class="navlink carryup" href="/maproom/">Maproom</a>
            </fieldset> 
           <fieldset class="navitem"> 
                <legend>Maproom</legend><span class="navtext">ENANDES</span> 
            </fieldset> 
            <fieldset class="navitem">
                <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/enandesregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Andes</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>
</div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">ENANDES</h2>
<p align="left" property="term:description">
    ENANDES fomenta la adaptación climática en los Andes
</p>
<p>
    El proyecto ENANDES, acrónimo de Enhancing Adaptive Capacity of Andean Communities through Climate Services (Mejora de la Capacidad de Adaptación de las Comunidades Andinas a través de los Servicios Climáticos), se llevará a cabo en Chile, Colombia y el Perú con el fin de generar y difundir conocimientos científicos que ayuden a las sociedades a adaptarse mejor a los efectos del cambio climático.
</p>
<p>
    El proyecto se ha concebido para mejorar la prestación de servicios climáticos a escala nacional y regional, es decir, la producción, traducción y difusión oportunas de información y conocimientos climáticos para la adopción de decisiones que incidan en el conjunto de la sociedad.
</p>
<p>
    Esta sección es dedicada a los pronósticos subestacionales, es decir, aquellos que reducen la brecha entre los pronósticos meteorológicos a mediano plazo (hasta 10 días) y las predicciones climáticas estacionales (más de un mes). Estos pronósticos subestacionales emiten semanalmente pronósticos semanales para las semanas 1 a 4 siguientes. Los pronósticos subestacionales pueden brindar información relevante sobre características climáticas clave, como el momento del inicio de una temporada de lluvia para la agricultura o el riesgo de eventos de lluvias extremas.
</p>
<p>
    Estos mapas incluyen pronósticos subestacionales de precipitación semanal basados en el ensamble de múltiples modelos de pronósticos individuales emitidos todos los viernes a través de la base de datos en tiempo real SubX.
</p>

</div>
<div class="rightcol tabbedentries" about="/maproom/ENANDES/" >
  <link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Forecasts_term" />
  <link rel="maproom:tabterm" 
  href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Skill_term" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Compartir</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
