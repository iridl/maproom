<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wn20schema="http://www.w3.org/2006/03/wn/wn20/schema/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.1"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta xml:lang="" property="maproom:Sort_Id" content="e01" />
<title>Pronóstico Semanal Flexible de Precipitación</title>

<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="precip_flex_subx_weekly.html?Set-Language=en" />
<link class="share" rel="canonical" href="precip_flex_subx_weekly.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />

<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Forecasts_term" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#weekly"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Colombia"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Peru"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Chile"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#subseasonal"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#percentile"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Probability"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#IRI"/>

<link rel="term:icon" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7Bpop%7D%7Bname//target_date/eq/%7Bpop%7D%7BS//S/get_parameter/VALUE/dup/null/eq/%7Bpop%7Dif%7Difelse%7Dforalldatasets2/counttomark/3/div/%7B3/array/astore/counttomark/1/roll%7Drepeat/counttomark/array/astore/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7Bdup/0/get/aload/pop/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a/50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul%7D%7B90.0//threshold2/parameter%7Difelse/exch/%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/4/-1/roll/mul/add/exch/counttomark/1/sub/index/mul/add/eexp/dup/1.0/add/div%7Dforall/counttomark/1/sub/dup/1/add/1/roll/counttomark/3/sub/%7Badd%7Drepeat/exch/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown/50.0//percentile/parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue/50.0//percentile/parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse//var/get_parameter/%28Percentile%29/ne/%7Bnip/correlationcolorscale/DATA/0/1/RANGE/dup%7Dif//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/3/-1/roll/pop/%7Btarget_date/percentile_val/forecast%7Dds//name//flexfcst/def/a-/.forecast/-a-/.target_date/-a/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//layers%5B//forecast//countries//lakes%5Dpsdef//S/last/plotvalue+//color_smoothing+1+psdef//antialias+true+psdef//plotborder+0+psdef//plotaxislength+432+psdef+.gif" />

<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
<script type="text/javascript" src="/uicore/insertui.js"></script>

<style>
sup { vertical-align:super; font-size:50%; }

.varPrecipitation #percThresh {
display: none;
}
.var #physThresh {
display: none;
}
.var #physThresh2 {
display: none;
}
.punits #physThresh {
display: none;
}
.punitsmmday #physThresh2 {
display: none;
}
.punitsmm #physThresh {
display: none;
}

.proba #notmedian {
display: none;  
}
.proba #percThresh {
display: none;  
}
.proba #physThresh {
display: none;
}
.proba #physThresh2 {
display: none;
}
.probaMedian #notmedian {
display: none;  
}
.probaMedian #percThresh {
display: none;  
}
.probaMedian #physThresh {
display: none;
}
.probaMedian #physThresh2 {
display: none;
}

.probaexceeding #iqr{
  display: none;
}
.probanon-exceeding #iqr{
  display: none;
}

.dlcontrol.L {display: none !important;}
.dlimage.ver2 {
	float: right;
}
@media only all and (max-width: 750px) {
.dlimage.ver2 {
        width: 95%;
	float: right;
}
}
</style>
</head>
<body xml:lang="es">
<form name="pageform" id="pageform">
<input class="carryup carry" name="Set-Language" type="hidden" />
<input class="carryup carry dlimg dlimgloc share" name="bbox" type="hidden" />
<input class="carry fcst iqr dlimgts share starttime startleadtime" name="S" type="hidden" />
<input class="carry fcst iqr dlimgts share startleadtime" name="L" type="hidden" data-default="4.5" />
<input class="carry dlimgts dlimgloc share" name="region" type="hidden" />
<input class="dlimgnotuseddlauximg" name="plotaxislength" type="hidden" />
<input class="pickarea1" name="resolution" type="hidden" data-default="1.0" />
<input class="carry fcst share bodyClass" name="proba" type="hidden" data-default="Median" />
<input class="carry fcst share threshold bodyClass" name="var" type="hidden" />
<input class="carry fcst share threshold" name="percentile" type="hidden" data-default="50.0" />
<input class="fcst share threshold" name="threshold2" type="hidden" data-default="90.0" />
<input class="fcst dlimgts share threshold bodyClass" name="punits" type="hidden" data-default="mm" />

<input class="fcst share" name="layers" value="forecast" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="lakes" checked="checked" type="checkbox" />

<input class="dlimg share" name="layers" value="2nd_order" type="checkbox" />
<input class="dlimg share" name="layers" value="1st_order" type="checkbox" />
<input class="dlimg share" name="layers" value="rivers_gaz" type="checkbox" />
<input class="dlimg share" name="layers" value="streams" type="checkbox" />
<input class="dlimg share" name="layers" value="int_streams" type="checkbox" />

</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Maprooms</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/ENANDES/">ENANDES</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Forecasts_term"><span property="term:label">Pronósticos SubX</span></legend>
            </fieldset>
            <fieldset class="navitem">
                <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/enandesregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Andes</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

      <fieldset class="navitem">
            <legend>Pronóstico Hecho en</legend>
            <link class="" rel="iridl:hasJSON" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/pop/S/last/first/RANGE/info.json" />
            <select class="pageformcopy containsAllValids" name="S">
               <optgroup class="template" label="Issues">
                  <option class="iridl:values S@value S"></option>
               </optgroup>
               </select>
         </fieldset>

      <fieldset class="navitem valid">
        <legend>Valido para</legend>
        <link class="starttime" rel="iridl:hasJSON" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/info.json" />
        <select class="pageformcopy" name="L">
          <optgroup class="template" label="Target Period">
            <option class="iridl:values L@value target_date"></option>
          </optgroup>
        </select>
      </fieldset>

            <fieldset class="navitem">
              <legend>Pronóstico</legend>
              <select class="pageformcopy" name="proba">
                <option value="Median">Mediana</option>
                <option value="exceeding">Probabilidad de excedencia</option>
                <option value="non-exceeding">Probabilidad de no-excedencia</option>
              </select>
              
            <span id="notmedian">
              <select class="pageformcopy" name="var">
                <option value="">Percentile</option>
                <option value="Precipitation">Precipitación</option>
              </select>
            </span>
              <span id="percThresh">
                <link class="threshold" rel="iridl:hasJSON" href="/expert/grid%3A//name//percentile/def/values%3A/1./2.5/5./10./15./20./25./30./35./40./45./50./55./60./65./70./75./80./85./90./95./97.5/99./%3Avalues/%3Agrid/0./add//name//threshold/def//long_name//threshold/def//fullname//threshold/def/info.json" />
                <select class="pageformcopy containsAllValids" name="percentile">
                  <optgroup class="template">
                    <option class="iridl:values percentile@value threshold"></option>
                  </optgroup>
                </select>%-ile
              </span>
              <span id="physThresh">
                <input class="pageformcopy" name="threshold2" type="text" value="90.0" size="5" maxlength="5"/>mm/día
              </span>
              <span id="physThresh2">
                <input class="pageformcopy" name="threshold2" type="text" value="90.0" size="5" maxlength="5"/>mm
              </span>
</fieldset>
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripción</a></li>
      <li><a href="#tabs-2" >Documentación de los Datos</a></li>
      <li><a href="#tabs-4" >Instrucciones</a></li>
      <li><a href="#tabs-5" >Contáctenos</a></li>
    </ul>

    <fieldset class="regionwithinbbox dlimage ver2" about="" >
<p>
<table>
<tr>
  <td rowspan="2"><img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-85:-55:-65:15:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-73:0:-72:1:bb%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
  </td>

  <td>
    <span class="valid">
       <a class="startleadtime" rel="iridl:hasJSON" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/L/4.5//L/parameter/VALUE/L/removeGRID/info.json" ></a>
      <span class="template">
        <table bgcolor="f0f0f0">
           <tr>
              <td>Valido para</td><td>Hecho en</td><td>Lead Time</td>
           </tr>
           <tr>
             <th class="iridl:value"></th>
             <th class="iridl:hasIndependentVariables">
               <span class="iridl:value"></span>
             </th>
           </tr>
         </table>
       </span>
     </span>
   </td>
</tr>
</table>
   </p>
   
     <div class="valid" style="display: inline-block; text-align: top;">
    <a class="dlimgloc" rel="iridl:hasJSON" href="/expert/%28bb:-73:0:-72:1:bb%29//region/parameter/geoobject/info.json"></a>
    <div class="template">Pronóstico hecho para <span class="bold iridl:long_name"></span></div>
  </div>
<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgloc" rel="iridl:hasJSON" href="/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3AWorld%3AsecondOrder_GAUL_coarse%3Ads)/geoobject/%28bb:-73:0:-72:1:bb%29//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json"></a>
<div class="template">localizado en or cerca de <span class="bold iridl:value"></span></div>
</div>

      <h4>Probabilidad de Excedencia</h4>
  <img class="dlimgts" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/a-/-a/mark/ELR_coeffs/{name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/{pop}{.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/L/4.5//L/parameter/VALUE/exch/grid://name/(probability)def//long_name/(probability of exceeding)def/9.9999997E-05/0.9999/2/copy/exch/sub/500.0/div/exch/:grid/0.0/add/dup/-1.0/mul/1.0/add/exch/div/ln/exch/ELR_coeffs/{pop}{name//target_date/eq/{pop}{(bb:-73:0:-72:1:bb)//region/parameter/geoobject[X/Y]weighted-average/L//L/get_parameter/VALUE/S//S/get_parameter/VALUE/dup/null/eq/{pop}if}ifelse}foralldatasets2/counttomark/3/div/{3/array/astore/counttomark/1/roll}repeat/counttomark/array/astore/{aload/pop/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/5/-1/roll/mul/add/counttomark/1/add/index/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul//long_name/(Precipitation)def//units/(mm/week)def/exch/probability/0.0/add/dup/-1.0/mul/1.0/add/exch/div/ln/1/index/ind/0/VALUE/ind/removeGRID/sub/exch/ind/2/VALUE/ind/removeGRID/div/dup/dataflag/1/masklt/exch/0.0/max/mul//long_name/(Precipitation)def//units/(mm/week)def}forall/counttomark/2/div/dup/1/sub/{5/-4/roll/3/-1/roll/add/3/-2/roll/add/exch/3/-1/roll}repeat/2/{3/-1/roll/1/index/div/exch}repeat/pop/4/-2/roll/pop/pop/{target_date/forecast/clim}ds//name//flexfcst/def/a-/.clim/probability/100/mul//units//percent/def/use_as_grid/probability//fullname//Climatology/def/-a-/.target_date/S/-a-/.forecast/probability/100/mul//units//percent/def/use_as_grid/probability//fullname//Forecast/def/-a/fig-/medium/black/dotted/scatterline/plotlabel/plotlabel/green/solid/scatterline/-fig//framelabel/(%25=[target_date] Flexible SubX Precipitation forecast issued %25d[S] %25b[S] %25Y[S])psdef//antialias/true/psdef//XOVY/1.6/psdef/+.gif" />
  
        <h4>Distribución de las Probabilidades</h4>
  <img class="dlimgts" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/a-/-a/mark/ELR_coeffs/{name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/{pop}{.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/L/4.5//L/parameter/VALUE/exch/grid%3A//name/(probability)/def/9.9999997E-05/0.9999/2/copy/exch/sub/500.0/div/exch/%3Agrid/0.0/add/dup/-1.0/mul/1.0/add/div/ln/probability/0.0/add//name//myinv/def/exch/use_as_grid/ln//stdvar/renameGRID//long_name/(proba)/def/[stdvar]partial/exch/ELR_coeffs/{pop}{name//target_date/eq/{pop}{/(bb:-73:0:-72:1:bb)//region/parameter/geoobject[X/Y]weighted-average/L//L/get_parameter/VALUE/S//S/get_parameter/VALUE/dup/null/eq/{pop}if/}ifelse}foralldatasets2/counttomark/3/div/{3/array/astore/counttomark/1/roll}repeat/counttomark/array/astore/{aload/pop/exch/a%3A/ind/2/VALUE/ind/removeGRID/%3Aa%3A/ind/0/VALUE/ind/removeGRID/%3Aa/stdvar/exch/sub/1/index/div/dup/dataflag/1/masklt/exch/0.0/max/mul//long_name/(Precipitation)/def//units/(mm/week)/def/exch/counttomark/1/add/index/mul/4/-2/roll/a%3A/ind/2/VALUE/ind/removeGRID/%3Aa%3A/ind/0/VALUE/ind/removeGRID/%3Aa%3A/ind/1/VALUE/ind/removeGRID/%3Aa/4/-1/roll/mul/add/stdvar/exch/sub/1/index/div/dup/dataflag/1/masklt/exch/0.0/max/mul//long_name/(Precipitation)/def//units/(mm/week)/def/exch/counttomark/1/add/index/mul//long_name/(probability density)/def}forall/counttomark/4/div/dup/1/sub/{5/-1/roll/9/-1/roll/add/5/-1/roll/8/-1/roll/add/5/-1/roll/7/-1/roll/add/6/-2/roll/add/5/-1/roll}repeat/5/-4/roll/4/{4/-1/roll/4/index/div}repeat//fullname//Forecast/def/3/-1/roll//fullname//Climatology/def/3/1/roll/8/-4/roll/pop/pop/pop/3/-2/roll/fig-/medium/black/dotted/scatterline/plotlabel/green/solid/scatterline/-fig//framelabel/(%25%3D[target_date] Flexible SubX Precipitation forecast issued %25d[S] %25b[S] %25Y[S])/psdef//antialias/true/psdef//XOVY/1.6/psdef/+.gif" />
  </fieldset>

  <fieldset class="dlimage">
    <a rel="iridl:hasFigure" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7Bpop%7D%7Bname//target_date/eq/%7Bpop%7D%7BS//S/get_parameter/VALUE/dup/null/eq/%7Bpop%7Dif%7Difelse%7Dforalldatasets2/counttomark/3/div/%7B3/array/astore/counttomark/1/roll%7Drepeat/counttomark/array/astore/%28Median%29//proba/parameter/%28Median%29eq/%7B%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/-1.0/mul/:a:/ind/1/VALUE/ind/removeGRID/-1/mul/:a/4/-1/roll/mul/add/exch/div%7Dforall/counttomark/dup/1/add/1/roll/counttomark/2/sub/%7Badd%7Drepeat/exch/div/nip/precip_colors/DATA/0/AUTO/RANGE//long_name/%28Most%20Likely%20Forecast%29def//units//mm/def/dup%7D%7B%28Percentile%29//var/parameter/%28Percentile%29eq/%7Bdup/0/get/aload/pop/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a/50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul%7D%7B90.0//threshold2/parameter%7Difelse/exch/%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/4/-1/roll/mul/add/exch/counttomark/1/sub/index/mul/add/eexp/dup/1.0/add/div%7Dforall/counttomark/1/sub/dup/1/add/1/roll/counttomark/3/sub/%7Badd%7Drepeat/exch/div//proba/get_parameter/%28exceeding%29eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown/50.0//percentile/parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue/50.0//percentile/parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse//var/get_parameter/%28Percentile%29ne/%7Bnip/correlationcolorscale/DATA/0/1/RANGE/dup%7Dif//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/3/-1/roll/pop//percent/unitconvert%7Difelse/%7Btarget_date/contours/forecast%7Dds//name//flexfcst/def/a-/.forecast/-a-/.target_date/-a/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/grey/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Precipitation%20forecast%20issued%20%25d%5BS%5D%20%25b%5BS%5D%20%25Y%5BS%5D%29psdef//layers%5B//forecast//countries//lakes%5Dpsdef//antialias/true/psdef/S/last/plotvalue/">visit site</a>
    <img class="dlimg fcst" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7Bname/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7Bpop%7D%7B.target_date/S/.last%7Difelse%7D%7Bpop%7Dforalldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7Bpop%7D%7Bname//target_date/eq/%7Bpop%7D%7BS//S/get_parameter/VALUE/dup/null/eq/%7Bpop%7Dif%7Difelse%7Dforalldatasets2/counttomark/3/div/%7B3/array/astore/counttomark/1/roll%7Drepeat/counttomark/array/astore/%28Median%29//proba/parameter/%28Median%29eq/%7B%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/-1.0/mul/:a:/ind/1/VALUE/ind/removeGRID/-1/mul/:a/4/-1/roll/mul/add/exch/div%7Dforall/counttomark/dup/1/add/1/roll/counttomark/2/sub/%7Badd%7Drepeat/exch/div/nip/precip_colors/DATA/0/AUTO/RANGE//long_name/%28Most%20Likely%20Forecast%29def//units//mm/def/dup%7D%7B%28Percentile%29//var/parameter/%28Percentile%29eq/%7Bdup/0/get/aload/pop/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a/50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul%7D%7B90.0//threshold2/parameter%7Difelse/exch/%7Baload/pop/nip/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/4/-1/roll/mul/add/exch/counttomark/1/sub/index/mul/add/eexp/dup/1.0/add/div%7Dforall/counttomark/1/sub/dup/1/add/1/roll/counttomark/3/sub/%7Badd%7Drepeat/exch/div//proba/get_parameter/%28exceeding%29eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown/50.0//percentile/parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue/50.0//percentile/parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse//var/get_parameter/%28Percentile%29ne/%7Bnip/correlationcolorscale/DATA/0/1/RANGE/dup%7Dif//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/3/-1/roll/pop//percent/unitconvert%7Difelse/%7Btarget_date/contours/forecast%7Dds//name//flexfcst/def/a-/.forecast/-a-/.target_date/-a/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/grey/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Precipitation%20forecast%20issued%20%25d%5BS%5D%20%25b%5BS%5D%20%25Y%5BS%5D%29psdef//layers%5B//forecast//countries//lakes%5Dpsdef//antialias/true/psdef/S/last/plotvalue/+.gif" />
    <img class="dlauximg fcst" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/{name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/{pop}{.target_date/S/.last}ifelse}{pop}foralldatasets2/counttomark/2/div/1/sub/{dup/3/index/ge/{nip/nip}{pop/pop}ifelse}repeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/{pop}{name//target_date/eq/{pop}{S//S/get_parameter/VALUE/dup/null/eq/{pop}if}ifelse}foralldatasets2/counttomark/3/div/{3/array/astore/counttomark/1/roll}repeat/counttomark/array/astore/(Median)//proba/parameter/(Median)/eq/{{aload/pop/nip/a%3A/ind/2/VALUE/ind/removeGRID/%3Aa%3A/ind/0/VALUE/ind/removeGRID/-1.0/mul/%3Aa%3A/ind/1/VALUE/ind/removeGRID/-1/mul/%3Aa/4/-1/roll/mul/add/exch/div}forall/counttomark/dup/1/add/1/roll/counttomark/2/sub/{add}repeat/exch/div/nip/precip_colors/DATA/0/AUTO/RANGE//long_name/(Most Likely Forecast)/def//units//mm/def/dup}{/(Percentile)//var/parameter/(Percentile)/eq/{dup/0/get/aload/pop/pop/nip/a%3A/ind/2/VALUE/ind/removeGRID/%3Aa%3A/ind/0/VALUE/ind/removeGRID/%3Aa/50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul}{90.0//threshold2/parameter}ifelse/exch/{aload/pop/nip/a%3A/ind/2/VALUE/ind/removeGRID/%3Aa%3A/ind/0/VALUE/ind/removeGRID/%3Aa%3A/ind/1/VALUE/ind/removeGRID/%3Aa/4/-1/roll/mul/add/exch/counttomark/1/sub/index/mul/add/eexp/dup/1.0/add/div}forall/counttomark/1/sub/dup/1/add/1/roll/counttomark/3/sub/{add}repeat/exch/div//proba/get_parameter/(exceeding)/eq/{-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown/50.0//percentile/parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap}{startcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue/50.0//percentile/parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap}ifelse//var/get_parameter/(Percentile)/ne/{nip/correlationcolorscale/DATA/0/1/RANGE/dup}if//long_name/(Probability of )//proba/get_parameter/append/def/3/-1/roll/pop//percent/unitconvert%7Difelse/{target_date/contours/forecast}ds//name//flexfcst/def/.forecast/fig-/colorscale/-fig/+.gif" />
  </fieldset>
  
  <fieldset class="dlimage ver2" id="iqr">
<a rel="iridl:hasFigure" href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7B/name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7B/pop/%7D%7B/.target_date/S/.last/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7B/name//paramfcst2/ne/%7B/pop/%7D%7B/.precip/S//S/get_parameter/VALUE/ind/2/VALUE/9./ln/exch/div/dup/null/eq/%7Bpop%7Dif/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/dup/1/add/1/roll/counttomark/2/sub/%7Badd%7Drepeat/exch/div/DATA/0/AUTO/RANGE//name//IQR/def//long_name/%28Uncertainty%20-%20IQR%29/def//units//mm/def/nip/exch/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Precipitation%20forecast%20issued%20%25d%5BS%5D%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//antialias/true/psdef//layers%5B//IQR//countries//lakes%5Dpsdef/S/last/plotvalue/">visit site</a>
<img class="dlimg iqr" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7B/name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7B/pop/%7D%7B/.target_date/S/.last/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7B/name//paramfcst2/ne/%7B/pop/%7D%7B/.precip/S//S/get_parameter/VALUE/ind/2/VALUE/9./ln/exch/div/dup/null/eq/%7Bpop%7Dif/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/dup/1/add/1/roll/counttomark/2/sub/%7Badd%7Drepeat/exch/div/DATA/0/AUTO/RANGE//name//IQR/def//long_name/%28Uncertainty%20-%20IQR%29/def//units//mm/def/nip/exch/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Precipitation%20forecast%20issued%20%25d%5BS%5D%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//antialias/true/psdef//layers%5B//IQR//countries//lakes%5Dpsdef/S/last/plotvalue/ind/last/plotvalue+.gif" />
<img class="dlauximg iqr" src="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/mark/ELR_coeffs/%7B/name/dup//fcstmean/eq/1/index//paramfcst2/eq/or/1/index//paramclim/eq/or/exch//ELR_coeffs/eq/or/%7B/pop/%7D%7B/.target_date/S/.last/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/2/div/1/sub/%7Bdup/3/index/ge/%7Bpop/pop%7D%7Bnip/nip%7Difelse%7Drepeat/S/exch/cvsunits//S/parameter/VALUE/exch/ELR_coeffs/%7B/name//paramfcst2/ne/%7B/pop/%7D%7B/.precip/S//S/get_parameter/VALUE/ind/2/VALUE/9./ln/exch/div/dup/null/eq/%7Bpop%7Dif/%7Difelse/%7D%7B/pop/%7D/foralldatasets2/counttomark/dup/1/add/1/roll/counttomark/2/sub/%7Badd%7Drepeat/exch/div/DATA/0/AUTO/RANGE//name//IQR/def//long_name/%28Uncertainty%20-%20IQR%29/def//units//mm/def/nip/exch/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL_coarse//name//2nd_order/def/.the_geom/:a:/.firstOrder_GAUL_coarse//name//1st_order/def/.the_geom/:a:/.Countries_GAUL_coarse//name//countries/def/.the_geom/:a/X/Y/fig-/colors/plotlabel/black/stroke/stroke/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Precipitation%20forecast%20issued%20%25d%5BS%5D%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//antialias/true/psdef//layers%5B//IQR//countries//lakes%5Dpsdef/S/last/plotvalue/ind/last/plotvalue+.auxfig/+.gif" />
</fieldset>
  
 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title">Pronóstico Semanal Flexible de Precipitación</h2>

<p align="left" property="term:description">
  Este sistema de pronóstico subestacional consiste en pronósticos probabilísticos de precipitación basados en la distribución de probabilidad.
</p>
<p>
  Los pronósticos subestacionales probabilísticos de un ensamble de múltiples modelos brindan información confiable a la comunidad de pronóstico. El ensamble de múltiples modelos usan <span property="wn30:lexicalForm"  rel="wn30:hasSense" resource="[wn30:wordsense-statistical_recalibration-noun-1]">recalibración estadística</span>, basada en el desempeño histórico de esos modelos. El uso flexible de las distribuciones de probabilidad permite la entrega de mapas interactivos y distribuciones puntuales que se vuelven relevantes para las necesidades determinadas por el usuario.
</p><p>
  El mapa de “Most Likely Forecast [mm]" a la derecha aparece el pronóstico de precipitación semanal más probable (mediana) en mm como colores.
</p><p>
  El mapa “IQR [mm]" a la izquierda ilustra la incertidumbre asociada con la mediana. El mapa “IQR [mm]” aparece solo cuando el pronóstico se muestra en términos de la mediana (más información sobre los diferentes pronósticos está abajo en la sección sobre los <i>controles</i>).
</p><p>
Controles (ubicados en la parte arriba de la página):
</p><p>
  Pronóstico Hecho en / Valido para: Antes del 1 de mayo de 2021, el período climatológico es 1999-2014; Después del 1 de mayo de 2021, el período climatológico es 1999-2016. Lo que hace que el pronóstico sea flexible es que subyacente al mapa predeterminado se encuentra la distribución de probabilidad completa para el pronóstico y la climatología. Por lo tanto, el usuario puede especificar el percentil histórico o un valor cuantitativo (aquí precipitación en mm/semana) para la probabilidad de excedencia o no excedencia.
</p><p>
  Pronóstico:
</p><p>
  <ul>
  <li>“Mediana” para aparecer el pronóstico de precipitación semanal más probable en mm como colores.  El mapa “IQR [mm]" a la izquierda ilustra la incertidumbre asociada con la mediana. El mapa “IQR [mm]” aparece solo cuando el pronóstico se muestra en términos de la mediana, ya que las otras opciones probabilísticas contienen la información de incertidumbre en sí mismas.</li>
  <li>“Probabilidad de excedencia” y “Probabilidad de no-excedendica” son para mostrar la probabilidad (colores entre 0 y 100) de un umbral dado la precipitación (como colores) o de superar o no superar un percentil de la distribución de la climatología histórica, en cuyo caso los contornos son el valor de ese percentil.</li>
  </ul>
</p><p>
  Al hacer clic en un punto del mapa, se mostrarán las funciones de distribución de probabilidad y distribución acumulativa local del pronóstico (verde) junto con la distribución climatológica (puntos negros).
</p>
</div>

<div id="tabs-2" class="ui-tabs-panel">
<h2  align="center">Documentación de los Datos</h2>
<p>
<b>Parámetros de Distribución:</b> Los parámetros ELR de la referencia climatologica observada y de los pronósticos para cade modelo están disponibles <a href="/SOURCES/.ENANDES/.SubX/.SubX_ELR_Forecast/.weekly_precip/.ELR_coeffs/" target="_blank">aquí</a>.
</p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Instrucciones</h2>
<div class="buttonInstructions"></div>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h2  align="center">Asistencia</h2>
<p>
Contáctenos <a href="mailto:help@iri.columbia.edu?subject=Flexible Forecast: Precipitation">help@iri.columbia.edu</a> con cualquiera pregunta con respecto a esta Map Room.
 </p>

</div>
</div>
<div class="optionsBar">
   <fieldset class="navitem" id="share">
<legend>Compartir</legend></fieldset> 
   <fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>

