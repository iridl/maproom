<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:wms="http://www.opengis.net/wms#" xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#" xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#" xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#" xmlns:xs="http://www.w3.org/2001/XMLSchema#" version="XHTML+RDFa 1.0" xml:lang="es">

  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta property="maproom:Sort_Id" content="a02" />
    <title>Capacidad predictiva del pronóstico retrospectivo semanal</title>

    <link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
    <link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />

    <link class="share" rel="canonical" href="subx_skill_precip_weekly.html" />
    <link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
    <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />

    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Skill_term" />
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term" />
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Colombia"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Peru"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Chile"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#subseasonal"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#weekly"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#skill"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Probability"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#IRI"/>

    <link rel="term:icon" href="/SOURCES/.ENANDES/.SubX/.SubX_Hindcast/.Subx_verification/.monthly_RPSS_weekly_precip/%28.%29/%28week1%29//range/parameter/append/interp/%28.%29/%28MME_v21%29//model/parameter/append/interp/%28.%29/%28RPSS%29//skillscore/parameter/append/interp//name//skillscore/def/a-/-a/X/Y/fig-/colors/coasts_gaz/countries_gaz/-fig+/T/0.5/plotvalue//plotaxislength+432+psdef//plotborder+0+psdef//color_smoothing+1+psdef/+.gif" />

    <script type="text/javascript" src="/uicore/uicore.js"></script>
    <script type="text/javascript" src="/localconfig/ui.js"></script>
    <script type="text/javascript" src="/uicore/insertui.js"></script>

    <style>
      sup { vertical-align:super; font-size:50%; }
    </style>
  </head>

  <body xml:lang="es">
    <form name="pageform" id="pageform">
      <input class="carryup carry" name="Set-Language" type="hidden" />
      <input class="carryup carry dlimg share" name="bbox" type="hidden" />
      <input class="carry dlimg share" name="S" type="hidden" />
      <input class="dlimg share carry" name="L" type="hidden" data-default="21.0" />
      <input class="dlimg share" name="model" type="hidden"  data-default="MME_v21" />
      <input class="dlimg dlauximg share" name="skillscore" type="hidden" data-default="RPSS" />
    </form>

    <div class="controlBar">
      <fieldset class="navitem" id="toSectionList">
        <legend>Maprooms</legend>
        <a rev="section" class="navlink carryup" href="/maproom/ENANDES/">ENANDES</a>
      </fieldset>

      <fieldset class="navitem" id="chooseSection">
        <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENANDES_SubX_Skill_term"><span property="term:label">Capacidad predictiva del pronóstico retrospectivo SubX</span></legend>
      </fieldset>

            <fieldset class="navitem">
                <legend>Región</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/enandesregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Andes</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

      <fieldset class="navitem valid">
        <legend>Valido para</legend>
        <select class="pageformcopy" name="L">
        <option value="7.0">semana 1</option>
        <option value="14.0">semana 2</option>
        <option value="21.0">semana 3</option>
        <option value="28.0">semana 4</option>
        </select>
      </fieldset>

      <fieldset class="navitem valid">
        <legend>Modelo</legend>
        <select class="pageformcopy" name="model">
        <option value="MME_v21">MME</option>
        <option value="ESRL_v21">ESRL</option>
        <option value="CFSv2_v21">CFSv2</option>
        <option value="GEFSv12">GEFSv12</option>
        </select>
      </fieldset>

      <fieldset class="navitem valid">
        <legend>Indice de capacidad predictiva</legend>
        <select class="pageformcopy" name="skillscore">
        <option value="RPSS">RPSS</option>
        </select>
      </fieldset>

    </div>

    <div class="ui-tabs">
      <ul class="ui-tabs-nav">
        <li><a href="#tabs-1" >Descripción</a></li>
        <li><a href="#tabs-2" >Documentación de los Datos</a></li>
        <li><a href="#tabs-4" >Instrucciones</a></li>
        <li><a href="#tabs-5" >Contáctenos</a></li>
      </ul>

<fieldset class="dlimage">
<a rel="iridl:hasFigure" href="/SOURCES/.ENANDES/.SubX/.SubX_Hindcast/.Subx_verification/.monthly_RPSS_weekly_precip/21.0//L/parameter/7.0/eq/%7B/.week1%7D/%7B/21.0//L/parameter/14./eq/%7B/.week2%7D/%7B/21.0//L/parameter/21./eq/%7B/.week3%7D/%7B/21.0//L/parameter/28./eq/%7B/.week4%7D/if/%7D/ifelse/%7D/ifelse/%7D/ifelse/%28.%29/%28MME_v21%29//model/parameter/append/interp/%28.%29/%28RPSS%29//skillscore/parameter/append/interp/T//long_name/%28Month%20Forecast%20Issued%29/def//S/renameGRID//name//skillscore/def/a-/-a/X/Y/fig-/colors/coasts_gaz/countries_gaz/-fig/S/0.5/plotvalue/">visit site</a>
<img class="dlimg" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_Hindcast/.Subx_verification/.monthly_RPSS_weekly_precip/21.0//L/parameter/7.0/eq/%7B/.week1%7D/%7B/21.0//L/parameter/14./eq/%7B/.week2%7D/%7B/21.0//L/parameter/21./eq/%7B/.week3%7D/%7B/21.0//L/parameter/28./eq/%7B/.week4%7D/if/%7D/ifelse/%7D/ifelse/%7D/ifelse/%28.%29/%28MME_v21%29//model/parameter/append/interp/%28.%29/%28RPSS%29//skillscore/parameter/append/interp/T//long_name/%28Month%20Forecast%20Issued%29/def//S/renameGRID//name//skillscore/def/a-/-a/X/Y/fig-/colors/coasts_gaz/countries_gaz/-fig/S/0.5/plotvalue/+.gif" />
<img class="dlauximg" rel="iridl:hasFigureImage" src="/SOURCES/.ENANDES/.SubX/.SubX_Hindcast/.Subx_verification/.monthly_RPSS_weekly_precip/21.0//L/parameter/7.0/eq/%7B/.week1%7D/%7B/21.0//L/parameter/14./eq/%7B/.week2%7D/%7B/21.0//L/parameter/21./eq/%7B/.week3%7D/%7B/21.0//L/parameter/28./eq/%7B/.week4%7D/if/%7D/ifelse/%7D/ifelse/%7D/ifelse/%28.%29/%28MME_v21%29//model/parameter/append/interp/%28.%29/%28RPSS%29//skillscore/parameter/append/interp/T//long_name/%28Month%20Forecast%20Issued%29/def//S/renameGRID//name//skillscore/def/a-/-a/X/Y/fig-/colors/coasts_gaz/countries_gaz/-fig/S/0.5/plotvalue/+.auxfig/+.gif" />
</fieldset>

      <div id="tabs-1" class="ui-tabs-panel" about="">
        <h2 class="titre1" property="term:title">Capacidad predictiva del pronóstico retrospectivo semanal</h2>

        <p property="term:description">
          La capacidad predictiva subestacional se basa en el rendimiento histórico de cada modelo SubX calibrado y su ensamble de 3 modelos.
        </p>

        <p>
          La capacidad predictiva se asigna por mes calendario para cada semana. La semana 1 corresponde a los días 2 a 8 siguientes a la emisión del pronóstico. La semana 2 a los días 9 a 15 siguientes a la emisión del pronóstico, La semana 3 a los días 16 a 22 siguientes a la emisión del pronóstico y la semana 4 a los días 23 a 29 siguientes a la emisión del pronóstico. La medida o puntaje (o score en inglés) de la capacidad predictiva de los pronósticos combinan los inicios de los meses calendario entre los años 1999 a 2016.
        </p>
        <p>
          Estos mapas de diagnóstico de medidas o puntajes de la capacidad predictiva dan una idea de dónde y cuándo (emitidos en qué meses del año y para qué semanas) los pronósticos subestacionales pueden tener  potencial para proporcionar información útil.
        </p>
        <p>
        <b>Definiciones de la métrica de capacidad predictiva :</b>
        <ul>
        <li><b>RPSS</b> : Ranked Probability Skill Score (RPSS por sus siglas en inglés) es un índice de medida de la capacidad predictiva(RPSS; Epstein (1969); Murphy (1969, 1971); Weigel et al. (2007)) que cuantifica en qué medida mejoran las predicciones de categoría de terciles calibradas en comparación con las frecuencias climatológicas. Los valores de RPSS tienden a ser pequeños, incluso para pronósticos con alta capacidad predictiva. La relación entre RPSS y correlación es tal que un valor de RPSS de 0,1 corresponde a una correlación de alrededor de 0,44 (Tippett et al. 2010).</li>
        </ul>
        </p>
        <p>
        <b>Referencias:</b>
        <ul>
        <li>Epstein, E.S., 1969: <a href="https://journals.ametsoc.org/doi/abs/10.1175/1520-0450%281969%29008%3C0985%3AASSFPF%3E2.0.CO%3B2">A Scoring System for Probability Forecasts of Ranked Categories.</a> J. Appl. Meteor., 8, 985–987</li>
        <li>Murphy, A.H., 1969: <a href="https://journals.ametsoc.org/doi/abs/10.1175/1520-0450%281969%29008%3C0988%3AOTPS%3E2.0.CO%3B2">On the “Ranked Probability Score”</a>. J. Appl. Meteor., 8, 988–989</li>
        <li>Murphy, A.H., 1971: <a href="https://journals.ametsoc.org/doi/abs/10.1175/1520-0450%281971%29010%3C0155%3AANOTRP%3E2.0.CO%3B2">A Note on the Ranked Probability Score.</a> J. Appl. Meteor., 10, 155–156</li>
        <li>Tippett, M.K., A.G. Barnston, and T. DelSole, 2010: <a href="https://journals.ametsoc.org/doi/abs/10.1175/2009MWR3214.1">Comments on “Finite Samples and Uncertainty Estimates for Skill Measures for Seasonal Prediction”.</a> Mon. Wea. Rev., 138, 1487–1493</li>
        <li>Weigel, A.P., M.A. Liniger, and C. Appenzeller, 2007: <a href="https://journals.ametsoc.org/doi/abs/10.1175/MWR3280.1">The Discrete Brier and Ranked Probability Skill Scores.</a> Mon. Wea. Rev., 135, 118–124</li>
        </ul>
        </p>
      </div>

      <div id="tabs-2" class="ui-tabs-panel">
        <h2 class="titre1">Documentación de los Datos</h2>
        <p><b>Pronósticos de capacidades predictivas :</b> Global 1˚ Multi-Model Ensemble pronostica medidas de habilidades por mes del año durante el período 1999-2016 disponible <a href="http://.iridl.ldeo.columbia.edu/SOURCES/.ENANDES/.SubX/.SubX_Hindcast/.Subx_verification/.monthly_RPSS_weekly_precip/" target="_blank">aquí</a>.</p>
      </div>

      <div id="tabs-4"  class="ui-tabs-panel">
        <h2 class="titre1">Instrucciones</h2>
        <p style="padding-right:25px;">
          <div class="buttonInstructions bis"></div>
        </p>
      </div>

      <div id="tabs-5"  class="ui-tabs-panel">
        <h2 class="titre1">Asistencia</h2>
        <p class="p1">Contáctenos <a href="mailto:help@iri.columbia.edu?subject=SubX skill score">help@iri.columbia.edu</a> con cualquiera pregunta con respecto a esta Map Room.</p>
      </div>
    </div>

    <div class="optionsBar">
      <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
      <fieldset class="navitem langgroup" id="contactus"></fieldset>
    </div>

  </body>
</html>
