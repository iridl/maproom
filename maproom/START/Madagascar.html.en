<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
 xml:lang="en"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta property="maproom:Sort_Id" content="c02" />
<title>Madagascar Hazards Information Resources</title>
<link rel="stylesheet" type="text/css" href="../../../uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link rel="stylesheet" type="text/css" href="startnet.css" />
<!-- <link class="altLanguage" rel="alternate" hreflang="es" href="Madagascar.html?Set-Language=es" /> -->
<link rel="canonical" href="Madagascar.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/maproom/START/#CountryPages" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Hazards" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<!-- <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast" /> -->
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
<!-- <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/> -->
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#START_term"/>
<!-- <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global"/> -->
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Madagascar"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#Madagascar_94_1"/>

<!--
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg2.5x2.5"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Probability"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#verification"/>
-->
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#IRI"/>
<!-- <link rel="term:icon" href="skillmap-small.jpg" />  -->
<script type="text/javascript" src="../../../uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
</head>
<body>
<form name="pageform" id="pageform" class="carryLanguage carryup carry share">
<input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList"> 
                <legend>START Network</legend> 
                      <a rev="section" class="navlink carryup" href="/maproom/START/">Forecasts</a>
            </fieldset> 
            <fieldset class="navitem" id="chooseSection"> 
                <legend about="http://iridl.ldeo.columbia.edu/maproom/START/#CountryPages"><span property="term:label">Country Pages</span></legend>
            </fieldset> 
 </div>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
<div id="ifrcdiv">
</div>
      <li><a href="#tabs-1" >Description</a></li>
<!--
      <li><a href="#tabs-2" >More Information</a></li>
      <li><a href="#tabs-3" >Instructions</a></li>
      <li><a href="#tabs-4" >Dataset Documentation</a></li>
-->
      <li><a href="#tabs-5" >Contact Us</a></li>
    </ul>

<!--
<fieldset class="dlimage" id="content" about="">
<img class="dlimg" src="skillmap.jpg" border="0" alt="image" />
</fieldset>
-->

 <div id="tabs-1" class="ui-tabs-panel" about="">
<h2 align="center"  property="term:title" >Madagascar Hazards Information Resources</h2>
<p align="left"><span property="term:description">The hazards listed here impact Madagascar.  This isn't an exhaustive list of hazards which impact the country.  They have been highlighted by Start Fund members because they frequently impact Madagascar.</span>  The <a href="https://startnetwork.org/start-fund">Start Fund</a> can respond to humanitarian crises caused by these hazards or to mitigate escalating risks associated with these hazards.</p>
<p align="left">Start Fund members should ensure that forecasts that inform Start Fund alerts are robust.  Good forecasts are <b>consistent</b> (they should match what the forecaster actually thinks), they are <b>valuable</b> (they are useful because they reduce uncertainty involved), they are <b>specific</b> (they aren't ambiguous), and they are of <b>high quality</b> (they can be evaluated to illustrate their quality based on false alarms and specific missed events).  Start members have highlighted the following forecasting sources for the hazards below: </p>
<p align="left">Cyclones:</p>
<ul>
 <li><a href="http://www.gdacs.org/">GDACS</a> for estimates of cyclone impact and links to forecast information</li>
 <li><a href="http://www.cyclonextreme.com">CycloneXtreme to monitor active cyclones</a></li>
 <li><a href="https://www.bngrc-mid.mg/">Malagasy Office for Disaster Management</a> for latest forecasts</li>
</ul>
<p align="left">Drought:</p>
<ul>
 <li><a href="http://fews.net/southern-africa/madagascar">FEWSNET for early warning and analysis on acute food insecurity</a></li>
 <li><a href="https://www.bngrc-mid.mg/">Malagasy Office for Disaster Management</a> for drought information</li>
 <li><a href="http://www.fao.org/giews/countrybrief/country.jsp?lang=en&amp;code=MDG">GIEWS</a> for up-to-date information on the food security situation</li>
</ul>
<p align="left">Flooding:</p>
<ul>
 <li><a href="http://www.globalfloods.eu/">GLOFAS</a> for flood forecasts</li>
 <li><a href="https://www.bngrc-mid.mg/">Malagasy Office for Disaster Management</a> for the latest forecasts</li>
 <li><a href="http://iridl.ldeo.columbia.edu/maproom/index.html">IRI Maprooms</a> to see where heavy rainfall is expected</li>
</ul>

<!--
<p align="left">The website for PAGASA, the national meteorological agency for the Philippines is available here:  <a href="https://www1.pagasa.dost.gov.ph/">PAGASA</a>. </p>
<p align="left">The World Meteorological Organization provides a list of links to the websites of national meteorological services here:  <a href="https://public.wmo.int/en/about-us/members/national-services/">National Meteorological Services</a>. </p>
-->

</div>

<!--
<div id="tabs-2"  class="ui-tabs-panel">
<h2  align="center">More Information</h2>
<p> Place more information here. </p>
</div>


<div id="tabs-3"  class="ui-tabs-panel">
<h2  align="center">How to navigate between maps</h2>
<p><i>Switch to another map:</i>
Select the dropdown menu at the top of the page, to the right of the blue “Forecasts” heading.</p>
<p><i>Return to the menu page:</i>
Click the blue link called “Forecasts” at the top left corner of the page.</p>
</div>
<div id="tabs-4" class="ui-tabs-panel">
<h2  align="center">Dataset Documentation</h2>
<p></p>
</div>
-->

<div id="tabs-5"  class="ui-tabs-panel">
<h2  align="center">Helpdesks</h2>
<p>
Contact <a href="mailto:help@iri.columbia.edu?subject=START Madagascar Hazards Information Resources">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room, for example, the forecasts not displaying or updating properly.
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>
