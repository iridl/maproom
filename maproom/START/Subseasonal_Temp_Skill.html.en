<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>How well can we predict subseasonal temperature?</title>
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>

<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" /> 
<link rel="stylesheet" type="text/css" href="startnet.css" />
<!-- <link class="altLanguage" rel="alternate" hreflang="sp" href="Climatology.html?Set-Language=en" /> -->
<link rel="canonical" href="Subseasonal_Temp_Skill.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<!-- <link rel="shortcut icon" href="/localconfig/icons/icpac.png" /> -->
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Entry_Id" content="START_Subseasonal_Temp_Skill" />
<meta property="maproom:Sort_Id" content="b06" />

<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#START_term" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/maproom/START/#SubseasonalForecasts" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#weekly"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg1.0x1.0"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Surface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#PlanetarySurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#WaterSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#SeaSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#LandSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#anomaly" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#verification"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#IRI"/>

<!--
 <link rel="term:icon" href="http://iridl.ldeo.columbia.edu/home/.mbell/.SubX/.ESRL/.FIMr1p1/.skillstats/.precip/.CHIRPSv2/.Spearman/L/1.0//leadW/parameter/VALUE/X/Y/(bb%3A-180%3A-50%3A180%3A50%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/X/Y/fig%3A/colors/black/coasts_gaz/countries_gaz/%3Afig//plotborder/0/psdef//plotaxislength/220/psdef/(antialias)/true/psdef//Spearman/-1.0/1.0/plotrange//T/0.5/plotvalue/+.gif" />
-->

<!-- 
<style>
.timescaleseasonal #monleadMenu {
display: none;
}
.timescalemonthly #seasleadMenu {
display: none;
}
</style>
-->

</head>
<body xml:lang="en">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts dlimgloc share">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="dlimg dlauximg dlimg2 share dlimgloc dlimglocclick" name="bbox" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="unused" name="plotaxislength" type="hidden" data-default="432" />
<input class="pickarea" name="resolution" type="hidden" data-default="1.0" />
<!-- <input class="carry dlimg dlauximg share bodyClass" name="timescale" type="hidden" data-default="seasonal" /> -->
<input class="dlimg share starttime" name="S" type="hidden" />
<input class="carry dlimg share leadtimeM" name="leadW" type="hidden" data-default="1" /> 
<!-- <input class="carry dlimg share leadtimeS" name="leadS" type="hidden" data-default="1.5" /> -->
<!-- <input class="carry dlimg share" name="model" type="hidden" data-default="FIMr1p1" /> -->
<input class="carry dlimg dlauximg share" name="var" type="hidden" data-default="pr" />
</form>

<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>START Network</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/START/">Forecasts</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
<!--            <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#START_Subseasonal_Forecasts_term"><span property="term:label">Subseasonal Forecasts</span></legend> -->
                <legend about="http://iridl.ldeo.columbia.edu/maproom/START/#SubseasonalForecasts"><span property="term:label">Subseasonal Forecasts</span></legend>
            </fieldset>

        <fieldset class="navitem">
          <legend>Region</legend>
          <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregions.json"></a>
          <select class="RegionMenu" name="bbox">
            <option value="">Global</option>
            <optgroup class="template" label="Region">
              <option class="irigaz:hasPart irigaz:id@value term:label"></option>
            </optgroup>
          </select>
        </fieldset>


<!--
           <fieldset class="navitem valid">
             <legend>Timescale</legend>
             <select class="pageformcopy" name="timescale">
             <option value="seasonal">Seasonal</option>
             <option value="monthly">Monthly</option>
             </select>
           </fieldset>
-->

<!--
           <fieldset class="navitem" id="modelMenu">
            <legend>Model</legend>
            <select class="pageformcopy" name="model">
            <option value="FIMr1p1">ESRL-FIMr1p1</option>
            <option value="CCSM4">RSMAS-CCSM4</option>     
            <option value="GEFS">EMC-GEFS</option>     
            <option value="GEOS">GMAO-GEOS_V2p1</option>      
            <option value="NESM">NRL-NESM</option>      
            <option value="GEM">ECCC-GEM</option>     
            </select>
           </fieldset>
-->

<!--
           <fieldset class="navitem">
            <legend>Variable</legend>
            <select class="pageformcopy" name="var">
            <option value="tas">Temperature</option> 
            <option value="sst">SST</option> 
            <option value="pr">Precipitation</option>      
            </select>
           </fieldset>
-->

           <fieldset class="navitem valid" id="weekleadMenu">
            <legend>Weekly Lead Time</legend>
            <select class="pageformcopy" name="leadW">
            <option value="1">Week 1</option>
            <option value="2">Week 2</option>
            <option value="3">Week 3</option>
            <option value="4">Week 4</option>
            </select>
           </fieldset>
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
<!--      <li><a href="#tabs-2" >Dataset Documentation</a></li> -->
      <li><a href="/SOURCES/.Models/.SubX/.ESRL/.FIMr1p1/">Dataset</a></li>
      <li><a href="#tabs-3" >Instructions</a></li>
      <li><a href="#tabs-4" >Contact Us</a></li>
    </ul>

<fieldset class="dlimage">

<!--
<a rel="iridl:hasFigure" href="http://iridl.ldeo.columbia.edu/home/.mbell/.SubX/.ESRL/.FIMr1p1/.skillstats/.precip/.CHIRPSv2/.Spearman/L/1.0//leadW/parameter/VALUE/X/Y/(bb%3A-180%3A-50%3A180%3A50%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/X/Y/fig%3A/colors/black/coasts_gaz/countries_gaz/%3Afig//plotaxislength/550/psdef/(antialias)/true/psdef//Spearman/-1.0/1.0/plotrange//T/0.5/plotvalue/"></a>

<img class="dlimg" src="http://iridl.ldeo.columbia.edu/home/.mbell/.SubX/.ESRL/.FIMr1p1/.skillstats/.precip/.CHIRPSv2/.Spearman/L/1.0//leadW/parameter/VALUE/X/Y/(bb%3A-180%3A-50%3A180%3A50%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/X/Y/fig%3A/colors/black/coasts_gaz/countries_gaz/%3Afig//plotaxislength/550/psdef/(antialias)/true/psdef//Spearman/-1.0/1.0/plotrange//T/0.5/plotvalue/+.gif" border="0" />

<img class="dlauximg" src="http://iridl.ldeo.columbia.edu/home/.mbell/.SubX/.ESRL/.FIMr1p1/.skillstats/.precip/.CHIRPSv2/.Spearman/L/1.0//leadW/parameter/VALUE/X/Y/%28bb:-180:-50:180:50:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/X/Y/fig:/colors/black/coasts_gaz/countries_gaz/:fig//plotaxislength/550/psdef/%28antialias%29/true/psdef//Spearman/-1.0/1.0/plotrange//T/0.5/plotvalue/L/last/plotvalue+.auxfig/+.gif" />

-->

</fieldset>


 <div id="tabs-1" class="ui-tabs-panel" about="">

 <h2 align="center" property="term:title">How well can we predict subseasonal temperature?</h2>

  <p align="left" property="term:description">
These maps show the correlation between the weekly temperature forecast and observed weekly temperature values, depending upon the month of year when the forecast is made and the weekly forecast leads (1 to 4 weeks into the future).  THIS MAP PAGE IS CURRENTLY UNDER DEVELOPMENT.</p> 
 
<p align="left"> The correlation values range between -1 and +1.  A high positive correlation (toward +1) indicates that the forecast and observed temperature tend to co-vary in phase, or in agreement with each other; a high negative correlation (toward -1) indicates that the forecast and observed temperature tend to vary out of phase, or out of agreement with each other. </p>

<p align="left">  This skill measure does not give any indication of the bias of the forecast.  In other words, even if there is a high positive correlation, the difference between the forecast and observed temperature could still be quite high. </p>
  <p align="left">
Use the drop-down menus at the top of the page to select the forecast week.  Mouse over the map to select the forecast start time from a control that appears just above the map.  Select a combination of the forecast start time and the week in the future to produce a map for a target week.  </p>
<p align="left"> Keep in mind that forecasts beyond one or two weeks are still an area of active research, tend to have low skill, and should be treated with caution.  </p>
<p align="left"> PLEASE NOTE THAT THESE FORECASTS ARE EXPERIMENTAL AND ARE NOT AN OFFICIAL IRI FORECAST PRODUCT. </p>
<h4>Acknowledgments</h4>
  <p align="left">
In order to document SubX data impact and enable continuing support, users of SubX data are expected to acknowledge SubX data and the participating modeling groups. The SubX model output should be referred to as "the SubX data [http://iridl.ldeo.columbia.edu/SOURCES/.Models/.SubX/]" and referenced using the SubX DOI: 10.7916/D8PG249H. In publications, users should include a table (referred to below as Table XX) listing the models and institutions that provided model output used in the SubX system, as well as the digital object identifier of publications documenting the models. In addition, an acknowledgment similar to the following should be included in any publication: “We acknowledge the agencies that support the SubX system, and we thank the climate modeling groups (Environment Canada, NASA, NOAA/NCEP, NRL and University of Miami) for producing and making available their model output. NOAA/MAPP, ONR, NASA, NOAA/NWS jointly provided coordinating support and led development of the SubX system.” where “Table XX” in the paper should list the models and modeling groups that provided the SubX data.
  </p>
  <p align="left">
  </p>
</div>
<div class="ui-tabs-panel-hidden">
<h2 align="center">Dataset</h2>
<p>
<a href="/SOURCES/.Models/.SubX/.ESRL/.FIMr1p1/">Access the dataset used to create this map.</a>
</p>
</div>
<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">How to use this interactive map</h2>
<p><i>Return to the menu page:</i>
Click the blue link called “Forecasts” at the top left corner of the page.</p>

<div class="buttonInstructions"></div>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Helpdesks</h2>
<p>
Contact <a href="mailto:help@iri.columbia.edu?subject=START Subseasonal Temperature Forecast Skill">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.
 </p>
</div>
</div>
<div class="optionsBar">
    <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
   <fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>

