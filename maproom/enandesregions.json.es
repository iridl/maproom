{
"@context": {
"bb": "http://iridl.ldeo.columbia.edu/ontologies/gisuri/geobb/",
"irigaz": "http://iridl.ldeo.columbia.edu/ontologies/irigaz_frwk.owl#",
"term": "http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#",
"irigaz:id": { "@type": "@id" },
"irigaz:hasPart": { "@container": "@list", "@type": "irigaz:GazEntity"},
"term:label": {"@language": "en"}
},
"irigaz:hasPart": [
{ "irigaz:id": "bb:-85:-5:-65:15:bb",
  "term:label": "Colombia"
},
{ "irigaz:id": "bb:-83:-20:-67:2:bb",
  "term:label": "Perú"
},
{ "irigaz:id": "bb:-76:-56:-67:-17:bb",
  "term:label": "Chile"
}
]
}
