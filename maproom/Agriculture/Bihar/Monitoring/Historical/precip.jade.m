<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Northern India Historical Precipitation Monitoring
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='precip.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(property='maproom:Entry_Id', content='Agriculture_Historical_NorthernIndia_Precipitation')
    meta(property='maproom:Sort_Id', content='a2')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#agriculture')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Historical_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#WetDayFrequency')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Count')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#mean')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standard_deviation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.5x0.5')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.25x0.25')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average_resolution')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#India')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#India_68_1')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')
    link(rel='term:icon', href='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.precipitation/.historical/X/%2883E%29/%2888.5E%29/RANGEEDGES/Y/%2824N%29/%2828N%29/RANGEEDGES/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//antialias/true/psdef//plotborder/0/psdef//antialias/true/psdef//color_smoothing/1/psdef/+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    style.
      .dlimgtsbox {
      width: 99%;
      display: inline-block
      }
      .seasonalStatTotRain #pET2 {
      display: none;
      }
      .seasonalStatTotRain #pET3 {
      display: none;
      }
      .seasonalStatTotRain #pET4 {
      display: none;
      }
      .seasonalStatTotRain #pET5 {
      display: none;
      }
      .seasonalStatNumWD #pET1 {
      display: none;
      }
      .seasonalStatNumWD #pET3 {
      display: none;
      }
      .seasonalStatNumWD #pET4 {
      display: none;
      }
      .seasonalStatNumWD #pET5 {
      display: none;
      }
      .seasonalStatRainInt #pET1 {
      display: none;
      }
      .seasonalStatRainInt #pET2 {
      display: none;
      }
      .seasonalStatRainInt #pET4 {
      display: none;
      }
      .seasonalStatRainInt #pET5 {
      display: none;
      }
      .seasonalStatNumDS #pET1 {
      display: none;
      }
      .seasonalStatNumDS #pET2 {
      display: none;
      }
      .seasonalStatNumDS #pET3 {
      display: none;
      }
      .seasonalStatNumDS #pET5 {
      display: none;
      }
      .seasonalStatNumWS #pET1 {
      display: none;
      }
      .seasonalStatNumWS #pET2 {
      display: none;
      }
      .seasonalStatNumWS #pET3 {
      display: none;
      }
      .seasonalStatNumWS #pET4 {
      display: none;
      }
      .seasonalStat #pET2 {
      display: none;
      }
      .seasonalStat #pET3 {
      display: none;
      }
      .seasonalStat #pET4 {
      display: none;
      }
      .seasonalStat #pET5 {
      display: none;
      }
      .seasonalStatPerDA #yearlyStat {
      display: none;
      }
      .yearlyStatMean #pET1 {
      display: none;
      }
      .yearlyStatStdDev #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }
      .yearlyStatMean #pET2 {
      display: none;
      }
      .yearlyStatStdDev #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }
      .yearlyStatMean #pET3 {
      display: none;
      }
      .yearlyStatStdDev #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }
      .yearlyStatMean #pET4 {
      display: none;
      }
      .yearlyStatStdDev #pET4 {
      display: none;
      }
      .yearlyStat #pET4 {
      display: none;
      }
      .yearlyStatMean #pET5 {
      display: none;
      }
      .yearlyStatStdDev #pET5 {
      display: none;
      }
      .yearlyStat #pET5 {
      display: none;
      }
  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.dlimg.dlauximg.share.dlimgloc.dlimglocclick(name='bbox', type='hidden')
      input.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.unused(name='plotaxislength', type='hidden', data-default='432')
      input.pickarea.dlimg.dlauximg.share(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')
      input.dlimg.dlauximg.share.dlimgts(name='YearStart', type='hidden', data-default='1901')
      input.dlimg.dlauximg.share.dlimgts(name='YearEnd', type='hidden', data-default='2015')
      input.carry.dlauximg.dlimg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jan')
      input.carry.dlauximg.dlimg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Jan')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='1')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='31')
      input.dlimg.dlauximg.share.dlimgts(name='minFrac', type='hidden', data-default='0.5')
      input.dlimg.dlauximg.share.dlimgts(name='obsOnly', type='hidden', data-default='off')
      input.dlimg.dlauximg.share.dlimgts(name='wetThreshold', type='hidden', data-default='1.')
      input.dlimg.dlauximg.share.dlimgts(name='spellThreshold', type='hidden', data-default='5.')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='TotRain')
      input.dlimg.dlauximg.share.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='Mean')
      input.dlimg.share.pET(name='probExcThresh1', type='hidden', data-default='150.')
      input.dlimg.share.pET(name='probExcThresh2', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh3', type='hidden', data-default='10.')
      input.dlimg.share.pET(name='probExcThresh4', type='hidden', data-default='3.')
      input.dlimg.share.pET(name='probExcThresh5', type='hidden', data-default='3.')
    .controlBar
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
          span(property='term:label') Monitoring
      fieldset.navitem
        legend Seasons (1901 to 2015)
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='1', size='2', maxlength='2')
        input.pageformcopy(name='YearStart', type='text', value='1901', size='4', maxlength='4')
        |  to 
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='31', size='2', maxlength='2')
        input.pageformcopy(name='YearEnd', type='text', value='2015', size='4', maxlength='4')
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='TotRain') Total Rainfall
          option(value='NumWD') Number of Wet Days
          option(value='RainInt') Rainfall Intensity
          option(value='NumDS') Number of Dry Spells
          option(value='NumWS') Number of Wet Spells
          option(value='PerDA') Percentage of Data Available
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='Mean') Mean
          option(value='StdDev') Standard deviation
          option(value='PoE') Probability of exceeding
        span#pET0
          span#pET1
            input.pageformcopy(name='probExcThresh1', type='text', value='150', size='3', maxlength='3')
            | mm
          span#pET2
            input.pageformcopy(name='probExcThresh2', type='text', value='30', size='3', maxlength='3')
            | days
          span#pET3
            input.pageformcopy(name='probExcThresh3', type='text', value='10', size='2', maxlength='2')
            | mm/day
          span#pET4
            input.pageformcopy(name='probExcThresh4', type='text', value='3', size='2', maxlength='2')
            | spells
          span#pET5
            input.pageformcopy(name='probExcThresh5', type='text', value='3', size='2', maxlength='2')
            | spells
      fieldset.navitem
        legend Spatial Resolution
        select.pageformcopy(name='resolution')
          option(value='irids:SOURCES:Features:Political:India:Bihar_dist:ds') district
          option(value='0.25') 0.25˚
      fieldset.navitem
        legend Mask interpolated values
        select.pageformcopy(name='obsOnly')
          option(value='off') off
          option(value='on') on
      fieldset.navitem
        legend Seasonal Data Coverage
        | Minimum fraction of non-missing daily values:
        input.pageformcopy(name='minFrac', type='text', value='0.5', size='4', maxlength='4')
      fieldset.navitem
        legend Wet/Dry Day definition
        | Rainfall amount above/below
        input.pageformcopy(name='wetThreshold', type='text', value='1.', size='5', maxlength='5')
        | mm/day
      fieldset.navitem
        legend Wet/Dry Spell definition
        input.pageformcopy(name='spellThreshold', type='text', value='5', size='2', maxlength='2')
        | continuous wet/dry days
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Options
        li
          a(href='#tabs-3') Dataset Documentation
        li
          a(href='#tabs-4') Contact Us
      fieldset.regionwithinbbox.dlimage(about='')
        img.dlimgloc(src='/expert/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:83%2C24%2C88.5%2C28%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:85%2C26%2C85.25%2C26.25%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/india_dist/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')
        br
        .dlimgtsbox
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/X/73.5/88.5/RANGEEDGES/Y/24/33/RANGEEDGES/a:/.rf/%28off%29//obsOnly/parameter/%28on%29/eq/%7B:a:/.rstn/0.0/maskle/0.0/mul/add%7Dif/:a/T/%281901%29//YearStart/parameter/%282015%29//YearEnd/parameter/RANGEEDGES/%28TotRain%29//seasonalStat/parameter/%28PerDA%29/eq/%7BSOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/X/73.5/88.5/RANGEEDGES/Y/24/33/RANGEEDGES/.stn/0.0/maskle/dataflag/nip%7Dif/%281%29//DayStart/parameter/%28%20%29/append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29/append/%2831%29//DayEnd/parameter/append/%28%20%29/append/%28Jan%29//seasonEnd/parameter/append/%285%29//spellThreshold/parameter/interp/%281%29//wetThreshold/parameter/interp/%280.5%29//minFrac/parameter/interp//seasonalStat/get_parameter/%28TotRain%29/eq/%7Bnip/nip/flexseastotAvgFill%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B3/-1/roll/pop/flexseasonalfreqGT%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B3/-1/roll/pop/flexseasonalmeandailyvalueGT%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7BflexseasonalnonoverlapDSfreq%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7BflexseasonalnonoverlapWSfreq%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7Bpop/pop/pop/T/exch/0.0/seasonalAverage%7Dif/%28bb:%5B80%2C30%2C80.25%2C30.25%5D%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average//seasonalStat/get_parameter/%28TotRain%29/eq/%7B//long_name/%28Precipitation%29/def//units/%28mm%29/def%7Dif//seasonalStat/get_parameter/%28NumWD%29/eq/%7B//long_name/%28count%29/def%7Dif//seasonalStat/get_parameter/%28RainInt%29/eq/%7B//long_name/%28Precipitation%29/def//units/%28mm/day%29/def%7Dif//seasonalStat/get_parameter/%28NumDS%29/eq/%7B//long_name/%28count%29/def//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28NumWS%29/eq/%7B//long_name/%28count%29/def//units/%28unitless%29/def%7Dif//seasonalStat/get_parameter/%28PerDA%29/eq/%7B/%28percent%29/unitconvert//long_name/%28Availability%29/def%7Dif/T/fig-/medium/line/-fig//antialias/true/psdef//plotaxislength/432/psdef//framelabel//seasonalStat/get_parameter/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/psdef/+.gif')
      fieldset.dlimage
        a(rel='iridl:hasFigure', href='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.precipitation/.historical/X/%2883E%29/%2888.5E%29/RANGEEDGES/Y/%2824N%29/%2828N%29/RANGEEDGES/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28PerDA%29/1/index/eq/not/%7B//yearlyStat/get_parameter/%28%20of%20%29/append/exch/append%7Dif/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', src='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.precipitation/.historical/X/%2883E%29/%2888.5E%29/RANGEEDGES/Y/%2824N%29/%2828N%29/RANGEEDGES/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28PerDA%29/1/index/eq/not/%7B//yearlyStat/get_parameter/%28%20of%20%29/append/exch/append%7Dif/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/+.gif')
        img.dlauximg(rel='iridl:hasFigureImage', src='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.precipitation/.historical/X/%2883E%29/%2888.5E%29/RANGEEDGES/Y/%2824N%29/%2828N%29/RANGEEDGES/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28PerDA%29/1/index/eq/not/%7B//yearlyStat/get_parameter/%28%20of%20%29/append/exch/append%7Dif/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Weather within climate: Rainfall
        p(align='left', property='term:description')
          | Maps of wet and dry spell characteristics. Click on map to plot a yearly time series for a district.
      #tabs-2.ui-tabs-panel(about='')
        h2(align='center') Options
        p(align='left')
          b Years and Season
          | :
          | Specify the range of years over which to perform the analysis, and choose the start and end dates of the season over which the diagnostics are to be performed.
          br
          b Mask and Seasonal Data Coverage
          | :
          | The APHRODITE dataset includes a record of the number of station data observations present for each gridbox on each day of the dataset. The maproom allows this information to be used in two ways: (1) gridbox-days without at least one observation can be masked prior to any calculations (masking so called interpolated values); (2) a minimum fraction of non-masked days per season can be required in order for the seasonal diagnostic to be defined at that gridbox — if this minimum threshold is not met, then the seasonal diagnostic is assigned a missing value at that gridbox, for that season and year.
          br
          b Wet/Dry Day/Spell Definitions
          | :
          | These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Total Rainfall
          | : total cumulative precipitation (in mm) falling over the season.
          br
          b Number of Wet Days
          | : the number of wet days (as defined above) during the season.
          br
          b Rainfall Intensity
          | :
          | the average daily precipitation over the season considering only wet days.
          br
          b Number of Wet (Dry) Spells
          | :
          | the number of wet (dry) spells during the season according to the definitions in the Options section. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
          br
          b Percentage of Data Available
          | : the percentage of days with non missing values within the season.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | :
          | The analysis can performed and map at each grid point for both 0.25˚ and 0.50˚ resolution APHRODITE products. Additionally it is possible to average the results of the analysis over the 0.25˚ grid points falling within administrative boundaries for the time series graph.
      #tabs-3.ui-tabs-panel(about='')
        h2(align='center') Dataset Documentation
        p(align='left')
          b Data
          | : GRIDDED RAINFALL from IMD RF0p25 gridded daily v1901-2015: IMD 0.25 Degree Daily Gridded Precipitation Data Set. 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/') here
          | .
      #tabs-4.ui-tabs-panel
        h2(align='center') Helpdesks
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Maproom') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
