<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Northern India Historical Temperature Monitoring
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='temp.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(property='maproom:Entry_Id', content='d03')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#agriculture')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Historical_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Temperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#AirTemperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#AirTemperature2m')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#z2m')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#NearSurface')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Count')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#mean')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standard_deviation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg1x1')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average_resolution')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#India')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#India_68_1')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')
    link(rel='term:icon', href='/expert/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.temperature/.IMD_temperature/.historical/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/grey/stroke/stroke/black/countries_gaz/-fig/X/82/89/plotrange/Y/24/28/plotrange//antialias/true/psdef//color_smoothing/1/psdef//plotaxislength/152/psdef//plotborder/0/psdef/+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    style.
      .dlimgtsbox {
      width: 99%;
      display: inline-block
      }
      .seasonalStatMinTemp #pET2 {
      display: none;
      }
      .seasonalStatMinTemp #pET3 {
      display: none;
      }
      .seasonalStatMinTemp #pET4 {
      display: none;
      }
      .seasonalStatMinTemp #pET5 {
      display: none;
      }
      .seasonalStatMaxTemp #pET1 {
      display: none;
      }
      .seasonalStatMaxTemp #pET3 {
      display: none;
      }
      .seasonalStatMaxTemp #pET4 {
      display: none;
      }
      .seasonalStatMaxTemp #pET5 {
      display: none;
      }
      .seasonalStatHDD #pET1 {
      display: none;
      }
      .seasonalStatHDD #pET2 {
      display: none;
      }
      .seasonalStatHDD #pET4 {
      display: none;
      }
      .seasonalStatHDD #pET5 {
      display: none;
      }
      .seasonalStatNumCD #pET1 {
      display: none;
      }
      .seasonalStatNumCD #pET2 {
      display: none;
      }
      .seasonalStatNumCD #pET3 {
      display: none;
      }
      .seasonalStatNumCD #pET5 {
      display: none;
      }
      .seasonalStatNumHD #pET1 {
      display: none;
      }
      .seasonalStatNumHD #pET2 {
      display: none;
      }
      .seasonalStatNumHD #pET3 {
      display: none;
      }
      .seasonalStatNumHD #pET4 {
      display: none;
      }
      .seasonalStat #pET2 {
      display: none;
      }
      .seasonalStat #pET3 {
      display: none;
      }
      .seasonalStat #pET4 {
      display: none;
      }
      .seasonalStat #pET5 {
      display: none;
      }
      .yearlyStatMean #pET1 {
      display: none;
      }
      .yearlyStatStdDev #pET1 {
      display: none;
      }
      .yearlyStat #pET1 {
      display: none;
      }
      .yearlyStatMean #pET2 {
      display: none;
      }
      .yearlyStatStdDev #pET2 {
      display: none;
      }
      .yearlyStat #pET2 {
      display: none;
      }
      .yearlyStatMean #pET3 {
      display: none;
      }
      .yearlyStatStdDev #pET3 {
      display: none;
      }
      .yearlyStat #pET3 {
      display: none;
      }
      .yearlyStatMean #pET4 {
      display: none;
      }
      .yearlyStatStdDev #pET4 {
      display: none;
      }
      .yearlyStat #pET4 {
      display: none;
      }
      .yearlyStatMean #pET5 {
      display: none;
      }
      .yearlyStatStdDev #pET5 {
      display: none;
      }
      .yearlyStat #pET5 {
      display: none;
      }
  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.dlimg.dlauximg.share.dlimgloc.dlimglocclick(name='bbox', type='hidden')
      input.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.unused(name='plotaxislength', type='hidden', data-default='432')
      input.pickarea.dlimg.dlauximg.share(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')
      input.dlimg.dlauximg.share.dlimgts(name='YearStart', type='hidden', data-default='1951')
      input.dlimg.dlauximg.share.dlimgts(name='YearEnd', type='hidden', data-default='2015')
      input.carry.dlauximg.dlimg.share.dlimgts(name='seasonStart', type='hidden', data-default='Jan')
      input.carry.dlauximg.dlimg.share.dlimgts(name='seasonEnd', type='hidden', data-default='Jan')
      input.dlimg.dlauximg.share.dlimgts(name='DayStart', type='hidden', data-default='1')
      input.dlimg.dlauximg.share.dlimgts(name='DayEnd', type='hidden', data-default='31')
      input.dlimg.dlauximg.share.dlimgts(name='hotThreshold', type='hidden', data-default='0.')
      input.dlimg.dlauximg.share.dlimgts.pET.ylyStat.bodyClass(name='seasonalStat', type='hidden', data-default='MinTemp')
      input.dlimg.dlauximg.share.ylyStat.pET.bodyClass(name='yearlyStat', type='hidden', data-default='Mean')
      input.dlimg.share.pET(name='probExcThresh1', type='hidden', data-default='0.')
      input.dlimg.share.pET(name='probExcThresh2', type='hidden', data-default='0.')
      input.dlimg.share.pET(name='probExcThresh3', type='hidden', data-default='100.')
      input.dlimg.share.pET(name='probExcThresh4', type='hidden', data-default='30.')
      input.dlimg.share.pET(name='probExcThresh5', type='hidden', data-default='30.')
    .controlBar
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Hist_Temp_Bihar_Monitoring_term')
          span(property='term:label') Monitoring
      fieldset.navitem
        legend Seasons
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayStart', type='text', value='1', size='2', maxlength='2')
        input.pageformcopy(name='YearStart', type='text', value='1951', size='4', maxlength='4')
        |  to 
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        input.pageformcopy(name='DayEnd', type='text', value='31', size='2', maxlength='2')
        input.pageformcopy(name='YearEnd', type='text', value='2015', size='4', maxlength='4')
      fieldset.navitem
        legend Seasonal daily statistics
        select.pageformcopy(name='seasonalStat')
          option(value='MinTemp') Minimum Temperature
          option(value='MaxTemp') Maximum Temperature
          //option(value='HDD') Heating Degree Days
          option(value='NumCD') Number of Cold Days
          option(value='NumHD') Number of Hot Days
      fieldset#yearlyStat.navitem
        legend Yearly seasonal statistics
        select.pageformcopy(name='yearlyStat')
          option(value='Mean') Mean
          option(value='StdDev') Standard deviation
          option(value='PoE') Probability of exceeding
        span#pET0
          span#pET1
            input.pageformcopy(name='probExcThresh1', type='text', value='0', size='3', maxlength='3')
            | ˚C
          span#pET2
            input.pageformcopy(name='probExcThresh2', type='text', value='0', size='3', maxlength='3')
            | ˚C
          span#pET3
            input.pageformcopy(name='probExcThresh3', type='text', value='100', size='4', maxlength='4')
            | ˚C.days
          span#pET4
            input.pageformcopy(name='probExcThresh4', type='text', value='30', size='2', maxlength='2')
            | days
          span#pET5
            input.pageformcopy(name='probExcThresh5', type='text', value='30', size='2', maxlength='2')
            | days
      fieldset.navitem
        legend Spatial Resolution
        select.pageformcopy(name='resolution')
          option(value='irids:SOURCES:Features:Political:India:Bihar_dist:ds') district
          option(value='1.0') 1.0˚
      fieldset.navitem
        legend Hot/Cold Day definition
        | Temperature above/below
        input.pageformcopy(name='hotThreshold', type='text', value='0.', size='5', maxlength='5')
        | ˚C
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Options
        li
          a(href='#tabs-3') Dataset Documentation
        li
          a(href='#tabs-4') Contact Us
      fieldset.regionwithinbbox.dlimage(about='')
        img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:83%2C24%2C88.5%2C28%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:85%2C26%2C85.25%2C26.25%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/india_dist/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json')
          .template
            | Observations for 
            span.bold(class='iridl:long_name')
        br
        .dlimgtsbox
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='/SOURCES/.IMD/.HRDGT/.v1951-2015/X/73.5/88.5/RANGEEDGES/Y/24/33/RANGEEDGES/T/%281951%29//YearStart/parameter/%282015%29//YearEnd/parameter/RANGEEDGES/%281%29//DayStart/parameter/%28%20%29append/%28Jan%29//seasonStart/parameter/append/%28%20-%20%29append/%2831%29//DayEnd/parameter/append/%28%20%29append/%28Jan%29//seasonEnd/parameter/append/%280%29//hotThreshold/parameter/interp/0.0/%28MinTemp%29//seasonalStat/parameter/%28MinTemp%29eq/%7Bnip/3/-1/roll/.tmin/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/%28MaxTemp%29eq/%7Bnip/3/-1/roll/.tmax/T/4/-2/roll/seasonalAverage%7Dif//seasonalStat/get_parameter/%28HDD%29eq/%7B4/-1/roll/.tmean/3/-1/roll/sub/0/maskge/abs/3/-2/roll/flexseastotZeroFill%7Dif//seasonalStat/get_parameter/%28NumCD%29eq/%7B4/-1/roll/.tmin/4/-3/roll/flexseasonalfreqLT%7Dif//seasonalStat/get_parameter/%28NumHD%29eq/%7B4/-1/roll/.tmax/4/-3/roll/flexseasonalfreqGT%7Dif/X/73.5/0.5/88.5/GRID/Y/24.0/0.5/33.0/GRID/%28bb:%5B80%2C26%2C81%2C27%5D%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//seasonalStat/get_parameter/%28MinTemp%29eq/%7B//long_name/%28Minimum%20Temperature%29def%7Dif//seasonalStat/get_parameter/%28MaxTemp%29eq/%7B//long_name/%28Maximum%20Temperature%29def%7Dif//seasonalStat/get_parameter/%28HDD%29eq/%7B//long_name/%28Heating%20Degree%20Days%29def%7Dif//seasonalStat/get_parameter/dup/%28NumCD%29eq/exch/%28NumHD%29eq/or/%7B//long_name/%28count%29def%7Dif/T/fig-/medium/line/-fig//antialias/true/psdef//plotaxislength/432/psdef//framelabel//seasonalStat/get_parameter/%28%20in%20season%20%29append/lpar/append//seasonStart/get_parameter/append/%28%20%29append//DayStart/get_parameter/append/%28%20-%20%29append//seasonEnd/get_parameter/append/%28%20%29append//DayEnd/get_parameter/append/rpar/append/psdef/+.gif')
      fieldset.dlimage
        a(rel='iridl:hasFigure', href='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.temperature/.IMD_temperature/.historical/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/grey/stroke/stroke/black/countries_gaz/-fig/X/82/89/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20of%20%29/append//seasonalStat/get_parameter/append/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', src='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.temperature/.IMD_temperature/.historical/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/grey/stroke/stroke/black/countries_gaz/-fig/X/82/89/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20of%20%29/append//seasonalStat/get_parameter/append/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/+.gif')
        img.dlauximg(rel='iridl:hasFigureImage', src='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.temperature/.IMD_temperature/.historical/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/grey/stroke/stroke/black/countries_gaz/-fig/X/82/89/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//yearlyStat/get_parameter/%28%20of%20%29/append//seasonalStat/get_parameter/append/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef/+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Weather within climate: Temperature
        p(align='left', property='term:description')
          | Maps of hot, cold and growing degree day characteristics. Click on map to plot a yearly time series for a district.
      #tabs-2.ui-tabs-panel(about='')
        h2(align='center') Options
        p(align='left')
          b Years and Season
          | :
          | Specify the range of years over which to perform the analysis, and choose the start and end dates of the season over which the diagnostics are to be performed.
          br
          b Hot/Cold Day Definition
          | :
          | This threshold is used to define in Celsius degrees (non inclusive) the temperature under which a day is considered cold if the minimum temperature is below it; or the temperature above which a days is considered hot if the maximum temperature is above it; or the reference temperature for to compute the heating degree days.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Minimum and Maximum Temperature
          | : average minimum or maximum daily temperature over the season.
          br
          //b Heating Degree Days
          //| : Heating degree days are summations of negative differences between the mean daily temperature and user-defined reference base temperature during the season.
          //br
          b Number of Cold and Hot days
          | :
          | the number of cold or hot days during the season according to the user-defined threshold and according respectively to the minumim temperature and the maximum temperature.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | :
          | The analysis is performed and mapped at each grid point. Additionally it is possible to average the results of the analysis over the 1˚ grid points falling within administrative boundaries for the time series graph.
      #tabs-3.ui-tabs-panel(about='')
        h2(align='center') Dataset Documentation
        p(align='left')
          b Data
          | : India Meteorological Department (IMD) High Resolution Daily Gridded Temperature Data Set 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IMD/.HRDGT/.v1951-2015/') here
          | .
      #tabs-4.ui-tabs-panel
        h2(align='center') Helpdesks
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Maproom') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
