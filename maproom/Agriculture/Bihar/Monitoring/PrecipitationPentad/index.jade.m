<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
<<<en:
  - var title = "Pentad Precipitation by District";
|||es:
  - var title = "Precipitación promedio decadal por Municipio";
>>>
  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')


    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    link.carryLanguage(rel='home', href='http://irapclimate.org/', title='IRAP')
    link.carryLanguage(rel='home alternate', type='application/json', href='/localconfig/navmenu.json')

    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Example')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')

    link(rel='term:icon', href='/home/.mbell/.IMD/.daily/.RT_0p25/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/T/0.0/pentadAverage/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/stroke/lightgrey/medium/countries_gaz/-fig//framelabelstart/%28Rainfall%20on%20%29/psdef//antialias/true/psdef//T/last/plotvalue+//plotaxislength+432+psdef//framelabelstart+%28Rainfall%20on%20%29+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    title #{title}
    meta(xml:lang='', property='maproom:Entry_Id', content='RT_pentad_Monitoring')
    link.share(rel='canonical', href='<<<canonical>>>')

  body(xml:lang='en')
    form#pageform.carryLanguage.carryup.carry.dlimg.dlauximg.share(name='pageform')
      input.carryLanguage.carryup.carry(name='Set-Language', type='hidden')
      input.carry.dlimg.share.dlimgloc.dlimglocclick(name='bbox', type='hidden')
      input.dlimg.dlauximg.share.cumulOpt.bodyClass(name='monitAna', type='hidden', data-default='rainfall')
      input.dlimg.share(name='T', type='hidden')
      input.dlimg.dlauximg(name='plotaxislength', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden')
      input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.carry.pickarea(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')
      input.share.dlimgts.cumulOpt(name='startDay', type='hidden', data-default='1 Jan')
    .controlBar
      fieldset#toSectionList.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
          span(property='term:label') Monitoring
      //fieldset.navitem
        legend Region
        a.carryLanguage(rel='iridl:hasJSON', href='/maproom/rwandaregions.json')
        select.RegionMenu(name='bbox')
          option(value='') Rwanda
          optgroup.template(label='Region')
            option(class='irigaz:hasPart irigaz:id@value term:label')
      fieldset.navitem
        legend Spatially Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='irids:SOURCES:Features:Political:India:Bihar_dist:ds') District
          option(value='irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds') Province
          option(value='.25') gridpoint
      //fieldset.navitem
        legend Analysis
        select.pageformcopy(name='monitAna')
          option(value='rainfall') Rainfall Amounts
          option(value='cumul') Cumulative Anomalies
          option(value='anom') Rainfall Anomalies
          option(value='spi') SPI
      //fieldset#startDay.navitem
        legend Cumulative Rainfall Anomaly since
        input.pageformcopy(name='startDay', type='text', value='1 Jan', size='14', maxlength='14')
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='http://iridl.ldeo.columbia.edu/home/.mbell/.IMD/.daily/.RT_0p25/.rf/#info') Dataset
        li
          a(href='#tabs-3') Instructions
        li
          a(href='#tabs-4') Contact Us

      fieldset.regionwithinbbox.dlimage.ver3(about='')
        -
           s0 = `
           SOURCES .WORLDBATH .bath X Y (bb:83.0,24,88.5,28) /bbox parameter geoobject geoboundingbox BOXEDGES
           SOURCES .Features .Political .India .Bihar_dist .the_geom exch (bb:86,26,86.25,26.25) dup /region parameter dup
           /bbox get_parameter eq {pop}{nip}ifelse geoobject 2 copy rasterize Y cosd mul X 1 index
           [X Y] weighted-average exch Y exch [X Y]weighted-average 5 4 roll 4 1 roll
           X Y fig- lightgrey mask grey stroke red fill red smallpushpins -fig
           /plotbordertop 8 psdef
           /antialias true psdef
           /plotaxislength 120 psdef
           /plotborder 0 psdef
           /plotborderleft 8 psdef
           /plotborderright 8 psdef
           /plotborderbottom 8 psdef
           `;

        img.dlimgloc(src='#{encodeIngrid(s0,"+.gif")}')

        br
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json')
          .template(align='center')
            | IMD Rainfall for 
            span.bold(class='iridl:long_name')
        br
        br
        .dlimgtsbox
          div a)
          -
             s = `
             home .mbell .IMD .daily .RT_0p25 .rf
             T 0.0 pentadAverage
             X (83E) (88.5E) RANGE
             Y (24N) (28N) RANGE
             T last cvsunits
             ( ) rsearch pop
             nip nip VALUES
             (mm/day) unitconvert
             T 0.0 pentadAverage
             (bb:[86,26,86.25,26.25])
             /region parameter
             geoobject
             [X Y] weighted-average
             DATA 0 AUTO RANGE
             toNaN
             [T]REORDER
             dup
             0 mul
             T fig: deltabars :fig
             /plotborder 90 psdef
             /plotbordertop 35 psdef
             /plotborderbottom 62 psdef
             /plotaxislength 323 psdef
             /fntsze 11 psdef
             /XOVY null psdef
             `;

          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='#{encodeIngrid(s,"+.gif")}')

        //- .dlimgtsbox
          div b)
          -
             s2 = `
             home .mbell .IMD .daily .RT_0p25 .rf
             T 0.0 pentadAverage
             X (83E) (88.5E) RANGE
             Y (24N) (28N) RANGE

             T last 73.0
             3.0 mul
             sub
             last RANGE
             SOURCES .IMD .RF0p25 .gridded .daily .v1901-2015 .rf
             T 0.0 pentadAverage
             X (83E) (88.5E) RANGE
             Y (24N) (28N) RANGE
             T (2006) (2016) RANGEEDGES
             T 73 splitstreamgrid
             [T2] average
             sub
             (bb:[86,26,86.25,26.25])
             /region parameter
             geoobject
             [X Y] weighted-average
             /scale_symmetric true def 
             /long_name (Rainfall 2006-2016 Anomalies) def 
             dup 
             T fig- colorbars2 medium -fig /plotborder 90 psdef /plotbordertop 35 psdef /plotaxislength 323 psdef /fntsze 11 psdef /XOVY null psdef
             `;

          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='#{encodeIngrid(s2,"+.gif")}')

        .dlimgtsbox
          div b)
          -
             s3 = `
             home .mbell .IMD .daily .RT_0p25 .rf
             T 0.0 pentadAverage
             X (83E) (88.5E) RANGE
             Y (24N) (28N) RANGE
             (bb:[86,26,86.25,26.25])
             /region parameter
             geoobject
             [X Y]weighted-average
             a: T last cvsunits
             ( ) rsearch pop
             nip nip VALUES
             :a: T last cvsunits
             ( ) rsearch pop
             nip nip interp
             1 sub
             s== VALUES
             :a
             2 {2 -1 roll T last cvsunits ( ) rsearch pop nip nip nip /fullname exch def}repeat
             grid:
             /name /T def
             /units (pentads since )
             5 index .fullname append
             (-01-01) append
             def
             0.5 1 72.5 :grid
             1 {2 -1 roll T 2 index replaceGRID exch}repeat
             pop
             2 -1 roll
             T 2 index .T .units streamgridunitconvert
             1 index .T
             fig: solid blue line medium black line :fig
             /plotbordertop 35 psdef
             /plotborder 90 psdef
             /plotborderbottom 62 psdef
             /plotaxislength 323 psdef
             /antialias true def
             `;
          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='#{encodeIngrid(s3,"+.gif")}')

        //- .dlimgtsbox
          div d)
          -
             s4 = `
             SOURCES .NOAA .NCEP .CPC .CMORPH .pentad .mean .morphed .cmorph
             X 83 88.5 RANGEEDGES
             Y 24 28 RANGEEDGES
             dup T last 371 sub last RANGE T (1 Jan) /startDay parameter last RANGE T 0.0 pentadAverage
             (bb:86:26:86.1:26.1:bb) /region parameter geoobject [X Y] 0.0 weighted-average [T] REORDER CopyStream
             T exch T integral
             3 -1 roll T (1 Jan 2006) (31 Dec 2016) RANGE
             /region get_parameter geoobject [X Y] 0.0 weighted-average
             T 73 splitstreamgrid
             a: [T2]average :a: [T2] 0.1 0.9 0 replacebypercentile [T]REORDER CopyStream :a 
             2 { exch T 3 index .T a: .first :a: .last :a RANGE T integral T /pointwidth 1 def pop } repeat
             a: percentile 0.9 VALUE percentile removeGRID :a: percentile 0.1 VALUE percentile removeGRID :a
             4 -2 roll
             T fig- grey deltabars medium solid black line blue line -fig
             /antialias true psdef
             /framelabel (                 Grey shade covers area between the 10th and 90th percentiles) psdef
             /plotborder 90 psdef
             /plotbordertop 35 psdef
             /plotborderbottom 85 psdef
             /plotaxislength 323 psdef
             /fntsze 12 psdef
             /antialias true psdef
             `;

          img.dlimgts.regionwithinbbox(rel='iridl:hasFigureImage', src='#{encodeIngrid(s4,"+.gif")}')

        br
        br
        p a) Pentad (i.e., 5-daily) rainfall for the selected region for the year.
        //p b) Pentad rainfall differences (from 2006-2016 mean) for the selected region over the recent past.
        p b) Smoothed pentad rainfall for the current year (thick black line) compared to previous years (blue-1 yr from present).
        //p d) Cumulative pentad rainfall (solid black line) and the cumulative long-term average rainfall (grey dotted line) for the most recent 12-month period in the selected region. The blue (red) bars are indicative of rainfall that is above (below) the long-term average.

      -
         s = `
         home .mbell .IMD .daily .RT_0p25 .rf
         X 83 88.5 RANGEEDGES
         Y 24 28 RANGEEDGES
         T 0.0 pentadAverage
         X 83.0 0.1 88.5 GRID
         Y 24.0 0.1 28.0 GRID
         dup
         SOURCES .Features .Political .India .Provinces_WHO .the_geom
         gid 29 VALUE
         rasterize
         gid removeGRID
         mul
         SOURCES .Features .Political .World .firstOrder_GAUL .the_geom gid (2064) VALUES
         SOURCES .Features .Political .India .Bihar_dist .the_geom
         SOURCES .Features .Political .India .Provinces_WHO .the_geom
         X Y fig- colors grey thinnish stroke thin white stroke stroke lightgrey medium countries_gaz -fig 
         /framelabelstart (Rainfall on ) psdef
         /antialias true psdef /T last plotvalue
         `;

      fieldset#content.dlimage.withMa(about='')
        link(rel='iridl:hasFigure', href='#{encodeIngrid(s,"")}')
        img.dlimg(border='0', alt='image', src='#{encodeIngrid(s,"+.gif")}')
        br
        img.dlauximg(src='#{encodeIngrid(s,"+.auxfig/+.gif")}')

      #tabs-1.ui-tabs-panel(about='')
        h2.titre1(property='term:title') Real time rainfall monitoring
        p(align='left' property='term:description')
          | A map of Bihar showing (5-day) averaged rainfall amounts in near real time. Click on the map to see a time series for the current year.
        //table(border='1', cols='1')
          tr
            td
              center
                pre.
                  
          tr
            td
              table
                tr
                  td
                    center
                      font(size='-2')
                        | Table adapted from McKee 
                        i et al.
                        |  (1993)
      br
      #tabs-2.ui-tabs-panel
        h2.titre1 Dataset Documentation
        dl.datasetdocumentation
          //p The International Research Institute for Climate and Society (IRI) and partners have been leading an effort to simultaneously improve the availability, access and use of climate information at the national level in African countries. This effort, named &ldquo;Enhancing National Climate Services (ENACTS)&rdquo;, has already been implemented in a number of countries in Africa. The foundation of ENACTS is the creation of quality-controlled, spatially and temporally complete climate data set. This involves organization and quality control of station data, and generating historical time series by combining station observations with satellite and other proxies. This approach has been implemented by Meteo Rwanda to generate over 30 years (1981 to present) of rainfall and over 50 years (1961-2012) temperature time series at pentad (5-daily) time scale and for every 5km grid point across Rwanda. This document provides a brief description of how these datasets were created along with the quality of the generated data.
          //img(align='left', src='ENACTS_fig_1.png', alt='Station Locations')
          //img(align='right', src='ENACTS_fig_2.png', alt='Number of Stations')
          //p
            | 1. Availability and quality of station data
            | Figure 1 presents the location of stations that measure rainfall. A small number of these stations (~25) also measure temperatures. Some of the stations may not be reporting all the time.   Figure 2 show the number of stations that reported rainfall each year between 1981 and 2013.  This figure show that missing observations is a very serious challenge for Rwanda, owed to the devastating 1994 genocide and the long time it took the national meteorological agency to recover its capacity.
          //p
            | 2. Quality control of station data
            | Data quality control is a critical component of ENACTS. A significant amount of time is spent on this aspect of the work.  Data quality control involves checking station coordinates, identifying outliers as well as checking for the homogeneity of the time series for each station.
            | The numbers of outliers found were relatively small. The homogeneity test revealed some serious problems, particularly with temperature data. These erroneous measurements were either fixed or removed.
          //p
            | 3. Reconstructing historical time series
            | Rainfall
            | In the standard ENACTS approach, historical rainfall time series are generated by combining station observations with satellite estimates for each dekad (ten day period) of each year. However, this approach does not work for Rwanda because of the 15-year gap in observations. Thus, a different approach was needed for Rwanda. The approach adopted removes the mean (climatological) differences (bias) between the satellite rainfall estimate and station data.  The mean bias is computed using data between 1981 and 1993.  The satellite product used is CHIRP from University of California, Santa Barbara. The generated data are used for the Climate Analysis Maproom.  Standard merging is also done for the years when station data is available. The time series generated with this approach is not uniform and is used only for the Climate Monitoring Maproom.
          //img(align='center', src='ENACTS_fig_3.png', alt='Comparison of Satellite')
          //p
            | The simple adjustment (bias removal) of the satellite estimate results in significant improvement (Figure 3). The comparison in Fig.3 is done for point values and part of the scatter comes from inaccurate station locations as well as some errors in station measurements.
          //img(align='left', src='ENACTS_fig_4.1.png', alt='Reanalysis Minimum Temperature')
          //img(align='right', src='ENACTS_fig_4.2.png', alt='Reanalysis Maximum Temperature')
          //p
            | For temperature, the Japanese 55-year Reanalysis (JRA55) was used as a proxy. The reanalysis data were first downscaled from 50km to 5km. Then a similar approach to that of rainfall is used to remove climatological bias between station measurements and the reanalysis product.  Figure 4 compares three different reanalysis maps with that of the station measurements for minimum and maximum temperature. The downscaled reanalysis has a more realistic spatial distribution of temperature compared to the first map. However, both reanalysis products significantly overestimate minimum temperature compared to the station measurements (top left). In the case of maximum temperature, the reanalysis products, both downscaled and original resolution, are close to the station measurements. The corrected products are very close to station values. The correction factors were developed using data from 1981 to 1993, but the result shown are for a specific dekad of a specific year.
          //img(align='center', src='ENACTS_fig_5.png', alt='Station vs. Corrected Reanalysis')
          //p
            | The scatter plots in Figure 5 also show a very good agreement between the station measurements and corrected reanalysis data. Station measurement and location uncertainties significantly contribute to scatters in this figure. This means that the actual quality the data could be better than what is shown in the scatter plots.
      #tabs-3.ui-tabs-panel
        h2.titre1 How to use this interactive map
        p(style='padding-right:25px;')
          .buttonInstructions.bis
      #tabs-4.ui-tabs-panel
        h2.titre1 Helpdesk
        p
          | Contact 
          a.lien(href='mailto:help@iri.columbia.edu?subject=Climate Monitoring - Rwanda') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
