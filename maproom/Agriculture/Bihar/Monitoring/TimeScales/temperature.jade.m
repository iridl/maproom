<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', xmlns:wn30='http://purl.org/vocabularies/princeton/wn30/', xmlns:wordnet-ontology='http://wordnet-rdf.princeton.edu/ontology#', version='XHTML+RDFa 1.1', xml:lang='<<<Language>>>')
  head
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Time Scales
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='temperature.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    link(rel='iridl:hasGlossary', href='/dochelp/definitions/index.html')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_TimeScales_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#decadal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interannual')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#change')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#variance')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#standard_deviation')
    link(rel='term:icon', href='http://iridl.ldeo.columbia.edu/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/%28Decadal%29//timeScale/parameter/%28Decadal%29/eq/%7Bpop/nip%7Dif//timeScale/get_parameter/%28Trend%29/eq/%7Bpop/pop%7Dif//timeScale/get_parameter/%28Inter-annual%29/eq/%7Bnip/nip%7Dif/%5BT%5Drmsaover/0/maskle//units/%28Celsius_scale%29/def//long_name/%28standard%20deviation%29/def/startcolormap/DATA/0/256/RANGE/transparent/white/210/250/210/RGB/0/VALUE/210/250/210/RGB/160/240/165/RGB/4/VALUE/130/210/135/RGB/8/VALUE/90/190/95/RGB/16/VALUE/yellow/51/VALUE/orange/102/VALUE/red/128/VALUE/magenta/162/VALUE/purple/196/VALUE/MidnightBlue/230/VALUE/MidnightBlue/MidnightBlue/256/bandmax/MidnightBlue/endcolormap/DATA/0/AUTO/RANGE/%28Percent%20of%20Variance%20Explained%29//timeAnal/parameter/%28Percent%20of%20Variance%20Explained%29/eq/%7Bdup/mul/exch%5BT%5Drmsaover/dup/mul/div//long_name/%28variance%20explained%29/def//units/unitless/def/%28percent%29/unitconvert/DATA/0/100/RANGE%7Dif/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/-fig//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20%29/append//timeScale/get_parameter/append/%28%20Time%20Scale%20%29/append//refdata/get_parameter/append/psdef//plotaxislength/700/psdef//plotborder/72/psdef+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef//framelabel+%28Jun-Aug%20Decadal%20Time%20Scale%20IMD%29+psdef+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    style.
      .val999 {
      visibility: hidden
      }
      .message999 {
      display: none;
      }
      .message999.val999 {
      display: block;
      visibility: visible;
      }
      .message1 {
      display: block;
      }
      .message1.sig999 {
      display: none;
      }
      .message0 {
      display: none;
      }
      .message0.sig0 {
      display: block;
      }
      .dlimage.ver3, .dlimage.ver3.wide {
      width: 29%;
      }
      @media only all and (min-width: 800px) {
      .dlimage.withMap, .dlimage.withMap.wide {
      width: 60%;
      }
      }
      @media only all and (min-width: 1200px) {
      .dlimage.withMap {
      width: 40%;
      }
      .dlimage.withMap.wide {
      width: 60%;
      }
      }
      @media only all and (min-width: 1400px) {
      .dlimage.withMap.wide {
      width: 40%;
      }
      }
      @media only all and (max-width: 800px) {
      .dlimage, .dlimage.wide {
      width: 95%;
      }
      .dlimage.ver3 {
      width: 95%;
      }
      }
      .climatechange {
      color: red;
      }
      .decadal {
      color: green;
      }
      .residual {
      color: blue;
      }
      .covariance {
      color: purple;
      }
      .flag {
      color: red;
      }
  body
    form#pageform(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carryup.carry.dlimg.dlauximg.dlimgloc.share(name='bbox', type='hidden')
      input.carry.dlimgts.dlimgloc.share(name='region', type='hidden')
      input.carry.dlimg.dlauximg.dlimgts.share(name='refdata', type='hidden')
      input.carry.dlimg.dlauximg.share(name='timeScale', type='hidden', data-default='Decadal')
      input.carry.dlimg.dlauximg.share(name='timeAnal', type='hidden', data-default='Percent of Variance Explained')
      input.carry.dlimg.dlauximg.dlimgts.share(name='seasonStart', type='hidden', data-default='Jun')
      input.carry.dlimg.dlauximg.dlimgts.share(name='seasonEnd', type='hidden', data-default='Aug')
      input.notused(name='plotaxislength', type='hidden')
      input.pickarea.dlimg.dlauximg.dlimgts.share(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')

    .controlBar
      fieldset#toSectionList.navitem
        legend Monitoring
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
          span(property='term:label') Time Scales
      fieldset.navitem
        legend Reference Data
        select.pageformcopy(name='refdata')
          option(value='') IMD
      fieldset.navitem
        legend Time Scale
        select.pageformcopy(name='timeScale')
          option Trend
          option Decadal
          option Inter-annual
      fieldset.navitem
        legend Variability
        select.pageformcopy(name='timeAnal')
          option Percent of Variance Explained
          option Standard Deviation
      fieldset.navitem
        legend Season
        select.pageformcopy(name='seasonStart')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
        |  to 
        select.pageformcopy(name='seasonEnd')
          option Jan
          option Feb
          option Mar
          option Apr
          option May
          option Jun
          option Jul
          option Aug
          option Sep
          option Oct
          option Nov
          option Dec
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Documentation
        li
          a(href='#tabs-3') Dataset Documentation
        li
          a(href='#tabs-4') Instructions
        li
          a(href='#tabs-5') Contact Us
      fieldset.dlimage.ver3
        a.dlimgts(rel='iridl:hasTable', href='http://iridl.ldeo.columbia.edu/expert/0/ds//UEA3p1/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.tmp/SOURCES/.UEA/.CRU/.TS3p1/.climatology/.c1901-2009/.tmp%5BX/Y%5DregridLinear/sub/X//gridtype/periodic/def/pop%7Ddef//GISSL/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.LandOnly/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.LandOnly/.tempanomaly/sub//name//tmp/def%7Ddef//GISSLO/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.Land-Ocean/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.Land-Ocean/.tempanomaly/sub//name//tmp/def%7Ddef/%28UEA3p1%29//refdata/parameter/get/exec/T/%28Jun%29//seasonStart/parameter/%28-%29append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29append/0.66/seasonalAverage//refdata/get_parameter/%28UEA3p1%29eq/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.stn/.tmp/0/maskle/T//seasonStart/get_parameter/%28-%29append//seasonEnd/get_parameter/append/%28%201901-2009%29append/0.66/seasonalAverage/0/maskle%5BT%5D1.0/average/0/mul/add%7D%7B/dup/[T]1./average/0/mul/add%7Difelse/Y/-60/80/RANGE/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/4/%7B4/-1/roll/%28bb:-180:-60:180:80:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average//units/%28degC%29def/DATA/AUTO/AUTO/RANGE%7Drepeat/4/-1/roll//long_name/%28raw%29def/4/-1/roll//long_name/%28trend%29def/4/-1/roll//long_name/%28decadal%29def/4/-1/roll//long_name/%28interannual%29def/T/5/1/roll/table:/5/:table/')
        img.dlimgloc.regionwithinbbox(src='/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:83:24:89:28:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:85:26:85.25:26.25:bb%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        br
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb:83:24:89:28:bb%29//region/parameter/geoobject/info.json')
          .template(align='center')
            | Observations for: 
            span.bold(class='iridl:long_name')

        .valid(style='display: inline-block; text-align: top')
          link.dlimgts(rel='iridl:hasJSON', href='/expert/0/ds//UEA3p1/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.tmp/SOURCES/.UEA/.CRU/.TS3p1/.climatology/.c1901-2009/.tmp%5BX/Y%5DregridLinear/sub/X//gridtype/periodic/def/pop%7Ddef//GISSL/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.LandOnly/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.LandOnly/.tempanomaly/sub//name//tmp/def%7Ddef//GISSLO/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.Land-Ocean/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.Land-Ocean/.tempanomaly/sub//name//tmp/def%7Ddef/%28UEA3p1%29//refdata/parameter/get/exec/T/%28Jun%29//seasonStart/parameter/%28-%29append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29append/0.66/seasonalAverage//refdata/get_parameter/%28UEA3p1%29eq/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.stn/.tmp/0/maskle/T//seasonStart/get_parameter/%28-%29append//seasonEnd/get_parameter/append/%28%201901-2009%29append/0.66/seasonalAverage/0/maskle%5BT%5D1.0/average/0/mul/add%7D%7B/dup/[T]1./average/0/mul/add%7Difelse/Y/-60/80/RANGE/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/4/%7B4/-1/roll/%28bb:-180:-60:180:80:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average%5BT%5Drmsaover/dup/mul%7Drepeat/3/%7B3/-1/roll/3/index/div//percent/unitconvert/0.5/add/999/setmissing_value/toi4%7Drepeat/%7Bclimatechange/decadal/residual%7Dds/climatechange//long_name/%28Trend%29def/decadal//long_name/%28Decadal%29def/residual//long_name/%28Inter-Annual%29def/%7Bclimatechange/decadal/residual%7Dds/nip/nip//max-age/864000/7/mul/def/info.json')
          script(type='application/json', property='iridl:hasPUREdirective').
            {"tr": {
            "var&lt;-iridl:hasDependentVariables": {
            "td.name": "var.iridl:long_name",
            "td.name@class+": "var.iridl:name",
            "+td.value": "var.iridl:value",
            "td.value@class+": "#\173var.iridl:name\175 val#\173var.iridl:value\175"
            }
            }
            }
          table.valid.template
            tr
              td.name
              td.value(align='right') %
        br
        .valid(style='display: inline-block; text-align: top')
          link.dlimgts(rel='iridl:hasJSON', href='/expert/0/ds//UEA3p1/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.tmp/SOURCES/.UEA/.CRU/.TS3p1/.climatology/.c1901-2009/.tmp%5BX/Y%5DregridLinear/sub/X//gridtype/periodic/def/pop%7Ddef//GISSL/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.LandOnly/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.LandOnly/.tempanomaly/sub//name//tmp/def%7Ddef//GISSLO/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.Land-Ocean/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.Land-Ocean/.tempanomaly/sub//name//tmp/def%7Ddef/%28UEA3p1%29//refdata/parameter/get/exec/T/%28Jun%29//seasonStart/parameter/%28-%29append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29append/0.66/seasonalAverage//refdata/get_parameter/%28UEA3p1%29eq/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.stn/.tmp/0/maskle/T//seasonStart/get_parameter/%28-%29append//seasonEnd/get_parameter/append/%28%201901-2009%29append/0.66/seasonalAverage/0/maskle%5BT%5D1.0/average/0/mul/add%7D%7B/dup/[T]1./average/0/mul/add%7Difelse/Y/-60/80/RANGE/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/%28bb:-180:-60:180:80:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/2/index%5BT%5Dcorrelate/2/index/dup/dataflag%5BT%5Dsum/2.0/sub/sqrt/exch%5BT%5Drmsaover/mul/mul/3/-2/roll/sub%5BT%5Drmsaover/div/abs/107.0/poestudnt/2.0/mul/0.01/flagle/999/setmissing_value/toi4/nip/{significance}ds/significance//long_name/(significance of trend)def/{significance}ds/nip//max-age/864000/7/mul/def/info.json')
          script(type='application/json', property='iridl:hasPUREdirective').
            {
            ".message1@class+": "sig#\173iridl:hasDependentVariables.significance.iridl:value\175",
            ".message0@class+": "sig#\173iridl:hasDependentVariables.significance.iridl:value\175"
            }
          .template
            .message1
              | Trend 
              span.message0 not 
              | significant at 1%
        .valid(style='display: inline-block; text-align: top; float: right')
          link.dlimgts(rel='iridl:hasJSON', href='/expert/0/ds//UEA3p1/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.tmp/SOURCES/.UEA/.CRU/.TS3p1/.climatology/.c1901-2009/.tmp%5BX/Y%5DregridLinear/sub/X//gridtype/periodic/def/pop%7Ddef//GISSL/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.LandOnly/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.LandOnly/.tempanomaly/sub//name//tmp/def%7Ddef//GISSLO/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.Land-Ocean/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.Land-Ocean/.tempanomaly/sub//name//tmp/def%7Ddef/%28UEA3p1%29//refdata/parameter/get/exec/T/%28Jun%29//seasonStart/parameter/%28-%29append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29append/0.66/seasonalAverage//refdata/get_parameter/%28UEA3p1%29eq/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.stn/.tmp/0/maskle/T//seasonStart/get_parameter/%28-%29append//seasonEnd/get_parameter/append/%28%201901-2009%29append/0.66/seasonalAverage/0/maskle%5BT%5D1.0/average/0/mul/add%7D%7B/dup/[T]1./average/0/mul/add%7Difelse/Y/-60/80/RANGE/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1.0/0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/4/%7B4/-1/roll/%28bb:-180:-60:180:80:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average%7Drepeat/dup%5BT%5Drmsaover/2/index%5BT%5Drmsaover/mul/3/-2/roll%5BT%5Dcorrelate/mul/nip/exch%5BT%5Drmsaover/dup/mul/div//percent/unitconvert/0.5/add/999/setmissing_value/toi4/%7Bcovariance%7Dds/covariance//long_name/%28Decadal/Interannual%20covariance%29def/%7Bcovariance%7Dds/nip//max-age/864000/7/mul/def/info.json')
          script(type='application/json', property='iridl:hasPUREdirective').
            {"tr": {
            "var&lt;-iridl:hasDependentVariables": {
            "td.name": "var.iridl:long_name",
            "td.name@class+": "var.iridl:name",
            "+td.value": "var.iridl:value",
            "td.value@class+": "#\173var.iridl:name\175 val#\173var.iridl:value\175"
            }
            }
            }
          table.valid.template
            tr
              td.name
              td.value(align='right') %
        .valid(style='display: inline-block; text-align: top; float: right')
          link.dlimgts(rel='iridl:hasJSON', href='/expert/0/ds//UEA3p1/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.tmp/SOURCES/.UEA/.CRU/.TS3p1/.climatology/.c1901-2009/.tmp%5BX/Y%5DregridLinear/sub/X//gridtype/periodic/def/pop%7Ddef//GISSL/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.LandOnly/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.LandOnly/.tempanomaly/sub//name//tmp/def%7Ddef//GISSLO/%7BSOURCES/.NASA/.GISS/.GISSTEMP/.d2x2/.Land-Ocean/.tempanomaly/SOURCES/.NASA/.GISS/.GISSTEMP/.climatology/.c1901-2009/.Land-Ocean/.tempanomaly/sub//name//tmp/def%7Ddef/%28UEA3p1%29//refdata/parameter/get/exec/T/%28Jun%29//seasonStart/parameter/%28-%29append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29append/0.66/seasonalAverage//refdata/get_parameter/%28UEA3p1%29eq/%7BSOURCES/.UEA/.CRU/.TS3p1/.r2p5/.stn/.tmp/0/maskle/T//seasonStart/get_parameter/%28-%29append//seasonEnd/get_parameter/append/%28%201901-2009%29append/0.66/seasonalAverage/0/maskle%5BT%5D1.0/average/0/mul/add%7D%7B/dup/[T]1./average/0/mul/add%7Difelse/Y/-60/80/RANGE/%28bb:-180:-60:180:80:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average%5BT%5Daverage/999/setmissing_value/toi4/%7Bscreening%7Dds/screening//long_name/%28Data%20screening%29def/%7Bscreening%7Dds/nip//max-age/864000/7/mul/def/info.json')
          script(type='application/json', property='iridl:hasPUREdirective').
            {
            ".message999@class+": "val#\173iridl:hasDependentVariables.screening.iridl:value\175"
            }
          .template
            .message999
              b No analysis available at this location
        br
        img.dlimgts(rel='iridl:hasFigureImage', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/2.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/pop/pop/2/%7B2/-1/roll/%28bb:83:24:89:28:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/0/replaceNaN%7Drepeat//scale_symmetric/%28true%29/def//fullname/%28trend%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch//scale_symmetric/%28true%29/def//fullname/%28raw%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch/T/fig-/line/black/zeroline/thick/solid/red/line/-fig//plotaxislength/300/psdef//plotborder/72/psdef//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20%29/append/%28%20raw%20and%20trend%20components%20%29/append//refdata/get_parameter/append/psdef/+.gif')
        img.dlimgts(rel='iridl:hasFigureImage', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/2.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/pop/nip%5BX/Y%5DREORDER/2/%7B2/-1/roll/%28bb:83.0:24.0:89.0:28.0:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/0/replaceNaN%7Drepeat//scale_symmetric/%28true%29/def//fullname/%28decadal%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch//scale_symmetric/%28true%29/def//fullname/%28raw%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch/T/fig-/white/line/black/zeroline/thick/solid/green/line/-fig//plotaxislength/300/psdef//plotborder/72/psdef//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20decadal%20component%20%29/append//refdata/get_parameter/append/psdef/+.gif')
        img.dlimgts(rel='iridl:hasFigureImage', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201901-2009%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/Y/-60/80/RANGE/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/2.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/nip/nip%5BX/Y%5DREORDER/2/%7B2/-1/roll/%28bb:83.0:24.0:89.0:28.0:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/0/replaceNaN%7Drepeat//scale_symmetric/%28true%29/def//fullname/%28interannual%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch//scale_symmetric/%28true%29/def//fullname/%28raw%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch/T/fig-/white/line/black/zeroline/solid/blue/line/-fig//plotaxislength/300/psdef//plotborder/72/psdef//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20interannual%20component%20%29/append//refdata/get_parameter/append/psdef/+.gif')
        img.dlimgts(rel='iridl:hasFigureImage', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/2.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/pop/3/-1/roll/pop/2/%7B2/-1/roll%5BX/Y%5DREORDER/CopyStream/%28bb:83.0:24.0:89.0:28.0:bb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/0/replaceNaN%7Drepeat//scale_symmetric/%28true%29/def//fullname/%28decadal%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/exch//scale_symmetric/%28true%29/def//fullname/%28trend%29/def//units/%28degC%29/def/DATA/AUTO/AUTO/RANGE/2/copy/abs/exch/abs/max/-1/mul//fullname/%28%29/def/3/-2/roll/T/fig-/verythin/dotted/white/line/thick/solid/green/line/red/line/thinnish/black/zeroline/-fig//plotaxislength/300/psdef//plotborder/72/psdef//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20trend%20and%20decadal%20components%20%29/append//refdata/get_parameter/append/psdef/+.gif')
      fieldset.dlimage.withMap(about='')
        link(rel='iridl:hasFigure', href='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/%28Decadal%29//timeScale/parameter/%28Decadal%29/eq/%7Bpop/nip%7Dif//timeScale/get_parameter/%28Trend%29/eq/%7Bpop/pop%7Dif//timeScale/get_parameter/%28Inter-annual%29/eq/%7Bnip/nip%7Dif/%5BT%5Drmsaover/0/maskle//units/%28Celsius_scale%29/def//long_name/%28standard%20deviation%29/def/startcolormap/DATA/0/256/RANGE/transparent/white/210/250/210/RGB/0/VALUE/210/250/210/RGB/160/240/165/RGB/4/VALUE/130/210/135/RGB/8/VALUE/90/190/95/RGB/16/VALUE/yellow/51/VALUE/orange/102/VALUE/red/128/VALUE/magenta/162/VALUE/purple/196/VALUE/MidnightBlue/230/VALUE/MidnightBlue/MidnightBlue/256/bandmax/MidnightBlue/endcolormap/DATA/0/AUTO/RANGE/%28Percent%20of%20Variance%20Explained%29//timeAnal/parameter/%28Percent%20of%20Variance%20Explained%29/eq/%7Bdup/mul/exch%5BT%5Drmsaover/dup/mul/div//long_name/%28variance%20explained%29/def//units/unitless/def/%28percent%29/unitconvert/DATA/0/100/RANGE%7Dif/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/-fig//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20%29/append//timeScale/get_parameter/append/%28%20Time%20Scale%20%29/append//refdata/get_parameter/append/psdef//plotaxislength/700/psdef//plotborder/72/psdef/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', alt='image', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/%28Decadal%29//timeScale/parameter/%28Decadal%29/eq/%7Bpop/nip%7Dif//timeScale/get_parameter/%28Trend%29/eq/%7Bpop/pop%7Dif//timeScale/get_parameter/%28Inter-annual%29/eq/%7Bnip/nip%7Dif/%5BT%5Drmsaover/0/maskle//units/%28Celsius_scale%29/def//long_name/%28standard%20deviation%29/def/startcolormap/DATA/0/256/RANGE/transparent/white/210/250/210/RGB/0/VALUE/210/250/210/RGB/160/240/165/RGB/4/VALUE/130/210/135/RGB/8/VALUE/90/190/95/RGB/16/VALUE/yellow/51/VALUE/orange/102/VALUE/red/128/VALUE/magenta/162/VALUE/purple/196/VALUE/MidnightBlue/230/VALUE/MidnightBlue/MidnightBlue/256/bandmax/MidnightBlue/endcolormap/DATA/0/AUTO/RANGE/%28Percent%20of%20Variance%20Explained%29//timeAnal/parameter/%28Percent%20of%20Variance%20Explained%29/eq/%7Bdup/mul/exch%5BT%5Drmsaover/dup/mul/div//long_name/%28variance%20explained%29/def//units/unitless/def/%28percent%29/unitconvert/DATA/0/100/RANGE%7Dif/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/-fig//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20%29/append//timeScale/get_parameter/append/%28%20Time%20Scale%20%29/append//refdata/get_parameter/append/psdef//plotaxislength/700/psdef//plotborder/72/psdef/+.gif')
        br
        img.dlauximg(rel='iridl:hasFigureImage', src='/expert/%28IMD%29//refdata/parameter/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean/SOURCES/.IMD/.HRDGT/.v1951-2015_monthly/.tmean_climo/sub/X/83./0.5/89./GRID/Y/24.0/0.5/28.0/GRID/T/%28Jun%29//seasonStart/parameter/%28-%29/append/%28Aug%29//seasonEnd/parameter/append/%28%201951-2015%29/append/0.66/seasonalAverage/dup/%5BT%5D1./average/0/mul/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/%5BT%5D1.0/0.0/regridLinear/dup/%5BT%5Dstandardize/exch/2/index/%5BT%5Dcorrelate/mul/1/index/%5BT%5Drmsaover/mul/1/index/%5BT%5Daverage/add/1/index/1/index/sub/T/-1/1.0/10/div/butt_filter:/5.0/store/ford/:butt_filter/.butt/2/index/2/index/sub/1/index/sub/%28Decadal%29//timeScale/parameter/%28Decadal%29/eq/%7Bpop/nip%7Dif//timeScale/get_parameter/%28Trend%29/eq/%7Bpop/pop%7Dif//timeScale/get_parameter/%28Inter-annual%29/eq/%7Bnip/nip%7Dif/%5BT%5Drmsaover/0/maskle//units/%28Celsius_scale%29/def//long_name/%28standard%20deviation%29/def/startcolormap/DATA/0/256/RANGE/transparent/white/210/250/210/RGB/0/VALUE/210/250/210/RGB/160/240/165/RGB/4/VALUE/130/210/135/RGB/8/VALUE/90/190/95/RGB/16/VALUE/yellow/51/VALUE/orange/102/VALUE/red/128/VALUE/magenta/162/VALUE/purple/196/VALUE/MidnightBlue/230/VALUE/MidnightBlue/MidnightBlue/256/bandmax/MidnightBlue/endcolormap/DATA/0/AUTO/RANGE/%28Percent%20of%20Variance%20Explained%29//timeAnal/parameter/%28Percent%20of%20Variance%20Explained%29/eq/%7Bdup/mul/exch%5BT%5Drmsaover/dup/mul/div//long_name/%28variance%20explained%29/def//units/unitless/def/%28percent%29/unitconvert/DATA/0/100/RANGE%7Dif/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/-fig//framelabel//seasonStart/get_parameter/%28-%29/append//seasonEnd/get_parameter/append/%28%20%29/append//timeScale/get_parameter/append/%28%20Time%20Scale%20%29/append//refdata/get_parameter/append/psdef//plotaxislength/700/psdef//plotborder/72/psdef/+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Recent trends: Temperature
        p(align='left', property='term:description')
          | This maproom shows how temperature has varied over the past 100 years, and what trends exist for Bihar districts.
      #tabs-2.ui-tabs-panel
        h2(align='center') Documentation
        h3 The Time scales maproom
        p(style='margin-bottom: 0in')
          | Although the decomposition of a signal into trend, low- and high-frequency components may seem straightforward, the analysis presented involves a number of subtleties. This document provides a more detailed look at the analytical procedures utilized than does
          | the overview presented in Greene et al. (2011), and offers a number of caveats regarding the interpretation of maproom displays.
        h4 Method
        p
          | Data processing consists of three steps: 
          i Screening
          |  the individual gridbox values for filled data and for very dry seasons a
          | nd regions, 
          i detrending
          |  in order to extract slow, trend-like changes and 
          i filtering
          | , to separate high and low frequenc
          | y components in the detrended data. Each of these steps is described below. Data are processed gridbox by gridbox, meaning that results in adjacent gridboxes are not compared or combined, except when the user requests that analysis be performed on area-averaged data. Averaging over gridboxes is then performed prior to the time scales decomposition.
        h4 Screening
        p
          | The underlying datasets employed are 
          i complete
          | , i.e., they
          | do not contain missing values. This does not mean, however, that
          | actual measurements were available for every month and at every
          | geographic location covered by the data. The completeness requirement
          | has been imposed by the data providers with particular uses in mind
          | and is met by &quot;filling in&quot; values for which actual station
          | measurements do not exist. The exact manner by which this is
          | accomplished is described in documentation linked on the dataset
          | pages; these can be accessed via the maproom pages based on the
          | respective datasets.
        p
          | Relatively little filling has been
          | performed on the temperature data, so in this case the screening
          | requires that 
          i all
          |  data values represent actual measurements,
          | rather than filled values. The screening procedure employed for
          | precipitation is more flexible, with consideration given to both the
          | number and the distribution in time of actual measurements
          | contributing to each gridpoint value. A trade-off between spatial and
          | temporal coverage then comes into play, with a higher degree of
          | temporal coverage corresponding to fewer qualifying gridpoints, and
          | vice versa. The user can control this balance by choosing among &quot;high
          | temporal coverage,&quot; &quot;high spatial coverage&quot; and &quot;intermediate temporal
          | and spatial coverage.&quot; Because the focus of the maproom is time series
          | behavior, it is recommended that the user prefer &quot;high temporal
          | coverage&quot; or the intermediate option whenever possible. The &quot;high
          | spatial coverage&quot; option presents the data without any temporal
          | screening, so results from the time scales decomposition may be less
          | reliable than with the other choices.
        p
          | The &quot;high temporal
          | coverage&quot; option imposes the same requirement as that imposed on
          | temperature, viz., that all data values must represent actual
          | measurements, and that none can be filled. For the &quot;intermediate&quot;
          | option this requirement is relaxed somewhat, with at least half of the
          | data values required to represent measurements.
        p
          | In addition, it is required that the data be relatively uniformly
          | distributed in time. For example, using the intermediate option, the
          | 50% of values that are not filled may not all be concentrated in the
          | second half of the data series. As presently implemented, the
          | uniformity requirement is based on a 10-year sliding window. The
          | fraction of filled values within this window is not permitted to fall
          | below the specified threshold.
        p
          | In addition to the temporal screening there is the requirement
          | that, for precipitation, climatological seasonal rainfall must exceed
          | 30 mm. Such a threshold would represent very dry conditions, rendering
          | the utility of the time scales decomposition questionable. In
          | addition, even small fluctuations in precipitation would seem large
          | compared with such a dry climatology, increasing the variance of
          | estimated precipitation variations. The minimum screening requirement
          | avoids these situations.
        p
          | Gridpoints failing these requirements are shown as blank on
          | the maps presented; clicking on such points returns a &quot;no data&quot;
          | message. When the user selects &quot;area average&quot; for a region, only those
          | points meeting the minimum data requirements are utilized in computing
          | area-averaged data. It should be apparent that area-averaging over a
          | large area that contains few qualifying gridboxes will not produce a
          | result that is regionally representative.
        p
          | Because even partially filled records may be expected to degrade
          | analytical results to some degree, and because data at individual
          | gridpoints may be noisy, it is probably best, for the sake of
          | robustness, to average the data over at least a small region before
          | applying the time scales decomposition.
        h4 The trend component
        p
          | Trends are often computed in the time domain, in which case they
          | might be expressed, for example, as a change of so many millimeters
          | per month occurring per decade. The common procedure of fitting a
          | linear trend assumes that such a rate of change is constant with time.
        p
          | The map room takes a different approach, based on a simple
          | conceptual model: Rather than expressing local or regional trends as
          | functions of 
          i time
          | , we relate them instead to
          | global 
          i temperature
          |  change. The assumption is not that
          | precipitation (or temperature) changes simply as a result of the
          | passage of time, but rather, because of the warming of the planet. It
          | is in this sense that the trend component, as computed in the maproom,
          | can be identified with &quot;climate change.&quot; Such a trend has a
          | functional, rather than simply a numerical significance.
        p
          | Computation of the global temperature record to be used as a
          | regressand is less simple than it sounds. Fluctuations in the Earth&apos;s
          | climate have many sources, including &quot;natural&quot; variability &mdash; intrinsic
          | variations that are not associated with anthropogenically-induced
          | climate change. Such variations, if large enough in scale, can
          | significantly influence, or &quot;project&quot; onto the global mean
          | temperature. If we take the latter to represent in some sense the
          | signature of climate change, there is a risk that we will
          | unintentionally include some component of natural variability, which
          | will then mistakenly be identified with this signature.
        p
          | To circumvent this problem the global temperature signal is
          | computed using an ensemble of general circulation models (GCMs). These
          | models, which constitute a comprehensive representation of our current
          | understanding of the mechanisms of climate variability and change,
          | underlie much of the Fourth Assessment Report of the Intergovernmental
          | Panel on Climate Change (IPCC, 2007). Simulations from the &quot;Twentieth
          | Century Climate in Coupled Models&quot; (20C3M) experiment are
          | used.
        p
          | As with the real Earth, climate in each of these
          | simulations includes both a &quot;forced&quot; response (what we think of a
          | climate change) and natural, &quot;unforced&quot; variations. However, the
          | unforced variability is 
          i incoherent
          |  from model to model &mdash; there
          | is no synchronization or phase relationship among the models, and
          | indeed, the character of each model&apos;s unforced variability differs to
          | a greater or lesser degree from that of the others. To obtain an
          | estimate of the forced response, we average together the
          | 20
          sup th
          | -century global mean temperature records from the
          | members of this ensemble, which here includes 23 (nearly all) of the
          | IPCC GCMs. Averaging has the effect of attenuating the incoherent
          | (i.e., uncorrelated) unforced variability while enhancing that part of
          | the response that the models have in common &mdash; the climate change
          | signal. The multimodel averaging can thus be said to increase the
          | signal-to-noise ratio, where &quot;signal&quot; refers to the common climate
          | change response and &quot;noise&quot; the unforced natural variability. This
          | ratio is increased in the multimodel mean relative to that of the
          | individual simulations. Most of the models provide multiple 20C3M
          | simulations; to put the models on an even footing, a single simulation
          | from each model is used to create the multimodel average.
        p
          | The multimodel mean signal is further processed, by lowpass
          | filtering. This has the effect of removing most of the residual
          | year-to-year and decade-to-decade variability that has not been
          | averaged away in the formation of the multimodel mean. The resulting
          | smoothed global temperature signal, which serves as the signature of
          | the forced climate change response, is shown in Fig. 1. Downward
          | &quot;bumps&quot; in this signal in the 1900s, 1960s and early 1990s can
          | probably be attributed, at least in part, to major volcanic eruptions,
          | which have a short-term cooling effect; to the extent that these
          | variations are expressed in regional signals they will be recognized
          | as part of the forced response. Although the forcing is not
          | anthropogenic in this case, it is nevertheless considered &quot;external&quot;
          | as far as the maproom is concerned: Volcanic eruptions are not
          | believed to be associated, at least in any easily demonstrable way,
          | with natural climate variability, so it was deemed incorrect to treat
          | them as such.
        p
          br
          br
        p(align='CENTER')
          img(src='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas/T/fig:/blue/medium/solid/line/grey/thin/dotted/286.4/0.2/287.4/horizontallines/:fig/T/-708./624./plotrange//XOVY/null/psdef//antialias/true/psdef//tas/286.2704/287.4/plotrange//plotborder/77/psdef//fntsze/16/psdef//plotaxislength/500/psdef/+//plotborder+77+psdef//XOVY+null+psdef//plotaxislength+500+psdef+.gif', name='graphics1', align='BOTTOM', width='447', height='301', border='0')
        p(align='CENTER')
          | Figure 1: The global mean &quot;climate
          | change&quot; temperature record used for 
          span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-detrending-noun-1]') detrending
          | .
        p(align='CENTER')
          br
          br
        p
          | The trend component of a local
          | temperature or precipitation signal is extracted by regressing the
          | local series on the global temperature signal of Fig 1. Fitted values
          | from the regression represent, by construction, that part of the
          | regional signal which is linearly dependent on global mean
          | temperature. It is in this sense that the trend, as here computed, may
          | be thought of as the climate change component of the regional
          | signal.
        p
          | It is worth noting that for a local or regional
          | signal that is being analyzed in the maproom, the entanglement of
          | forced and natural components is still possible. This is because,
          | while the signal of Fig. 1 has been effectively stripped of natural
          | internal variability, a real-world signal may still contain natural
          | components that are &quot;masquerading&quot; as trend. This might happen, for
          | example, if some natural mode of variability were to be increasing
          | over a relatively long time period, say the last 30 years of the
          | 20
          sup th
          |  century. In such a case this mode might motivate a
          | similar increase in values of the regional series being analyzed,
          | which then &quot;maps&quot; onto the global mean temperature increase shown in
          | Fig. 1. The Atlantic Multidecadal Oscillation (AMO, see, e.g.,
          | Enfield, 2001) exhibits a signal something like this; to the extent
          | that the AMO influences local climate, there may be some possibility
          | for this sort of misidentification to occur (see, e.g., DelSole et
          | al., 2011). In general, and for the more approximative type of
          | assessment for which the maproom is designed, we do not believe that
          | such entanglement will pose a major problem with interpretation.
        p
          | Figure 2a illustrates the 
          span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-detrending-noun-1]') detrending
          |  step, as applied to a typical
          | precipitation record obtained from the maproom. Note that the inferred
          | trend is negative, and appears as a shifted, scaled inversion of the
          | signal shown in Fig. 1. The inverse characteristic results from the
          | fitting of a downward-trending regional signal; the fact that the
          | inferred trend is a scaled, shifted version of the signal of Fig. 1 is
          | a characteristic of the linear regression. Recall, finally, that the
          | inferred trend represents a regression on global
          | mean 
          i temperature
          | ; this explains its nonlinearity in the time
          | domain.
        p
          img(src='http://iridl.ldeo.columbia.edu/SOURCES/.WCRP/.GCOS/.GPCC/.FDP/.version4/.2p5/a:/.anom/T/%28Jun-Aug%201901-2007%29seasonalAverage/:a:/.cnts/T/%28Jun-Aug%201901-2007%29seasonalAverage/0/maskle%5BT%5D0.5/average/0/mul/add/:a:/.prcp/T/%28Jun-Aug%201901-2007%29seasonalAverage/%28Aug%29interp/%28Jun%29interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%5BT%5Daverage/30/masklt/0/mul/:a/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1./0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/2/%7Bexch/X/%28-20%29%2810%29RANGEEDGES/Y/%280%29%2822.5%29RANGEEDGES%5BX/Y%5Daverage%7Drepeat/%28scale_symmetric%29%28true%29def/%28fullname%29%28trend%29def/DATA/AUTO/AUTO/RANGE/exch/%28scale_symmetric%29%28true%29def/%28fullname%29%28raw%29def/DATA/AUTO/AUTO/RANGE/exch/dup/T/fig-/line/black/zeroline/thick/solid/red/line/plotlabel/-fig//framelabel/(a)psdef//plotborder/72/psdef//plotaxislength/300/psdef//asum/-43/43/plotrange//XOVY/null/psdef/+//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+300+psdef+.gif', name='graphics2', align='BOTTOM', border='0')
          img(src='http://iridl.ldeo.columbia.edu/SOURCES/.WCRP/.GCOS/.GPCC/.FDP/.version4/.2p5/a:/.anom/T/%28Jun-Aug%201901-2007%29seasonalAverage/:a:/.cnts/T/%28Jun-Aug%201901-2007%29seasonalAverage/0/maskle%5BT%5D0.5/average/0/mul/add/:a:/.prcp/T/%28Jun-Aug%201901-2007%29seasonalAverage/%28Aug%29interp/%28Jun%29interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%5BT%5Daverage/30/masklt/0/mul/:a/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1./0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/sub/dup/T/1/1./10/div/butt_filter:/5./store/ford/:butt_filter/.butt/2/%7Bexch/X/%28-20%29%2810%29RANGEEDGES/Y/%280%29%2822.5%29RANGEEDGES%5BX/Y%5Daverage%7Drepeat/%28scale_symmetric%29%28true%29def/%28fullname%29%28decadal%29def/DATA/AUTO/AUTO/RANGE/exch/%28scale_symmetric%29%28true%29def/%28fullname%29%28residual%29def/DATA/AUTO/AUTO/RANGE/exch/dup/T/fig-/black/line/black/zeroline/thick/solid/green/line/plotlabel/-fig//framelabel/(b)psdef//plotborder/72/psdef//plotaxislength/300/psdef//asum/-43/43/plotrange//XOVY/null/psdef/+//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+300+psdef+.gif', name='graphics3', align='BOTTOM', border='0')
          img(src='http://iridl.ldeo.columbia.edu/SOURCES/.WCRP/.GCOS/.GPCC/.FDP/.version4/.2p5/a:/.anom/T/%28Jun-Aug%201901-2007%29seasonalAverage/:a:/.cnts/T/%28Jun-Aug%201901-2007%29seasonalAverage/0/maskle%5BT%5D0.5/average/0/mul/add/:a:/.prcp/T/%28Jun-Aug%201901-2007%29seasonalAverage/%28Aug%29interp/%28Jun%29interp/sub/dup/0/ge/%7B1/add%7D%7B13/add%7Difelse/mul%5BT%5Daverage/30/masklt/0/mul/:a/add/SOURCES/.IRI/.Analyses/.TS/.AR4/.tas%5BT%5D1./0.0/regridLinear/dup%5BT%5Dstandardize/exch/2/index%5BT%5Dcorrelate/mul/1/index%5BT%5Drmsaover/mul/1/index%5BT%5Daverage/add/sub/dup/T/1/1./10/div/butt_filter:/5./store/ford/:butt_filter/.butt/sub/X/%28-20%29%2810%29RANGEEDGES/Y/%280%29%2822.5%29RANGEEDGES%5BX/Y%5Daverage/%28scale_symmetric%29%28true%29def/%28fullname%29%28interannual%29def/DATA/AUTO/AUTO/RANGE/dup/T/fig-/solid/blue/line/black/zeroline/plotlabel/-fig//framelabel/(c)psdef//plotborder/72/psdef//plotaxislength/300/psdef//adif/-43/43/plotrange//XOVY/null/psdef/+//plotborder+72+psdef//XOVY+null+psdef//plotaxislength+300+psdef+.gif', name='graphics4', align='BOTTOM', border='0')
        p(style='margin-bottom: 0in')
          | Figure 2: Stages of the
          | maproom decomposition process. (a) Trend component, represented by
          | the fitted values in a regression of the local signal onto the
          | multimodel mean temperature record of Fig. 1; (b) Residual signal
          | from this regression and its lowpass-filtered counterpart, the latter
          | identified with the decadal component of variability; (c) Interannual
          | component, which is the residual signal in (b), from which the
          | decadal component has been subtracted.
        h4 Decadal and Interannual components
        p
          | If the fitted values from the regression onto the global multimodel
          | mean temperature record of Fig. 1 are taken as the &quot;climate change&quot;
          | trend, the residuals from this regression then represent the natural,
          | unforced component of variability. The next step in the analysis aims
          | to decompose this residual signal into &quot;decadal&quot; and &quot;interannual&quot;
          | signals, representing respectively the low- and high-frequency
          | components of natural variability.
        p
          | To do this, the residuals are lowpassed by filtering, using an
          | order-five Butterworth filter with half-power at a period of 10
          | year. Although the Butterworth design has some desirable properties
          | that make it well-suited to this task, any number of alternate
          | filtering procedures could also have been used; testing indicates that
          | results are not sensitive to the filter details. Filter parameters
          | were chosen (a) so as to effect a clean separation between low- and
          | high-frequency components without introducing instability in the
          | filter response (this refers to the filter order) and (b) to
          | effectively classify variability due to El Ni&ntilde;o-Southern
          | Oscillation (ENSO) as &quot;interannual.&quot; With the order-five filter,
          | covariation between the two components generally amounts to no more
          | than a few percent of the variance of the initial &quot;raw&quot; series. (In
          | the real world a &quot;perfect&quot; separation of time scales is not
          | achievable; all practical filter designs represent compromises in this
          | regard.)
        p
          | ENSO exhibits a broad spectral peak in the 2-8 year band. Phenomena
          | responsible for variability on longer time scales belong to class of
          | processes that are less well-understood, and whose predictability is
          | currently the subject of active research (see, e.g., Meehl et
          | al. 2009). This &quot;low-frequency&quot; class includes large-scale modes such
          | as the Pacific Decadal Oscillation (PDO) and Atlantic Multidecadal
          | Oscillation (AMO), as well as low-frequency stochastic
          | variations. Thus the filtering effectively partitions variability by
          | process class, not simply by nominal time scale.
        p
          | This second stage in the decomposition in illustrated in Fig. 2b,
          | which shows in black the &quot;natural&quot; residual from the 
          span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-detrending-noun-1]') detrending
          | operation of Fig. 2a (i.e., the raw initial signal minus the trend
          | component). Superimposed on this is the green &quot;decadal&quot; signal, which
          | represents the output of the lowpass filter, applied to the natural
          | residual.
        p
          | Finally, the interannual component is computed as the difference
          | between black and green traces in Fig. 2b, i.e., the residual from the
          | detrending step minus its lowpassed incarnation. Shown in Fig. 2c,
          | this signal represents that part of natural variability having its
          | expression at periods shorter than ten years. The trend (red), decadal
          | (green) and interannual (blue) signals are what is shown in the
          | maproom when the user either clicks at a point or chooses &quot;area
          | average,&quot; the latter in order to display the time scale decomposition
          | as applied to an area-averaged signal.
        h4 A few things to be aware of in using the maproom 
        ul
          li
            p(style='margin-bottom: 0in')
              | As noted above, 
              span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-detrending-noun-1]') detrending
              | via regression on the multimodel temperature record represents only an
              | approximate separation of forced and natural variability; rigorously
              | performed, such separation is not a simple exercise. Depending on
              | timing and period, natural fluctuations in the data could project onto
              | the multimodel mean temperature signal and be incorrectly identified
              | with the forced response. The potential for such entanglement becomes
              | lower as length-of-record increases, and vice versa.
          p(style='margin-bottom: 0in')
          li
            p(style='margin-bottom: 0in')
              | We recommend the analysis
              | of area-averaged data if possible, for a number of reasons. First,
              | data at an individual gridpoint is likely to be 
              i noisy
              | . For
              | example, a couple of wet years near one terminus of the series could
              | create the impression of a trend, even though there is no overall
              | trend in the series. Even if the gridpoint passes the intermediate
              | screening criteria a number of values may have been filled, with
              | potentially misleading effects. These problems will generally be
              | ameliorated to some degree by area-averaging, for the same reason that
              | multimodel averaging increases the signal-to-noise ratio of the
              | resultant series: Anomalous events will be less likely to occur in
              | many locations at once, so their effects will be reduced.
          p(style='margin-bottom: 0in')
          li
            p(style='margin-bottom: 0in')
              | The maproom shows the
              | percent of variance in the raw data that is explained by each of the
              | three components. The alert user will notice that these percentages
              | often sum to less than one hundred percent. This is because the
              | filtering procedure is necessarily imperfect, and does not completely
              | separate the decadal and interannual components of the detrended
              | series, leading to a degree of covariance between them. (This is the
              | filtering problem known as &quot;leakage.&quot;) The value for this covariance
              | is also provided. Twice the covariance value, added to the trend,
              | decadal and interannual variance percentages, should add to one
              | hundred percent, give or take a percent for rounding error.
          p(style='margin-bottom: 0in')
          li
            p(style='margin-bottom: 0in')
              | After 
              span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-detrending-noun-1]') detrending
              | , about 20%
              | of the variance of annually-resolved white noise would be expected to
              | accrue to the decadal component, as here defined. White noise is a
              | random process having no &quot;memory,&quot; in the sense that its value at a
              | particular time does not exhibit any dependence on its values at other
              | previous times. This differs from processes having memory or
              | &quot;persistence,&quot; in which the process level is dependent on previous
              | values (such processes tend to vary more slowly than white
              | noise). Thus, a decadal variance fraction of as much as 20% (meaning
              | the decadal fraction divided by the sum of decadal and interannual
              | fractions) should not be mistaken for the signature of a systematic
              | decadal &quot;oscillation,&quot; or even a slow random process, that differs
              | from white noise.
          p(style='margin-bottom: 0in')
          li
            p(style='margin-bottom: 0in')
              | Although the variance
              | decomposition, or other aspects of the maproom plots may provide some
              | sense of future expectations (in the statistical sense), the maproom
              | is primarily a means of deconstructing past variations, not a
              | predictive tool. In particular, it cannot tell us how the character of
              | variability may change in response to global warming and thus, how the
              | decomposition of variance might evolve with the passage of time.
          p(style='margin-bottom: 0in')
        p(style='margin-bottom: 0in')
          | Thank you for visiting the Time Scales Maproom. We anticipate that interaction with maproom users will help us to understand how the product might be improved. Questions or comments are therefore solicited, and may be addressed to 
          font(color='#0000ff')
            u
              a(href='mailto:help@iri.columbia.edu') help@iri.columbia.edu
          | .Please include the phrase &quot;Time scales&quot; in the subject line.
        h4 References
        p(style='margin-bottom: 0in')
          | DelSole, Timothy, Michael K. Tippett, Jagadish Shukla, A Significant Component of Unforced Multidecadal Variability in the Recent Acceleration of Global Warming. J. Climate, 
          b 24
          | : 909&mdash;926. doi: 10.1175/2010JCLI3659.1, 2011.
        p(style='margin-bottom: 0in')
          br
        p(style='margin-bottom: 0in')
          | Enfield, D.B., A.M.
          | Mestas-Nunez and P.J. Trimble, The Atlantic Multidecadal Oscillation
          | and its relationship to rainfall and river flows in the continental
          | U.S., Geophys. Res. Lett. 
          b 28
          | : 2077&mdash;2080.
          font(color='#0000ff')
            a(href='http://en.wikipedia.org/wiki/Digital_object_identifier')
              font(color='#000000')
                span(style='text-decoration: none') doi
          | :
          font(color='#0000ff')
            a(href='http://dx.doi.org/10.1029%2F2000GL012745')
              font(color='#000000')
                span(style='text-decoration: none') 10.1029/2000GL012745
          | ,
          | 2001.
        p(style='margin-bottom: 0in')
          br
        p(style='margin-bottom: 0in')
          | IPCC, 
          font(color='#0000ff')
            a(href='http://ipcc-wg1.ucar.edu/wg1/wg1-report.html')
              font(color='#000000')
                span(style='text-decoration: none')
                  | Climate
                  | Change 2007: The Physical Science Basis. Contribution of Working
                  | Group I to the Fourth Assessment Report of the Intergovernmental
                  | Panel on Climate Change
          |  [Solomon, S., D.
          | Qin, M. Manning, Z. Chen, M. Marquis, K.B. Averyt, M.Tignor and H.L.
          | Miller (eds.)]. Cambridge University Press, Cambridge, United Kingdom
          | and New York, NY, USA, 2007.
        p(style='margin-bottom: 0in')
          br
        p(style='margin-bottom: 0in')
          | Meehl, Gerald A., and
          | Coauthors, Decadal Prediction: Can it be skillful, Bull. Amer.
          | Meteor. Soc., 
          b 90
          | , 1467&mdash;1485, doi: 10.1175/2009BAMS2778.1,
          | 2009.
        p(style='margin-bottom: 0in')
          br
        p
          | Greene, A.M, L. Goddard and R. Cousin, 
          a(href='http://www.agu.org/pubs/crossref/2011/2011EO450001.shtml') Web tool deconstructs variability in twentieth-century climate
          | , Eos Trans. AGU, 92(45), 397, doi:10.1029/2011EO450001.
      #tabs-3.ui-tabs-panel
        h2(align='center') Dataset Documentation
        p
          b Global-mean multimodel-mean temperature record
          br
          font(size='-1')
            b Data Source
            | :
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.WCRP/.CMIP3/', target='_blank') CMIP3 multi-model ensemble mean
        p
          b Observations
          br
          font(size='-1')
            b Data Source
            | :
            | monthly mean precipitation and temperature from 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/', target='_blank') GRIDDED RAINFALL from IMD RF0p25 v1901-2015: IMD 0.25 Degree Gridded Precipitation Data Set
            |  and 
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IMD/.HRDGT/.v1951-2015/') India Meteorological Department (IMD) High Resolution Gridded Temperature Data Set
            | .
      #tabs-4.ui-tabs-panel
        h2(align='center') Instructions
        .buttonInstructions
      #tabs-5.ui-tabs-panel
        h2(align='center') Helpdesks
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Temperature Time Scales Maproom') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room, for example, the forecasts not displaying or updating properly.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
