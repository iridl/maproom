<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Select a Point Climatology
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link.share(rel='canonical', href='Select_a_Point.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(xml:lang='', property='maproom:Entry_Id', content='Global_Climatologies_Select_a_Point')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#LandSurface')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#PlanetarySurface')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Surface')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climatology')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#WetDayFrequency')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#GroundFrostFrequency')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Elevation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Classification')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.5x0.5')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:icon', href='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/monthlyAverage/yearly-climatology/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/precip_colors/T//defaulvalue/%28first%29/def/pop/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/black/thinnish/stroke/grey/thin/stroke/black/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange/T/first/plotvalue//antialias/true/psdef//plotborder/0/psdef//plotaxislength/152/psdef+//antialias+true+psdef//color_smoothing+1+psdef+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
  body(xml:lang='en')
    form#pageform.info.carryup.carry.dlimg.dlauximg.share.dlimgts.dlimgloc(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carryup.carry.dlimg.share.dlimgloc(name='bbox', type='hidden')
      input.dlimg.share(name='T', type='hidden')
      input.carry.share.dlimgts.dlimgloc.dlimglocclick(name='region', type='hidden', data-default='bb:85.75:25.5:86:25.75:bb')
      input.share.dlimg.dlauximg(name='maptype', type='hidden', data-default='prec')
      input.unused(name='plotaxislength', type='hidden', data-default='432')
      input.carry.share.pickarea(name='resolution', type='hidden', data-default='.25')
    .controlBar
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
          span(property='term:label') Monitoring
      fieldset.navitem
        legend Map Type
        select.pageformcopy(name='maptype')
          option(value='prec') Precipitation
          option(value='meantemp') Mean Temperature
          option(value='mintemp') Minimum Temperature
          option(value='maxtemp') Maximum Temperature
          option(value='elev') Elevation
      fieldset.navitem
        legend Average Over
        span.selectvalue
        select.pageformcopy(name='resolution')
          option(value='.25') .25 Point
          option(value='.5') .5 Point
          option(value='irids:SOURCES:Features:Political:India:Bihar_dist:ds') District
    style.
      .leftpart {
      display: inline-block;
      width: 45%
      }
      .rightpart {
      margin-left: 10pt;
      display: inline-block;
      width: 45%
      }
      .rightblock {
      margin-left: 10pt;
      display: inline-block
      }
      @media only all and (max-width: 400px) {
      .leftpart { width: 100%}
      .rightpart {width: 95%}
      }
      .plotheading {
      text-align: center;
      margin: 0px;
      }
      @media only all and (min-width: 600px) {
      .dlimage.wide {
      width: 45%;
      }
      }
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='') Dataset
        li
          a(href='#tabs-3') Contact Us
      fieldset.dlimage

        img.regionwithinbbox.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/(bb:85.75:25.5:86:25.75:bb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(bb:85.75:25.5:86:25.75:bb)//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/%5BX/Y%5Dweighted-average/exch/Y/exch/%5BX/Y%5Dweighted-average/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/(2064)/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/lightgrey/mask/red/fill/red/smallpushpins/black/thinnish/stroke/grey/thin/stroke/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
        .rightblock.valid
          a.dlimgts(rel='iridl:hasJSON', href='/expert/(bb:85.75:25.5:86:25.75:bb)//region/parameter/geoobject/info.json')
          span.template.bold(class='iridl:long_name')
        div
          .leftpart
            h4.plotheading PRECIPITATION
            img.dlimgts(style='width: 100%', rel='iridl:hasFigureImage', src='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/monthlyAverage/yearly-climatology/(bb:85.75:25.5:86:25.75:bb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/dup/0/mul/T/fig-/grey/deltabars/black/dotted/0/30/900/horizontallines/-fig//XOVY/1.2/psdef//plotaxislength/250/psdef//framelabel//region/get_parameter/psdef/+.gif')

          .rightpart
            h4.plotheading TEMPERATURE
            img.dlimgts(style='width: 100%', rel='iridl:hasFigureImage', src='/SOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/a%3A/.tmax/monthlyAverage/yearly-climatology/(bb:85.75:25.5:86:25.75:bb)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/%3Aa%3A/.tmin/monthlyAverage/yearly-climatology/(bb:83%2C%2024%2C88.5%2C28)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/%3Aa%3A/.tmean/monthlyAverage/yearly-climatology/(bb:83%2C%2024%2C88.5%2C28)//region/parameter/geoobject/%5BX/Y%5Dweighted-average/%3Aa/T/fig-/grey/deltabars/medium/black/line/thin/dotted/-60/5/50/horizontallines/-fig//XOVY/1.2/psdef//plotaxislength/250/psdef//framelabel//region/get_parameter/psdef/+.gif')

      fieldset.dlimage
        link(rel='iridl:hasFigure', href='/expert/%28prec%29//maptype/parameter/%28prec%29/eq/%7BSOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/monthlyAverage/yearly-climatology/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/precip_colors/DATA/0/30/RANGE%7Dif/%28prec%29//maptype/parameter/%28meantemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmean/monthlyAverage/yearly-climatology/DATA/6/42/RANGE/%7Dif/%28prec%29//maptype/parameter/%28mintemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmin/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28maxtemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmax/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28elev%29/eq/%7BSOURCES/.NOAA/.NGDC/.ETOPO5/.elev/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/DATA/0/1000/RANGE/white/white/LightGoldenrodYellow/0/VALUE/LightGoldenrod/150/VALUE/goldenrod/350/VALUE/DarkGoldenrod/800/VALUE/sienna/1000/VALUE/sienna/endcolormap%7Dif/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T//defaulvalue/%7B/first/%7D/def/pop/%7D/if/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/black/thinnish/stroke/grey/thin/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T/first/plotvalue/%7D/if//antialias/true/psdef//plotborder/72/psdef//plotaxislength/432/psdef/')
        img.dlimg(src='/expert/%28prec%29//maptype/parameter/%28prec%29/eq/%7BSOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/monthlyAverage/yearly-climatology/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/precip_colors/DATA/0/30/RANGE%7Dif/%28prec%29//maptype/parameter/%28meantemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmean/monthlyAverage/yearly-climatology/DATA/6/42/RANGE/%7Dif/%28prec%29//maptype/parameter/%28mintemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmin/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28maxtemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmax/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28elev%29/eq/%7BSOURCES/.NOAA/.NGDC/.ETOPO5/.elev/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/DATA/0/1000/RANGE/white/white/LightGoldenrodYellow/0/VALUE/LightGoldenrod/150/VALUE/goldenrod/350/VALUE/DarkGoldenrod/800/VALUE/sienna/1000/VALUE/sienna/endcolormap%7Dif/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T//defaulvalue/%7B/first/%7D/def/pop/%7D/if/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/black/thinnish/stroke/grey/thin/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T/first/plotvalue/%7D/if//antialias/true/psdef//plotborder/72/psdef//plotaxislength/432/psdef/+.gif', border='0', alt='image')
        img.dlauximg(src='/expert/%28prec%29//maptype/parameter/%28prec%29/eq/%7BSOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/monthlyAverage/yearly-climatology/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/precip_colors/DATA/0/30/RANGE%7Dif/%28prec%29//maptype/parameter/%28meantemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmean/monthlyAverage/yearly-climatology/DATA/6/42/RANGE/%7Dif/%28prec%29//maptype/parameter/%28mintemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmin/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28maxtemp%29/eq/%7BSOURCES/.IMD/.HRDGT/.v1951-2015/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/.tmax/monthlyAverage/yearly-climatology/DATA/6/42/RANGE%7Dif/%28prec%29//maptype/parameter/%28elev%29/eq/%7BSOURCES/.NOAA/.NGDC/.ETOPO5/.elev/X/83/88.5/RANGEEDGES/Y/24/28/RANGEEDGES/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/DATA/0/1000/RANGE/white/white/LightGoldenrodYellow/0/VALUE/LightGoldenrod/150/VALUE/goldenrod/350/VALUE/DarkGoldenrod/800/VALUE/sienna/1000/VALUE/sienna/endcolormap%7Dif/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T//defaulvalue/%7B/first/%7D/def/pop/%7D/if/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/black/thinnish/stroke/grey/thin/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange/%28prec%29//maptype/parameter/%28elev%29/ne/%7B/T/first/plotvalue/%7D/if//antialias/true/psdef//plotborder/72/psdef//plotaxislength/432/psdef/+.auxfig/+.gif', border='0', alt='image')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Average monthly rainfall and temperature
        p(align='left', property='term:description')
          | Click anywhere over land on the map to see barplots of long-term-mean monthly precipitation, monthly average of daily maximum, minimum, and mean temperature.
      #tabs-2.ui-tabs-panel
        h2(align='center') Dataset Documentation
        h4
          a.carry Select a Point Climatology
        h4 Monthly Climatological Variables
        dl.datasetdocumentation
          dt Data
          dd
            | University of East Anglia Climatic Research Unit TS2.1 monthly precipitation, monthly average of daily maximum, minimum, and mean temperature, wet-day frequency, and ground frost frequency on a 0.5&deg; lat/lon grid
          dt Data Source
          dd
            | University of East Anglia Climatic Research Unit (
            a(href='http://iridl.ldeo.columbia.edu/SOURCES/.UEA/.CRU/.TS2p1/.dataset_documentation.html') CRU TS2.1
            | )
          dt Analysis
          dd
            | Monthly climatology of monthly precipitation, monthly average of daily maximum, minimum, and mean temperature, wet-day frequency, and ground frost frequency for the 1971-2000 base period
          dt Reference
          dd
            | Mitchell, T. D., P. D. Jones. An improved method of constructing a database of monthly climate observations and associated high-resolution grids. Int. J. Climatol. 25, 2005.
        h4 Koeppen Climate Classification
        dl.datasetdocumentation
          dt Data Source
          dd U.N. Food and Agriculture Organization (FAO) Agrometeorology Group
          dt Analysis
          dd
            a(href='http://www.fao.org/sd/EIdirect/climate/EIsptext.htm#anchor2') Koeppen climate classification
            |  prepared by the FAO Agrometeorology Group derived from mean monthly climate data prepared in 1991 by R. Leemans and W. Cramer and published by the International Institute for Applied Systems Analysis (IIASA). This Koeppen classification is based upon monthly rainfall and temperatures, including: average monthly temperature of the warmest month, average monthly temperature of the coldest month, average thermal amplitude between the coldest and warmest months, number of months with temperature exceeding 10&deg;C, and winter and summer rains.  For more detailed information see this 
            a(href='http://www.fao.org/sd/EIdirect/climate/EIsp0066.htm') Brief Guide to Koeppen Climate Classification System
            | .
        h4 Elevation
        dl.datasetdocumentation
          dt Data Source
          dd
            | National Oceanic and Atmospheric Administration (NOAA) National Geophysical Data Center (NGDC) ETOPO1 ice surface elevation/bathymetry
          dt Description
          dd
            a(href='http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/docs/ETOPO1.pdf') ETOPO1 Grid Registered 1 Arc-Minute Global Relief Model
          dt Reference
          dd
            | Amante, C. and B. W. Eakins, ETOPO1 1 Arc-Minute Global Relief Model: Procedures, Data Sources and Analysis, National Geophysical Data Center, NESDIS, NOAA, U.S. Department of Commerce, Boulder, CO, July 2008.

      .ui-tabs-panel-hidden
        h2(align='center') Dataset
        p
          a(href='http://iridl.ldeo.columbia.edu/expert/%28koeppen%29//maptype/parameter/%28koeppen%29eq/%7BSOURCES/.FAO/.NRMED/.SD/.Climate/.Derived/.class%7Dif/%28koeppen%29//maptype/parameter/%28prec%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.prcp/DATA/0/700/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28wetdays%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.wet%7Dif/%28koeppen%29//maptype/parameter/%28meantemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.mean/.temp/DATA/-55/40/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28mintemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.minimum/.temp/DATA/-55/40/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28maxtemp%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.maximum/.temp/DATA/-55/40/RANGE%7Dif/%28koeppen%29//maptype/parameter/%28frostdays%29eq/%7BSOURCES/.UEA/.CRU/.TS2p1/.climatology/.c7100/.frost%7Dif/%28koeppen%29//maptype/parameter/%28elev%29eq/%7BSOURCES/.NOAA/.NGDC/.ETOPO1/.z_ice/lon/%28X%29renameGRID/lat/%28Y%29renameGRID/X/-180/180/RANGE/Y/-90/90/RANGE/startcolormap/DATA/0/8752/RANGE/white/white/LightGoldenrodYellow/0/VALUE/LightGoldenrod/500/VALUE/goldenrod/1500/VALUE/DarkGoldenrod/3000/VALUE/sienna/8224/VALUE/sienna/endcolormap%7Dif/') Access the dataset used to create this map.
      #tabs-3.ui-tabs-panel
        h2(align='center') Helpdesk
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Select a Point Climatology') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
