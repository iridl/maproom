<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Bihar Monsoon Onset
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='index.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(property='maproom:Entry_Id', content='Monsoon_Onset')
    //meta(property='maproom:Sort_Id', content='a3')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Historical_Analysis')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.5x0.5')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.25x0.25')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average_resolution')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#spatial_average')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#time_series')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#India')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#India_68_1')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#daily')
    link(rel='term:icon', href='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v2p0/.rf/X/83/89/RANGE/Y/24/28/RANGE/X/83.0/0.1/89.0/GRID/Y/24.0/0.1/28.0/GRID/T/%281%20May%29//earlyStart/parameter/60//searchDays/parameter/0//rainyDay/parameter/5//runningDays/parameter/20//runningTotal/parameter/1//minRainyDays/parameter/10//dryDays/parameter/30//drySpell/parameter/onsetDate/T/sub/%5BT%5Daverage/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/0//searchDays/get_parameter/RANGE/white/white/white/0/VALUE/blue//searchDays/get_parameter/VALUE/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig:/colors/black/solid/stroke/stroke/countries_gaz/:fig+//plotaxislength+432+psdef//plotborder+0+psdef//color_smoothing+1+psdef+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.dlimg.dlauximg.share(name='bbox', type='hidden')
      //input.share.dlimgts(name='region', type='hidden')
      //input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.unused(name='plotaxislength', type='hidden', data-default='432')
      //input.pickarea.dlimg.dlauximg.share(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')
      input.dlimg.dlauximg.share.dlimgts(name='earlyStart', type='hidden', data-default='1 May')
      input.dlimg.dlauximg.share.dlimgts(name='minRainyDays', type='hidden', data-default='1')
      input.carry.dlauximg.dlimg.share.dlimgts(name='runningDays', type='hidden', data-default='5')
      input.carry.dlauximg.dlimg.share.dlimgts(name='runningTotal', type='hidden', data-default='20')
      input.dlimg.dlauximg.share.dlimgts(name='rainyDay', type='hidden', data-default='0')
      input.dlimg.dlauximg.share.dlimgts(name='drySpell', type='hidden', data-default='30')
      input.dlimg.dlauximg.share.dlimgts(name='searchDays', type='hidden', data-default='60')
      input.dlimg.dlauximg.share.dlimgts(name='dryDays', type='hidden', data-default='10')
    .controlBar
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
          span(property='term:label') Monitoring
      fieldset.navitem
        legend earlyStart
        input.pageformcopy(name='earlyStart', type='text', value='1 May', size='6', maxlength='6')
      fieldset.navitem
        legend minRainyDays
        input.pageformcopy(name='minRainyDays', type='text', value='1', size='3', maxlength='3')
      fieldset.navitem
        legend runningDays
        input.pageformcopy(name='runningDays', type='text', value='5', size='3', maxlength='3')
      fieldset.navitem
        legend runningTotal
        input.pageformcopy(name='runningTotal', type='text', value='20', size='4', maxlength='4')
      fieldset.navitem
        legend rainyDay
        input.pageformcopy(name='rainyDay', type='text', value='0', size='3', maxlength='3')
      fieldset.navitem
        legend drySpell
        input.pageformcopy(name='drySpell', type='text', value='30', size='3', maxlength='3')
      fieldset.navitem
        legend searchDays
        input.pageformcopy(name='searchDays', type='text', value='60', size='3', maxlength='3')
      fieldset.navitem
        legend dryDays
        input.pageformcopy(name='dryDays', type='text', value='10', size='3', maxlength='3')
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Options
        li
          a(href='#tabs-3') Dataset Documentation
        li
          a(href='#tabs-4') Contact Us
      fieldset.dlimage
        a(rel='iridl:hasFigure', href='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v2p0/.rf/X/83/89/RANGE/Y/24/28/RANGE/X/83.0/0.1/89.0/GRID/Y/24.0/0.1/28.0/GRID/T/%281%20May%29//earlyStart/parameter/60//searchDays/parameter/0//rainyDay/parameter/5//runningDays/parameter/20//runningTotal/parameter/1//minRainyDays/parameter/10//dryDays/parameter/30//drySpell/parameter/onsetDate/T/sub/%5BT%5Daverage/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/0//searchDays/get_parameter/RANGE/white/white/white/0/VALUE/blue//searchDays/get_parameter/VALUE/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig:/colors/black/solid/stroke/stroke/countries_gaz/:fig/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', src='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v2p0/.rf/X/83/89/RANGE/Y/24/28/RANGE/X/83.0/0.1/89.0/GRID/Y/24.0/0.1/28.0/GRID/T/%281%20May%29//earlyStart/parameter/60//searchDays/parameter/0//rainyDay/parameter/5//runningDays/parameter/20//runningTotal/parameter/1//minRainyDays/parameter/10//dryDays/parameter/30//drySpell/parameter/onsetDate/T/sub/%5BT%5Daverage/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/0//searchDays/get_parameter/RANGE/white/white/white/0/VALUE/blue//searchDays/get_parameter/VALUE/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig:/colors/black/solid/stroke/stroke/countries_gaz/:fig/+.gif')
        img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v2p0/.rf/X/83/89/RANGE/Y/24/28/RANGE/X/83.0/0.1/89.0/GRID/Y/24.0/0.1/28.0/GRID/T/%281%20May%29//earlyStart/parameter/60//searchDays/parameter/0//rainyDay/parameter/5//runningDays/parameter/20//runningTotal/parameter/1//minRainyDays/parameter/10//dryDays/parameter/30//drySpell/parameter/onsetDate/T/sub/%5BT%5Daverage/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/startcolormap/0//searchDays/get_parameter/RANGE/white/white/white/0/VALUE/blue//searchDays/get_parameter/VALUE/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig:/colors/black/solid/stroke/stroke/countries_gaz/:fig/+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Agronomic monsoon onset date
        p(align='left', property='term:description')
          | Maps of average rainy season onset date, computed using an agronomic definition, with user-defined parameters.
      #tabs-2.ui-tabs-panel(about='')
        h2(align='center') Options
        p(align='left')
          b Years and Season
          | :
          | Specify the range of years over which to perform the analysis, and choose the start and end dates of the season over which the diagnostics are to be performed.
          br
          b Mask and Seasonal Data Coverage
          | :
          | The APHRODITE dataset includes a record of the number of station data observations present for each gridbox on each day of the dataset. The maproom allows this information to be used in two ways: (1) gridbox-days without at least one observation can be masked prior to any calculations (masking so called interpolated values); (2) a minimum fraction of non-masked days per season can be required in order for the seasonal diagnostic to be defined at that gridbox — if this minimum threshold is not met, then the seasonal diagnostic is assigned a missing value at that gridbox, for that season and year.
          br
          b Wet/Dry Day/Spell Definitions
          | :
          | These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Total Rainfall
          | : total cumulative precipitation (in mm) falling over the season.
          br
          b Number of Wet Days
          | : the number of wet days (as defined above) during the season.
          br
          b Rainfall Intensity
          | :
          | the average daily precipitation over the season considering only wet days.
          br
          b Number of Wet (Dry) Spells
          | :
          | the number of wet (dry) spells during the season according to the definitions in the Options section. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
          br
          b Percentage of Data Available
          | : the percentage of days with non missing values within the season.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | :
          | The analysis can performed and map at each grid point for both 0.25˚ and 0.50˚ resolution APHRODITE products. Additionally it is possible to average the results of the analysis over the 0.25˚ grid points falling within administrative boundaries for the time series graph.
      #tabs-3.ui-tabs-panel(about='')
        h2(align='center') Dataset Documentation
        p(align='left')
          b Data
          | : GRIDDED RAINFALL from IMD RF0p25 gridded daily v1901-2015: IMD 0.25 Degree Daily Gridded Precipitation Data Set. 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IMD/.RF0p25/.gridded/.daily/.v1901-2015/.rf/') here
          | .
      #tabs-4.ui-tabs-panel
        h2(align='center') Helpdesks
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Maproom') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
