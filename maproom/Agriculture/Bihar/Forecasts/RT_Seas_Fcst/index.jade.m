<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    title Seasonal forecast
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='index.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(property='maproom:Entry_Id', content='Bihar_Realtime_Seasonal_Forecast')
    meta(property='maproom:Sort_Id', content='')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Forecasts_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg0.5x0.5')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#India')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#India_68_1')
    link(rel='term:icon', href='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.NMME_India_Forecast/.Precipitation/.prob/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/F/last/VALUE/L/first/VALUE/a:/C/%28Above_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/22/masklt/add/0/replaceNaN/:a:/C/%28Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/10/12/masknotrange/add/0/replaceNaN/add/:a:/C/%28Below_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/0/maskgt/add/0/replaceNaN/:a/add/0/maskle//CLIST%5B/%28%3C35%20Below%29/%2835-40%20Below%29/%2840-45%20Below%29/%2845-50%20Below%29/%2850-55%20Below%29/%2855-60%20Below%29/%2860-65%20Below%29/%2865-70%20Below%29/%2870-75%20Below%29/%2875-80%20Below%29/%28%3E80%20Below%29/%28%3C35%20Normal%29/%2835-40%20Normal%29/%2840-45%20Normal%29/%2845-50%20Normal%29/%2850-55%20Normal%29/%2855-60%20Normal%29/%2860-65%20Normal%29/%2865-70%20Normal%29/%2870-75%20Normal%29/%2875-80%20Normal%29/%28%3E80%20Normal%29/%28%3C35%20Above%29/%2835-40%20Above%29/%2840-45%20Above%29/%2845-50%20Above%29/%2850-55%20Above%29/%2855-60%20Above%29/%2860-65%20Above%29/%2865-70%20Above%29/%2870-75%20Above%29/%2875-80%20Above%29/%28%3E80%20Above%29/%5Ddef/startcolormap/DATA/1/33/RANGE/transparent/white/white/1/VALUE/white/white/2/bandmax/yellow/2/VALUE/red/11/VALUE/red/red/12/bandmax/white/white/13/bandmax/gray/13/VALUE/DimGray/22/VALUE/DimGray/DimGray/23/bandmax/white/white/24/bandmax/PaleGreen/24/VALUE/DarkGreen/33/VALUE/DarkGreen/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig:/colors/grey/thin/solid/stroke/:fig+//plotborder+0+psdef//color_smoothing+1+psdef//plotaxislength+232+psdef+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carryLanguage(name='Set-Language', type='hidden')
      input.dlimg.dlauximg.share(name='bbox', type='hidden')
      //input.share.dlimgts(name='region', type='hidden')
      //input.transformRegion.dlimglocclick(name='clickpt', type='hidden')
      input.unused(name='plotaxislength', type='hidden', data-default='432')
      //input.pickarea.dlimg.dlauximg.share(name='resolution', type='hidden', data-default='irids:SOURCES:Features:Political:India:Bihar_dist:ds')
    .controlBar
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Forecasts_term')
          span(property='term:label') Forecasts
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Options
        li
          a(href='#tabs-3') Dataset Documentation
        li
          a(href='#tabs-4') Contact Us
      fieldset.dlimage
        a(rel='iridl:hasFigure', href='/SOURCES/.IRI/.FD/.NMME_India_Forecast/.Precipitation/.prob/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/C/%28Above_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/22/masklt/add/0/replaceNaN/:a:/C/%28Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/10/12/masknotrange/add/0/replaceNaN/add/:a:/C/%28Below_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/0/maskgt/add/0/replaceNaN/:a/add/0/maskle//CLIST%5B/%28%3C35%20Below%29/%2835-40%20Below%29/%2840-45%20Below%29/%2845-50%20Below%29/%2850-55%20Below%29/%2855-60%20Below%29/%2860-65%20Below%29/%2865-70%20Below%29/%2870-75%20Below%29/%2875-80%20Below%29/%28%3E80%20Below%29/%28%3C35%20Normal%29/%2835-40%20Normal%29/%2840-45%20Normal%29/%2845-50%20Normal%29/%2850-55%20Normal%29/%2855-60%20Normal%29/%2860-65%20Normal%29/%2865-70%20Normal%29/%2870-75%20Normal%29/%2875-80%20Normal%29/%28%3E80%20Normal%29/%28%3C35%20Above%29/%2835-40%20Above%29/%2840-45%20Above%29/%2845-50%20Above%29/%2850-55%20Above%29/%2855-60%20Above%29/%2860-65%20Above%29/%2865-70%20Above%29/%2870-75%20Above%29/%2875-80%20Above%29/%28%3E80%20Above%29/%5Ddef/startcolormap/DATA/1/33/RANGE/transparent/white/white/1/VALUE/white/white/2/bandmax/yellow/2/VALUE/red/11/VALUE/red/red/12/bandmax/white/white/13/bandmax/gray/13/VALUE/DimGray/22/VALUE/DimGray/DimGray/23/bandmax/white/white/24/bandmax/PaleGreen/24/VALUE/DarkGreen/33/VALUE/DarkGreen/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig:/colors/grey/thin/solid/stroke/:fig/')
        img.dlimg(rel='iridl:hasFigureImage', border='0', src='/SOURCES/.IRI/.FD/.NMME_India_Forecast/.Precipitation/.prob/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/C/%28Above_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/22/masklt/add/0/replaceNaN/:a:/C/%28Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/10/12/masknotrange/add/0/replaceNaN/add/:a:/C/%28Below_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/0/maskgt/add/0/replaceNaN/:a/add/0/maskle//CLIST%5B/%28%3C35%20Below%29/%2835-40%20Below%29/%2840-45%20Below%29/%2845-50%20Below%29/%2850-55%20Below%29/%2855-60%20Below%29/%2860-65%20Below%29/%2865-70%20Below%29/%2870-75%20Below%29/%2875-80%20Below%29/%28%3E80%20Below%29/%28%3C35%20Normal%29/%2835-40%20Normal%29/%2840-45%20Normal%29/%2845-50%20Normal%29/%2850-55%20Normal%29/%2855-60%20Normal%29/%2860-65%20Normal%29/%2865-70%20Normal%29/%2870-75%20Normal%29/%2875-80%20Normal%29/%28%3E80%20Normal%29/%28%3C35%20Above%29/%2835-40%20Above%29/%2840-45%20Above%29/%2845-50%20Above%29/%2850-55%20Above%29/%2855-60%20Above%29/%2860-65%20Above%29/%2865-70%20Above%29/%2870-75%20Above%29/%2875-80%20Above%29/%28%3E80%20Above%29/%5Ddef/startcolormap/DATA/1/33/RANGE/transparent/white/white/1/VALUE/white/white/2/bandmax/yellow/2/VALUE/red/11/VALUE/red/red/12/bandmax/white/white/13/bandmax/gray/13/VALUE/DimGray/22/VALUE/DimGray/DimGray/23/bandmax/white/white/24/bandmax/PaleGreen/24/VALUE/DarkGreen/33/VALUE/DarkGreen/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig:/colors/grey/thin/solid/stroke/:fig/L/1.0/plotvalue/F/last/plotvalue+.gif')
        img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.IRI/.FD/.NMME_India_Forecast/.Precipitation/.prob/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/C/%28Above_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/22/masklt/add/0/replaceNaN/:a:/C/%28Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/10/12/masknotrange/add/0/replaceNaN/add/:a:/C/%28Below_Normal%29/VALUE/C/removeGRID/%7B/%28%3C35%29/35/%2835-40%29/40/%2840-45%29/45/%2845-50%29/50/%2850-55%29/55/%2855-60%29/60/%2860-65%29/65/%2865-70%29/70/%2870-75%29/75/%2875-80%29/80/%28%3E80%29/%7Dclassify/%5Bprob%5Ddominant_class/:a:/%5BC%5Ddominant_class/1.0/sub/11/mul/0/maskgt/add/0/replaceNaN/:a/add/0/maskle//CLIST%5B/%28%3C35%20Below%29/%2835-40%20Below%29/%2840-45%20Below%29/%2845-50%20Below%29/%2850-55%20Below%29/%2855-60%20Below%29/%2860-65%20Below%29/%2865-70%20Below%29/%2870-75%20Below%29/%2875-80%20Below%29/%28%3E80%20Below%29/%28%3C35%20Normal%29/%2835-40%20Normal%29/%2840-45%20Normal%29/%2845-50%20Normal%29/%2850-55%20Normal%29/%2855-60%20Normal%29/%2860-65%20Normal%29/%2865-70%20Normal%29/%2870-75%20Normal%29/%2875-80%20Normal%29/%28%3E80%20Normal%29/%28%3C35%20Above%29/%2835-40%20Above%29/%2840-45%20Above%29/%2845-50%20Above%29/%2850-55%20Above%29/%2855-60%20Above%29/%2860-65%20Above%29/%2865-70%20Above%29/%2870-75%20Above%29/%2875-80%20Above%29/%28%3E80%20Above%29/%5Ddef/startcolormap/DATA/1/33/RANGE/transparent/white/white/1/VALUE/white/white/2/bandmax/yellow/2/VALUE/red/11/VALUE/red/red/12/bandmax/white/white/13/bandmax/gray/13/VALUE/DimGray/22/VALUE/DimGray/DimGray/23/bandmax/white/white/24/bandmax/PaleGreen/24/VALUE/DarkGreen/33/VALUE/DarkGreen/endcolormap/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig:/colors/grey/thin/solid/stroke/:fig/L/1.0/plotvalue/F/last/plotvalue+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Seasonal forecasts
        p(align='left', property='term:description')
          | Rainfall forecasts up to 7 months ahead, with forecast confidence expressed using tercile category probabilities.
      #tabs-2.ui-tabs-panel(about='')
        h2(align='center') Options
        p(align='left')
          b Years and Season
          | :
          | Specify the range of years over which to perform the analysis, and choose the start and end dates of the season over which the diagnostics are to be performed.
          br
          b Mask and Seasonal Data Coverage
          | :
          | The APHRODITE dataset includes a record of the number of station data observations present for each gridbox on each day of the dataset. The maproom allows this information to be used in two ways: (1) gridbox-days without at least one observation can be masked prior to any calculations (masking so called interpolated values); (2) a minimum fraction of non-masked days per season can be required in order for the seasonal diagnostic to be defined at that gridbox — if this minimum threshold is not met, then the seasonal diagnostic is assigned a missing value at that gridbox, for that season and year.
          br
          b Wet/Dry Day/Spell Definitions
          | :
          | These define the amount in millimeters (non inclusive) above which a day is considered to be a wet day (as opposed to dry), and the minimum number (inclusive) of consecutive wet (dry) days to define a wet (dry) spell.
          br
          br
          b Seasonal daily statistics
          | : Choose the seasonal diagnostic quantity (i.e the statistic of the daily data) to be computed for each season, from the following choices.
          br
          b Total Rainfall
          | : total cumulative precipitation (in mm) falling over the season.
          br
          b Number of Wet Days
          | : the number of wet days (as defined above) during the season.
          br
          b Rainfall Intensity
          | :
          | the average daily precipitation over the season considering only wet days.
          br
          b Number of Wet (Dry) Spells
          | :
          | the number of wet (dry) spells during the season according to the definitions in the Options section. For example, if a wet spell is defined as 5 contiguous wet days, 10 contiguous wet days are counted as 1 wet spell. Note that a spell, according to the definitions above, that is overlapping the start or end of the season will be counted only if the part of the spell that falls within the season reaches the minimal length of consecutive days.
          br
          b Percentage of Data Available
          | : the percentage of days with non missing values within the season.
          br
          br
          b Yearly seasonal statistics
          | : a choice of yearly statistics over the chosen season of the selected range of years to produce the map among: the mean, the standard deviation and the probability of exceeding a user specified threshold.
          br
          br
          b Spatial Resolution
          | :
          | The analysis can performed and map at each grid point for both 0.25˚ and 0.50˚ resolution APHRODITE products. Additionally it is possible to average the results of the analysis over the 0.25˚ grid points falling within administrative boundaries for the time series graph.
      #tabs-3.ui-tabs-panel(about='')
        h2(align='center') Dataset Documentation
        p(align='left')
          b Data
          | : IRI FD NMME_India_Forecast India Seasonal Precipitation Forecast: India NMME Seasonal forecasts for probabilities of below-normal, above-normal, and near-normal precipitation. 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.NMME_India_Forecast/.Precipitation/') here
          | .
      #tabs-4.ui-tabs-panel
        h2(align='center') Helpdesks
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Maproom') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
