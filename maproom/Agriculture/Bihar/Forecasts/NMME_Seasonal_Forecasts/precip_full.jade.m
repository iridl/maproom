<<<includes>>>
=== canonical = "index.html"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:wn30='http://purl.org/vocabularies/princeton/wn30/', xmlns:wn20schema='http://www.w3.org/2006/03/wn/wn20/schema/', xmlns:wordnet-ontology='http://wordnet-rdf.princeton.edu/ontology#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.1', xml:lang='<<<Language>>>')
  head
    meta(http-equiv='content-type', content='text/html; charset=UTF-8')
    meta(name='viewport', content='width=device-width, initial-scale=1.0')
    meta(xml:lang='', property='maproom:Sort_Id', content='a01')
    title Precipitation flexible seasonal forecast
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link.share(rel='canonical', href='precip_full.html')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    meta(property='maproom:Entry_Id', content='Bihar_Flexible_Seasonal_Forecast')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='iridl:hasGlossary', href='/dochelp/definitions/index.html')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Forecasts_term')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#monthly')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#percentile')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Probability')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#IRI')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term')
    link(rel='term:icon', href='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.fcstmean/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/3/-1/roll/mul/add/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7B50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramclim/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/2/VALUE/ind/removeGRID/:a/3/-2/roll/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul/dup/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/mul/3/-2/roll/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/exch/mul/add/eexp/dup/1.0/add/div%5Bmodl%5Daverage/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse%7D%7B/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/90.0//threshold2/parameter/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/div/mul/add/eexp/dup/1.0/add/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add%7Dif%5Bmodl%5Daverage/correlationcolorscale/DATA/0/1/RANGE/dup%7Difelse/dup/%5BX/Y%5D%7Bstep/3.0/mul/runningAverage%7Dforall/exch/dataflag/1/masklt/mul/X/1/setgridtype/pop//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/DATA/0/1/RANGE/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/%7Bpercentile_val/forecast/target_date%7Dds//name//flexfcst/def/a-/.forecast/-a-/.percentile_val/-a-/.target_date/-a/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/contours/plotlabel/grey/thin/solid/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/S/last/plotvalue/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//layers%5B//forecast//Bihar_dist//lakes%5Dpsdef//antialias/true/psdef+//plotaxislength+432+psdef//color_smoothing+1+psdef//plotborder+0+psdef/+.gif')

    <<<altLanguages(Languages, Language, canonical, '    ')>>>

    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    style.
      sup { vertical-align:super; font-size:50%; }
      .varPrecipitation #percThresh {
      display: none;
      }
      .var #physThresh {
      display: none;
      }
      .var #physThresh2 {
      display: none;
      }
      .dlcontrol.L {display: none !important;}
      .dlcontrol.percentile {display: none !important;}
      .dlimage.ver2 {
      width: 29%;
      float: right;
      }
      .dlimage.ver3 {
      width: 99%;
      }
      @media only all and (max-width: 750px) {
      .dlimage.ver2 {
      width: 95%;
      float: right;
      }
      .dlimage.ver3 {
      width: 49%;
      }
      }
  body(xml:lang='en')
    form#pageform(name='pageform')
      input.carryup.carry(name='Set-Language', type='hidden')
      input.carryup.carry.dlimg.dlimgloc.share(name='bbox', type='hidden')
      input.carry.dlimg.dlimgts.share.starttime.startleadtime(name='S', type='hidden')
      input.carry.dlimg.dlimgts.share.startleadtime(name='L', type='hidden', data-default='2.5')
      input.carry.dlimgts.dlimgloc.share(name='region', type='hidden')
      input.pickarea(name='resolution', type='hidden', data-default='0.25')
      input.carry.dlimg.dlauximg.share(name='proba', type='hidden')
      input.carry.dlimg.dlauximg.share.threshold.bodyClass(name='var', type='hidden')
      input.carry.dlimg.dlauximg.share.threshold(name='percentile', type='hidden', data-default='50.0')
      input.dlimg.dlauximg.share.threshold(name='threshold2', type='hidden', data-default='90.0')
      input.dlimg.share(name='layers', value='forecast', checked='checked', type='checkbox')
      input.dlimg.share(name='layers', value='Bihar_dist', checked='checked', type='checkbox')
      input.dlimg.share(name='layers', value='lakes', checked='checked', type='checkbox')
      input.dlimg.share(name='layers', value='rivers_gaz', type='checkbox')
      input.dlimg.share(name='layers', value='streams', type='checkbox')
      input.dlimg.share(name='layers', value='int_streams', type='checkbox')
      input.dlimg.share(name='layers', value='percentile_val', type='checkbox')
    .controlBar   
      fieldset#toSectionList.navitem
        legend Climate and Agriculture
        a.navlink.carryup(rev='section', href='/maproom/Agriculture/bihar.html') Bihar Climate Maproom
      fieldset#chooseSection.navitem
        legend(about='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Forecasts_term')
          span(property='term:label') Forecasts
      //fieldset.navitem.valid
        legend Target Time
        link.starttime(rel='iridl:hasJSON', href='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/S/last/cvsunits//S/parameter/VALUE/info.json')
        select.pageformcopy(name='L')
          optgroup.template(label='Target Time')
            option(class='iridl:values L@value target_date')
      fieldset.navitem
        legend Probability
        select.pageformcopy(name='proba')
          option(value='') exceeding
          option(value='non-exceeding') non-exceeding
        select.pageformcopy(name='var')
          option(value='') Percentile
          option(value='Precipitation') Precipitation
        span#percThresh
          link.threshold(rel='iridl:hasJSON', href='/expert//percentile//unitless/ordered/10/5/90/NewEvenGRID/0/add//name//threshold/def//long_name//threshold/def//fullname//threshold/def/info.json')
          select.pageformcopy.containsAllValids(name='percentile')
            optgroup.template
              option(class='iridl:values percentile@value threshold')
          | %-ile
        span#physThresh
          input.pageformcopy(name='threshold2', type='text', value='90.0', size='5', maxlength='5')
          | mm
    .ui-tabs
      ul.ui-tabs-nav
        li
          a(href='#tabs-1') Description
        li
          a(href='#tabs-2') Dataset Documentation
        li
          a(href='#tabs-4') Instructions
        li
          a(href='#tabs-5') Contact Us
      fieldset.regionwithinbbox.dlimage
        p
          table
            tr
              td(rowspan='2')
                img.dlimgloc(src='/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:83:24:89:28:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:86:25:87:26:bb%29//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif')
              td
                span.valid
                  a.startleadtime(rel='iridl:hasJSON', href='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/S/last/cvsunits//S/parameter/VALUE/L/first/cvsnamedunits//L/parameter/VALUE/info.json')
                  span.template
                    table(bgcolor='f0f0f0')
                      tr
                        td Target Date
                        td Issue Date
                        td Lead Time
                      tr
                        th(class='iridl:value')
                        th(class='iridl:hasIndependentVariables')
                          span(class='iridl:value')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/%28bb:86:25:87:26:bb%29//region/parameter/geoobject/info.json')
          .template
            | Forecast made for 
            span.bold(class='iridl:long_name')
        .valid(style='display: inline-block; text-align: top;')
          a.dlimgloc(rel='iridl:hasJSON', href='/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3AIndia%3ABihar_dist%3Ads)/geoobject/%28bb:86:25:87:26:bb%29//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json')
          .template
            | located in or near 
            span.bold(class='iridl:value')
        h4 Probability of Exceeding
        img.dlimgts(rel='iridl:hasFigureImage', src='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/S/last/cvsunits//S/parameter/VALUE/L/2.5//L/parameter/VALUE/paramfcst2/.precip/%28bb:86:25:87:26:bb%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/fcstmean/.precip//region/get_parameter/geoobject/%5BX/Y%5Dweighted-average/mul/add/grid://name/%28probability%29/def//long_name/%28probability%20of%20exceeding%29/def/9.9999997E-05/0.9999/2/copy/exch/sub/500.0/div/exch/:grid/0./add/dup/-1./mul/1./add/exch/div/ln/exch/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul/%5Bmodl%5Daverage/target_length/mul//long_name/%28Precipitation%29/def//units//mm/def/paramclim/.precip//region/get_parameter/geoobject/%5BX/Y%5Dweighted-average/probability/0.0/add/dup/-1.0/mul/1.0/add/exch/div/ln/1/index/ind/0/VALUE/ind/removeGRID/sub/exch/ind/2/VALUE/ind/removeGRID/div/dup/dataflag/1/masklt/exch/0.0/max/mul/target_length/mul//long_name/%28Precipitation%29/def//units//mm/def/target_date/%7Bforecast/clim/target_date%7Dds//name//flexfcst/def/a-/.clim/probability/0./add//fullname//Climatology/def/-a-/.target_date/S/-a-/.forecast/probability/0./add//fullname//Forecast/def/-a/fig-/medium/black/scatterline/plotlabel/plotlabel/green/scatterline/-fig//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//antialias/true/psdef//XOVY/1.6/psdef/L/last/plotvalue/S/last/plotvalue+.gif')
        h4 Probability Distribution
        img.dlimgts(rel='iridl:hasFigureImage', src='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/S/last/cvsunits//S/parameter/VALUE/L/2.5//L/parameter/VALUE/a-/-a/grid://name/%28probability%29/def/9.9999997E-05/0.9999/2/copy/exch/sub/500.0/div/exch/:grid/0.0/add/dup/-1.0/mul/1.0/add/div/ln/probability/0.0/add//name//myinv/def/exch/use_as_grid/ln//stdvar/renameGRID//long_name/%28proba%29/def/%5Bstdvar%5Dpartial/dup/paramclim/.precip/%28bb:86:25:87:26:bb%29//region/parameter/geoobject/%5BX/Y%5Dweighted-average/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a/stdvar/exch/sub/1/index/div/dup/dataflag/1/masklt/exch/0.0/max/mul/target_length/mul//long_name/%28Precipitation%29/def//units//mm/def/3/-2/roll/mul//fullname//Climatology/def/paramfcst2/.precip//region/get_parameter/geoobject/%5BX/Y%5Dweighted-average/a:/ind/2/VALUE/ind/removeGRID/:a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/fcstmean/.precip//region/get_parameter/geoobject/%5BX/Y%5Dweighted-average/mul/add/stdvar/exch/sub/1/index/div/dup/dataflag/1/masklt/exch/0.0/max/mul/%5Bmodl%5Daverage/target_length/mul//long_name/%28Precipitation%29/def//units//mm/def/exch/5/-1/roll/mul/%5Bmodl%5Daverage//long_name/%28probability%20density%29/def//fullname//Forecast/def/target_date/S/4/-2/roll/fig-/medium/black/scatterline/plotlabel/plotlabel/green/scatterline/-fig//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//antialias/true/psdef//XOVY/1.6/psdef/L/last/plotvalue/S/last/plotvalue/+.gif')
      fieldset.dlimage
        a(rel='iridl:hasFigure', href='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.fcstmean/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/X/83.0/0.25/89.0/GRID/Y/24.0/0.25/28.0/GRID/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/3/-1/roll/mul/add/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7B50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramclim/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/2/VALUE/ind/removeGRID/:a/3/-2/roll/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul/dup/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/mul/3/-2/roll/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/exch/%5BS%5D/regridAverage/mul/add/eexp/dup/1.0/add/div%5Bmodl%5Daverage/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse%7D%7B/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/90.0//threshold2/parameter/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/div/mul/add/eexp/dup/1.0/add/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add%7Dif%5Bmodl%5Daverage/correlationcolorscale/DATA/0/1/RANGE/dup%7Difelse//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/DATA/0/1/RANGE/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/%7Bpercentile_val/forecast/target_date%7Dds//name//flexfcst/def/a-/.forecast/-a-/.percentile_val/-a-/.target_date/-a/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/contours/plotlabel/grey/thin/solid/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/S/last/plotvalue/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//layers%5B//forecast//Bihar_dist//lakes%5Dpsdef//antialias/true/psdef/') visit site
        img.dlimg(rel='iridl:hasFigureImage', src='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.fcstmean/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/X/83.0/0.25/89.0/GRID/Y/24.0/0.25/28.0/GRID/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/3/-1/roll/mul/add/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7B50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramclim/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/2/VALUE/ind/removeGRID/:a/3/-2/roll/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul/dup/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/mul/3/-2/roll/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/exch/%5BS%5D/regridAverage/mul/add/eexp/dup/1.0/add/div%5Bmodl%5Daverage/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse%7D%7B/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/90.0//threshold2/parameter/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/div/mul/add/eexp/dup/1.0/add/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add%7Dif%5Bmodl%5Daverage/correlationcolorscale/DATA/0/1/RANGE/dup%7Difelse//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/DATA/0/1/RANGE/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/%7Bpercentile_val/forecast/target_date%7Dds//name//flexfcst/def/a-/.forecast/-a-/.percentile_val/-a-/.target_date/-a/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/contours/plotlabel/grey/thin/solid/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/S/last/plotvalue/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//layers%5B//forecast//Bihar_dist//lakes%5Dpsdef//antialias/true/psdef/+.gif')
        img.dlauximg(rel='iridl:hasFigureImage', src='/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.fcstmean/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/X/83.0/0.25/89.0/GRID/Y/24.0/0.25/28.0/GRID/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/1/VALUE/ind/removeGRID/:a/3/-1/roll/mul/add/%28Percentile%29//var/parameter/%28Percentile%29/eq/%7B50.0//percentile/parameter/100.0/div/dup/-1.0/mul/1.0/add/div/ln/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramclim/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/a:/ind/0/VALUE/ind/removeGRID/:a:/ind/2/VALUE/ind/removeGRID/:a/3/-2/roll/sub/exch/div/dup/dataflag/1/masklt/exch/0.0/max/mul/dup/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/mul/3/-2/roll/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/exch/%5BS%5D/regridAverage/mul/add/eexp/dup/1.0/add/div%5Bmodl%5Daverage/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add/startcolormap/DATA/0/1/RANGE/transparent/black/RGBdup/0/VALUE/brown//percentile/get_parameter/100.0/div/-1.0/mul/1.0/add//probref/parameter/0.05/sub/3.0/div/VALUE/orange//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/yellow//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/LimeGreen//probref/get_parameter/0.05/add/0.005/add/VALUE/turquoise//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/blue//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/purple/1/VALUE/purple/endcolormap%7D%7Bstartcolormap/DATA/0/1/RANGE/transparent/purple/RGBdup/0/VALUE/blue//percentile/get_parameter/100.0/div//probref/parameter/0.05/sub/3.0/div/VALUE/turquoise//probref/get_parameter/0.05/sub/3.0/div/2.0/mul/VALUE/LimeGreen//probref/get_parameter/0.05/sub/VALUE/moccasin/RGBdup//probref/get_parameter/0.05/add/bandmax/yellow//probref/get_parameter/0.05/add/0.005/add/VALUE/orange//probref/get_parameter/0.05/add/2.0/mul/1.0/add/3.0/div/VALUE/brown//probref/get_parameter/0.05/add/2.0/add/3.0/div/VALUE/black/1/VALUE/black/endcolormap%7Difelse%7D%7B/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.paramfcst2/.precip/X/83/89/RANGEEDGES/Y/24/28/RANGEEDGES/ind/2/VALUE/ind/removeGRID/90.0//threshold2/parameter/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_length/div/mul/add/eexp/dup/1.0/add/div/%28exceeding%29//proba/parameter/%28exceeding%29/eq/%7B-1/mul/1/add%7Dif%5Bmodl%5Daverage/correlationcolorscale/DATA/0/1/RANGE/dup%7Difelse//long_name/%28Probability%20of%20%29//proba/get_parameter/append/def/DATA/0/1/RANGE/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/.target_date/%7Bpercentile_val/forecast/target_date%7Dds//name//flexfcst/def/a-/.forecast/-a-/.percentile_val/-a-/.target_date/-a/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/X/Y/fig-/colors/grey/contours/plotlabel/grey/thin/solid/stroke/blue/rivers_gaz/lakes/thin/streams/int_streams/-fig/S/last/plotvalue/L/first/plotvalue//framelabel/%28%25=%5Btarget_date%5D%20Flexible%20seasonal%20Precipitation%20forecast%20issued%20%25b%5BS%5D%20%25Y%5BS%5D%29/psdef//layers%5B//forecast//Bihar_dist//lakes%5Dpsdef//antialias/true/psdef/+.auxfig/+.gif')
      #tabs-1.ui-tabs-panel(about='')
        h2(align='center', property='term:title') Precipitation flexible seasonal forecast
        p(align='left', property='term:description')
          | This seasonal forecasting system consists of probabilistic precipitation seasonal forecasts based on the full estimate of the probability distribution.
        p
          | Probabilistic seasonal forecasts from multi-model ensembles through the use of 
          span(property='wn30:lexicalForm', rel='wn30:hasSense', resource='[wn30:wordsense-statistical_recalibration-noun-1]') statistical recalibration
          | , based on the historical performance of those models, provide reliable information to a wide range of climate risk and decision making communities, as well as the forecast community. The flexibility of the full probability distributions allows to deliver interactive maps and point-wise distributions that become relevant to user-determined needs.
        p
          | The default map shows the seasonal precipitation forecast probability (colors between 0 and 1) of exceeding the 50
          sup th
          |  percentile of the distribution from historical 1982-2010 climatology. The forecast shown is the latest forecast made (e.g. Dec 2017) for the next season to come (e.g. Jan-Mar 2018). Four different seasons are forecasted and it is also possible to consult forecasts made previously. The forecasts are directly computed from the 
          a(href='https://iri.columbia.edu/our-expertise/climate/forecasts/seasonal-climate-forecasts/methodology/') extended logistic regression
          |  model as probabilities of exceeding (or non-exceeding) of every 5th percentile of the climatological distribution. The specific quantile (in steps of 5%) can then be selected. The user can also specify the historical quantitative value (here seasonal total precipitation in mm) for probability of exceeding or non-exceeding.
        p
          | Clicking on a point on the map will show the local probability of exceeding and probability distribution of the forecast (green) together with the climatological distribution (black).
        p
          b Colors Scales
        p
          | Color scales are colors indicating that the distribution of the forecast tends towards drier (shades of brown) or wetter (shades of blue) conditions than normal (moccasin).
      #tabs-2.ui-tabs-panel
        h2(align='center') Dataset Documentation
        p
          b Forecast ELR coefficients:
          |  1&ring; spatial resolution multi-model ensemble mean and ELR coefficients available 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/', target='_blank') here
          | .
        p
          b Historical precipitation:
          |  ELR coefficients derived from the CPC-CMAP-URD dataset available 
          a(href='http://iridl.ldeo.columbia.edu/SOURCES/.IRI/.FD/.NMME_Seasonal_Forecast/.ELR_coeffs_IMD/') here
          |  at 1&ring; spatial resolution.
      #tabs-4.ui-tabs-panel
        h2(align='center') Instructions
        .buttonInstructions
      #tabs-5.ui-tabs-panel
        h2(align='center') Helpdesk
        p
          | Contact 
          a(href='mailto:help@iri.columbia.edu?subject=Flexible Forecast: Precipitation') help@iri.columbia.edu
          |  with any technical questions or problems with this Map Room.
    .optionsBar
      fieldset#share.navitem
        legend Share
      fieldset#contactus.navitem.langgroup
