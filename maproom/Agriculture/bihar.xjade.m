=== canonical = "bihar.html"
=== title = "<<<es:Sala de Mapas|||en:Bihar Climate Maproom>>>"
doctype transitional
html(xmlns='http://www.w3.org/1999/xhtml', xmlns:wms='http://www.opengis.net/wms#', xmlns:iridl='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#', xmlns:maproom='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#', xmlns:term='http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#', xmlns:xs='http://www.w3.org/2001/XMLSchema#', version='XHTML+RDFa 1.0', xml:lang='<<<Language>>>')
  head
    meta(name='viewport', content='width=device-width; initial-scale=1.0;')
    link(rel='stylesheet', type='text/css', href='/uicore/uicore.css')
    link(rel='stylesheet', type='text/css', href='/localconfig/ui.css')
    link(rel='canonical', href='<<<canonical>>>')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate')
    link(rel='term:isDescribedBy', href='http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast')
    link.carryLanguage(rel='home', href='http://iri.columbia.edu/', title='IRI')
    link.carryLanguage(rel='home alternate', type='application/json', href='/maproom/navmenu.json')
    link(rel='term:icon', href='/home/.jdcorral/.IRI/.Analyses/.Tailored/.Northern_India_v4p0/.precipitation/.historical/X/%2883E%29/%2888.5E%29/RANGEEDGES/Y/%2824N%29/%2828N%29/RANGEEDGES/X/83.0/0.1/88.5/GRID/Y/24.0/0.1/28.0/GRID/dup/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/gid/29/VALUE/rasterize/gid/removeGRID/mul/SOURCES/.Features/.Political/.World/.firstOrder_GAUL/.the_geom/gid/%282064%29/VALUES/SOURCES/.Features/.Political/.India/.Bihar_dist/.the_geom/SOURCES/.Features/.Political/.India/.Provinces_WHO/.the_geom/X/Y/fig-/colors/grey/thinnish/stroke/thin/white/stroke/black/stroke/countries_gaz/-fig/X/83/88.5/plotrange/Y/24/28/plotrange//plotaxislength/500/psdef//antialias/true/psdef//framelabel//seasonalStat/get_parameter/%28PerDA%29/1/index/eq/not/%7B//yearlyStat/get_parameter/%28%20of%20%29/append/exch/append%7Dif/%28%20in%20season%20%29/append/lpar/append//seasonStart/get_parameter/append/%28%20%29/append//DayStart/get_parameter/append/%28%20-%20%29/append//seasonEnd/get_parameter/append/%28%20%29/append//DayEnd/get_parameter/append/rpar/append/%28%20over%20years%20from%20%29/append//YearStart/get_parameter/append/%28%20to%20%29/append//YearEnd/get_parameter/append/psdef//plotborderbottom/72/psdef+//plotaxislength+432+psdef//framelabel+%28Mean%20of%20TotRain%20in%20season%20%28Jan%201%20-%20Jan%2031%29%20over%20years%20from%201901%20to%202015%29+psdef//plotborder+0+psdef//antialias+true+psdef//color_smoothing+1+psdef+.gif')
    script(type='text/javascript', src='/uicore/uicore.js')
    script(type='text/javascript', src='/localconfig/ui.js')
    <<<altLanguages(Languages, Language, canonical, '    ')>>>
    meta(property='maproom:Sort_Id', content='a05')
    title <<<title>>>
  body
    form#pageform(name='pageform')
      input.carryLanguage(name='Set-Language', type='hidden')
    .controlBar
      fieldset.navitem
        legend Maproom
        a.navlink.carryup(rev='section', href='index.html') Climate and Agriculture
      fieldset.navitem
        legend Climate and Agriculture
        span.navtext <<<title>>>
    style.
      @media only all and (min-width: 1250px) {
      .ui-tabs-nav li.toggle {
      display: inline-block;
      float: right;
      }
      .ui-tabs-panel-float {
      float: right;
      width: 36%;
      margin-left: 2px;
      resize: horizontal;
      overflow: hidden;
      }
      .itemIcon {
      padding-top: 16px;
      }
      .searchDescription {
      max-width: 340px;
      }
      .leftcolumnset {
      max-width: 340px;
      }
      .rightcol {
      width: 880px;
      }
      .rightcol .ui-tabs {
      width: 902px;
      }
      .rightcol .ui-tabs-panel {
      width: 880px;
      -webkit-column-count: 3;
      column-count: 3;
      -moz-column-count: 3;
       }
       .rightcol .ui-tabs-panel .itemGroup,
       .rightcol .ui-tabs-panel  .itemGroupDescription{
       column-span: all;
       -moz-column-span: all;
       -webkit-column-span: all;
       }
       }
    div
      #content.searchDescription
        h2(property='term:title') <<<title>>>
<<<es:
        p(align='left', property='term:description') La Sala de Mapas es una colección de mapas para el monitoreo de condiciones climáticas y sus impactos en la sociedad a través de información proveniente de bases de datos satelitales.
|||en:
        p(align='left', property='term:description') A collection of interactive maps for assessing climate conditions and risks over Bihar, based on past and current IMD high-resolution daily rainfall and temperature data, and IRI seasonal climate forecasts.
        p(align='left') Clicking anywhere on the maps brings up time series of chosen quantities at the district level.
>>>
      .rightcol.tabbedentries(about='/maproom/Agriculture/bihar.html')
        link(rel='maproom:tabterm', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Monitoring_term')
        link(rel='maproom:tabterm', href='http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Bihar_Forecasts_term')
    .optionsBar
      fieldset#share.navitem
        legend <<<en:Share|||es:Compartir|||fr:Partager>>>
      fieldset#contactus.navitem.langgroup
        legend <<<en:Contact Us|||es:Contáctenos>>>
