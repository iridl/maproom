
  /* Function Designed for control the interaction of vars changes (Precipitation and Temperature)
     this is needed for listener used on probability controls
  */
  function ChangeVar() {
    document.getElementById("var").value = document.getElementById("varBox").value;
    document.getElementById("varuser").value ="";
    updatePageForm();
   }
   /* controling the Leads in case the user picked a lead time not including on the Leads 6 options,
     for example, the user picked L=8.5 on Lead9 option then change to Lead6 option, in this case
     the Lead select will move to L=7.5
   */
   function ChangeLead() {
     const LSelect = document.querySelector('select[name="L"]');
     document.getElementById("leadsf").value = document.getElementById("leadsfBox").value;
     if ( parseFloat(LSelect.value) > 6) {
        document.getElementById("Lead").value ="6.0";
      }
      updatePageForm();
    }
    function ChangeLeadSub() {
      const LSelect = document.querySelector('select[name="L"]');
      const VarSelect = document.querySelector('select[name="leadsf"]');
      document.getElementById("leadsf").value = VarSelect.value;
      /*document.getElementById("leadsf").value = document.getElementById("leadsfBox").value;*/
      if ( VarSelect.value === ".forecast_biweekly") {
       if (LSelect.value === "4.5")  {
          document.getElementById("Lead").value ="15.0";
        } else if (LSelect.value === "11.5") {
         document.getElementById("Lead").value ="22.0";
        }
      }

      if ( VarSelect.value === ".forecast_week12") {
       if (LSelect.value === "15.0")  {
          document.getElementById("Lead").value ="4.5";
        } else if (LSelect.value === "22.0") {
         document.getElementById("Lead").value ="11.5";
        }
      }

       updatePageForm();
     }
  /* add listener events for controling the probability controls */
   document.addEventListener('DOMContentLoaded', function() {
       const varSelect = document.querySelector('select[name="var"]');
       const varUserSelect = document.querySelector('select[name="varuser"]');

       if (varSelect.value === '.precip') {
           varUserSelect.querySelector('option[value="Temperature"]').classList.add('hide-option');
           varUserSelect.querySelector('option[value="Precipitation"]').classList.remove('hide-option');
       }

       varSelect.addEventListener('change', function() {
           if (this.value === '.precip') {
               varUserSelect.querySelector('option[value="Precipitation"]').classList.remove('hide-option');
               varUserSelect.querySelector('option[value="Temperature"]').classList.add('hide-option');
               document.getElementById("threshold2").value ="90.0";
           } else {
               varUserSelect.querySelector('option[value="Precipitation"]').classList.add('hide-option');
               varUserSelect.querySelector('option[value="Temperature"]').classList.remove('hide-option');
               document.getElementById("threshold2").value ="20.0";

           }
       });
   });

  /* Forcing the layers selection */
   function setCheckboxValues(checkboxValues) {
     const checkboxes = document.querySelectorAll('input[name="layers"]');
     checkboxes.forEach((checkbox) => {
       checkbox.checked = checkboxValues.includes(checkbox.value);
     });
   }
