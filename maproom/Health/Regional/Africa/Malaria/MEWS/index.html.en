<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Dekadal (10-day) Precipitation</title>
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="es" href="index.html?Set-Language=es" />
<link class="altLanguage" rel="alternate" hreflang="fr" href="index.html?Set-Language=fr" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<meta property="maproom:Entry_Id" content="Health_Regional_Africa_Malaria_MEWS" />
<meta property="maproom:Sort_Id" content="e01" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Malaria"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#Africa_3327_1"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#health"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_Malaria_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#health"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Epidemic_Malaria"/>
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Monitoring_The_Environment"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Africa"/>
<link rel="term:icon" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/X/Y/fig-/colors/grey/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//plotborder+0+psdef+.gif" />
<style>
.dlimgtsbox {
width: 49%;
display: inline-block
 }
</style>
</head>
<body xml:lang="en">
<form name="pageform" id="pageform">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="carry dlimg dlauximg info share" name="var" type="hidden" />
<input class="dlimg share dlimgloc dlimglocclick" name="bbox" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
<input class="share dlimg" name="T" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="dlimg dlauximg" name="plotaxislength" type="hidden" data-default="432" />
<input class="pickarea" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Africa:Districts:ds" />
<input class="dlimg share" name="layers" value="est_prcp" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="mews_prov" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="mews_dist" type="checkbox" />
<input class="dlimg share" name="layers" value="Risk_map"  type="checkbox" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Climate and Health</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/Health/Regional/Africa/Malaria/System.html">Malaria Early Warning System</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Monitoring_The_Environment"><span property="term:label">Monitoring The Environment</span></legend>
            </fieldset>
 <fieldset class="navitem"><legend>Spatially Average Over</legend><select class="pageformcopy" name="resolution">
<option value=".1">11x11 km box</option>
<option value=".3">33x33 km box</option>
<option value=".5">55x55 km box</option>
<option value="1.">111x111 km box</option>
<option value="irids:SOURCES:Features:Political:Africa:Districts:ds">district boundaries</option>
</select></fieldset>
 </div>
<style>
.leftpart {
display: inline-block;
width: 60%
}
.rightpart {
margin-left: 10pt;
display: inline-block;
width: 30%
}
@media only all and (max-width: 400px) {
.leftpart { width: 100%}
.rightpart {width: 95%}
}
.plotheading {
text-align: center;
margin: 0px;
}
</style>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
      <li><a href="#tabs-2" >Dataset Documentation</a></li>
      <li><a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/">Dataset</a></li>
      <li><a href="#tabs-4" >Contact Us</a></li>
      <li><a href="#tabs-5" >Instructions</a></li>
    </ul>
<fieldset class="regionwithinbbox dlimage" about="">
<a class="dlimgts" rel="iridl:hasTable" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/null/null/RANGE//plotlast/200/def//fullname/%28Precipitation%29def/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject%5BX/Y%5Dweighted-average%5BT%5D2/SM121/T/2/index/.T//pointwidth/0/def/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/2/index/.T/replaceGRID//fullname/%282000-20%20Average%29def/T/3/1/roll/table:/3/:table/"></a>
<img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-20%2C-40%2C55%2C40%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-20%2C-40%2C-20%2C-40%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgts" rel="iridl:hasJSON" href="/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
<div class="template">
Observations for <span class="bold iridl:long_name"></span>
</div>
</div>
<br />
<div class="dlimgtsbox">
<div>a)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/200/RANGE/toNaN%5BT%5DREORDER/dup/0/mul/T/fig:/deltabars/:fig//est_prcp/0/200/plotrange/T/14579.0/last/plotrange//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>b)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/(bb%3A17%3A4.50%3A17.5%3A5%3Abb)//region/parameter/geoobject/[X/Y]weighted-average/DATA/null/null/RANGE//plotlast/200/def//fullname/(Precipitation)/def/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject/[X/Y]weighted-average/[T]2/SM121/T/2/index/.T//pointwidth/0/def/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/2/index/.T/replaceGRID//fullname/(2000-20 Average)/def/2/copy/maskgt/1/index/T/fig%3A/blue/deltabars/brown/deltabars/|/black/line/grey/line/%3Afig//est_prcp/0/200/plotrange/T/14579.0/last/plotrange//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>c)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/(bb%3A[17%2C4.50%2C17.5%2C5])//region/parameter/geoobject/[X/Y]weighted-average/a%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/3/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/2/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/1/sub/s%3D%3D/VALUES/%3Aa/4/{4/-1/roll/T/last/cvsunits/( )/rsearch/pop/nip/nip/nip//fullname/exch/def}repeat/3/index/.fullname/interp/1/Jan/2/index/julian_day/31/Dec/4/-1/roll/julian_day/dekadGRID/3/{4/-1/roll/T/2/index/replaceGRID/exch}repeat/pop/4/-1/roll/1/index/.T/fig%3A/solid/verythin/grey/line/magenta/line/blue/line/medium/black/line/%3Afig//est_prcp/0/250/plotrange//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>d)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/(bb%3A17%3A4.50%3A17.5%3A5%3Abb)//region/parameter/geoobject/[X/Y]weighted-average/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject/[X/Y]weighted-average/T/2/index/.T//pointwidth/0/def/a%3A/.first/cvsunits/%3Aa%3A/.last/cvsunits/%3Aa/RANGE/T/2/index/.T/replaceGRID/T/last/365/sub/last/RANGE/T/differential_div/T/integral//fullname/(Cumulative 2000-20 Avg)/def/2/1/roll/T/last/365/sub/last/RANGE/T/differential_div/T/integral//fullname/(Cumulative Precipitation)/def/2/1/roll/2/copy/maskgt/1/index/T/fig%3A/blue/deltabars/brown/deltabars/|/black/medium/line/grey/line/%3Afig//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div>
<p>
a) Dekadal (i.e., ~10-daily) precipitation estimates for the selected region from Dec 1999 to the present.
</p><p>
b) Same as (a) (solid black line) with the addition of the recent short-term average precipitation for the same region (grey dotted line).  The blue (red) bars are indicative of estimates that are above (below) the short-term average. Note that the short-term average precipitation data has been smoothed.
</p><p>
c) Same as (a) for the current year (thick black line), as indicated by the axis labels.
Precipitation estimates from previous years are also shown (blue-1 yr from present; magenta-
2 yrs from present; grey-3 yrs from present).
</p><p>
d) Cumulative dekadal satellite-derived precipitation estimates (solid black line) and the cumulative recent short-term average precipitation (grey dotted line) for the most
recent 12-month period in the selected region.  The blue (red) bars are indicative of estimates that are above (below) the short-term average.
</p><p>
<b>NOTE:</b> <u>The recent short-term average (2000-2020) of precipitation should not be interpreted as a climatological normal</u>, which is typically based on a long-term (e.g., 30-year) time series. The length of this short-term average will increase over time as more data becomes available. (An additional year of data will be included in the average during January of each year.)  Despite the limitations that the short-term average imposes, it may provide insight into changes in malaria risk in areas where precipitation anomalies are the principal cause of malaria epidemics by providing a recent historical reference.
</p>
</fieldset>
<fieldset class="dlimage">
<a rel="iridl:hasFigure" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/">visit site</a>
<img class="dlimg" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/+.gif" />
<img class="dlauximg" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/+.auxfig/+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
 <h2 align="center" property="term:title">Dekadal (10-day) Precipitation </h2>
  <p align="left" property="term:description">This map shows dekadal (10-day) precipitation estimates from the Climate Prediction Center. </p>

<p>
Precipitation, especially in warm semi-arid and desert fringe areas, is one of the factors responsible for creating the conditions which lead to the formation of sufficient surface water and moisture for mosquito breeding sites. Monitoring precipitation on short term time scales (1-2 weeks) may aid in determining the location and timing of a potential outbreak.
</p>
<p>
By placing recent precipitation in historical context, comparisons can be made to past outbreaks and useful early warning information can be developed for epidemic prone regions.
</p>


<h4>References</h4>
<p>
Grover-Kopec E., Kawano M., Klaver R. W., Blumenthal B., Ceccato P., Connor S. J. <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC548290/">An online operational rainfall-monitoring resource for epidemic malaria early warning systems in Africa</a>. <i>Malaria Journal</i>, 2005, <b>4</b>:6.
</p>
</div>
<div id="tabs-2" class="ui-tabs-panel">
<h2  align="center">Dataset Documentation</h2>
<h4>Precipitation Estimates</h4>
<dl class="datasetdocumentation">
<dt>Data</dt>
<dd>Dekadal precipitation estimates over land areas on a 0.1° x 0.1° lat/lon grid</dd>
<dt>Data Source</dt><dd> Climate Prediction Center/Famine Early Warning System
(<a href="http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.TEN-DAY/.RFEv2/.dataset_documentation.html">CPC/FEWS RFE2.0</a>)*</dd>
</dl>
<h4>Epidemiological Mask</h4>
<dl class="datasetdocumentation">
<dd>This map utilizes an <span property="wn30:lexicalForm"  rel="wn30:hasSense" resource="[wn30:wordsense-epidemiological_mask-noun-1]">epidemiological mask</span> to isolate areas prone to new exposure of malaria. The mask allows for MEWS to be exclusively illustrated in areas where malaria is considered to be epidemic, therefore, areas where malaria transmission is considered absent or endemic are excluded. The mask is based purely on climatic constraints to malaria transmission and does not yet account for areas in the northern and southern margins of the continent where control has eliminated malaria risk.</dd>

<dt>Reference</dt><dd>WHO: Final report on the 3rd meeting of the RBM Technical Resource Network on Epidemic Prevention and Control. Geneva: World Health Organization; 2002.  </dd>
</dl>
<p>
<font size="-1">*More dekadal and daily data from FEWS is available from the
<a href="http://igskmncnwb015.cr.usgs.gov/adds/">Africa Data Dissemination Service</a>.</font>
</p>
</div>
<div class="ui-tabs-panel-hidden">
<h2 align="center">Dataset</h2>
<p>
<a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/">Access the dataset used to create this map.</a>
</p>
</div>
<div id="tabs-5" class="ui-tabs-panel" instructions="">
 <h2 align="center">Instructions</h2>
  <p align="left">The default map on this page shows the most recent dekadal (10-day) estimated precipitation amounts over Africa. As the user moves the cursor over the map, a tool bar will appear on top that will allow for previous dekads (1999-the most recent dekad) to be displayed.</p>

<p>The interface consists of a clickable map that allows users to generate customized time series graphs. When a desired location is clicked, four time series graphs are generated that provide analyses of recent precipitation with respect to that of recent seasons and the short-term multiple-year average.
</p>
<div class="buttonInstructions"></div>
</div>
<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Helpdesks</h2>
<p>
Contact <a href="mailto:help@iri.columbia.edu?subject=Maproom">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room, for example, the forecasts not displaying or updating properly.
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>
