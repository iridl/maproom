<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      xmlns:wn30="http://purl.org/vocabularies/princeton/wn30/"
      xmlns:wordnet-ontology="http://wordnet-rdf.princeton.edu/ontology#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Precipitación decadal (p.ej. ~10-diariamente)</title>
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link class="altLanguage" rel="alternate" hreflang="fr" href="index.html?Set-Language=fr" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
<meta property="maproom:Entry_Id" content="Health_Regional_Africa_Malaria_MEWS" />
<meta property="maproom:Sort_Id" content="e01" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Rate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Malaria"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#Africa_3327_1"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#health"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_Malaria_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#health"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#dekad"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Epidemic_Malaria"/>
      <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Monitoring_The_Environment"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Africa"/>
<link rel="term:icon" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/X/Y/fig-/colors/grey/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//plotborder+0+psdef+.gif" />
<style>
.dlimgtsbox {
width: 49%;
display: inline-block
 }
</style>
</head>
<body xml:lang="es">
<form name="pageform" id="pageform">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="carry dlimg dlauximg info share" name="var" type="hidden" />
<input class="dlimg share dlimgloc" name="bbox" type="hidden" />
<input class="share dlimgts dlimgloc" name="region" type="hidden" />
<input class="share dlimg" name="T" type="hidden" />
<input class="dlimg dlauximg" name="plotaxislength" type="hidden" data-default="432" />
<input class="pickarea" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:Africa:Districts:ds" />
<input class="dlimg share" name="layers" value="est_prcp" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="countries_gaz" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="mews_prov" checked="checked" type="checkbox" />
<input class="dlimg share" name="layers" value="mews_dist" type="checkbox" />
<input class="dlimg share" name="layers" value="Risk_map"  type="checkbox" />
</form>
<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Clima y Salud</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/Health/Regional/Africa/Malaria/System.html">Precipitación decadal (p.ej. ~10-diariamente)</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Monitoring_The_Environment"><span property="term:label">el analisis de la ambiente</span></legend>
            </fieldset>
 <fieldset class="navitem"><legend>Promedios Espaciales sobre</legend><select class="pageformcopy" name="resolution">
<option value=".1">11x11 km box</option>
<option value=".3">33x33 km box</option>
<option value=".5">55x55 km box</option>
<option value="1.">111x111 km box</option>
<option value="irids:SOURCES:Features:Political:Africa:Districts:ds">límites de los distritos</option>
</select></fieldset>
 </div>
<style>
.leftpart {
display: inline-block;
width: 60%
}
.rightpart {
margin-left: 10pt;
display: inline-block;
width: 30%
}
@media only all and (max-width: 400px) {
.leftpart { width: 100%}
.rightpart {width: 95%}
}
.plotheading {
text-align: center;
margin: 0px;
}
</style>
<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripción</a></li>
      <li><a href="#tabs-2" >Documentación de la base de datos</a></li>
      <li><a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/">Base de Datos</a></li>
      <li><a href="#tabs-4" >Instructiónes</a></li>
      <li><a href="#tabs-5" >Contáctenos</a></li>
    </ul>
<fieldset class="regionwithinbbox dlimage" about="">
<a class="dlimgts" rel="iridl:hasTable" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/null/null/RANGE//plotlast/200/def//fullname/%28Precipitation%29def/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject%5BX/Y%5Dweighted-average%5BT%5D2/SM121/T/2/index/.T//pointwidth/0/def/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/2/index/.T/replaceGRID//fullname/%282000-20%20Average%29def/T/3/1/roll/table:/3/:table/"></a>
<img class="dlimgloc" src="/SOURCES/.WORLDBATH/.bath/X/Y/%28bb:-20%2C-40%2C55%2C40%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28bb:-20%2C-40%2C-20%2C-40%29dup//region/parameter/dup//bbox/get_parameter/eq/%7Bpop%7D%7Bnip%7Difelse/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index%5BX/Y%5Dweighted-average/exch/Y/exch%5BX/Y%5Dweighted-average/X/Y/fig-/lightgrey/mask/grey/mews_prov/black/countries_gaz/red/fill/red/smallpushpins/-fig//plotbordertop/8/psdef//antialias/true/psdef//plotaxislength/120/psdef//plotborder/0/psdef//plotborderleft/8/psdef//plotborderright/8/psdef//plotborderbottom/8/psdef/+.gif" />
<div class="valid" style="display: inline-block; text-align: top;">
<a class="dlimgts" rel="iridl:hasJSON" href="/expert/%28bb:1:2:3:4:bb%29//region/parameter/geoobject/info.json"></a>
<div class="template">
Observaciones para <span class="bold iridl:long_name"></span>
</div>
</div>
<br />
<div class="dlimgtsbox">
<div>a)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/0/200/RANGE/toNaN%5BT%5DREORDER/dup/0/mul/T/fig:/deltabars/:fig//est_prcp/0/200/plotrange/T/14579.0/last/plotrange//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>b)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/DATA/null/null/RANGE//plotlast/200/def//fullname/%28Precipitation%29def/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject%5BX/Y%5Dweighted-average%5BT%5D2/SM121/T/2/index/.T//pointwidth/0/def/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/2/index/.T/replaceGRID//fullname/%282000-20%20Average%29def/2/copy/maskgt/1/index/T/fig:/blue/deltabars/brown/deltabars/|/black/line/grey/line/:fig//est_prcp/0/200/plotrange//est_prcp/0/200/plotrange/T/14579.0/last/plotrange//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>c)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/(bb%3A[17%2C4.50%2C17.5%2C5])//region/parameter/geoobject/[X/Y]weighted-average/a%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/3/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/2/sub/s%3D%3D/VALUES/%3Aa%3A/T/last/cvsunits/( )/rsearch/pop/nip/nip/interp/1/sub/s%3D%3D/VALUES/%3Aa/4/{4/-1/roll/T/last/cvsunits/( )/rsearch/pop/nip/nip/nip//fullname/exch/def}repeat/3/index/.fullname/interp/1/Jan/2/index/julian_day/31/Dec/4/-1/roll/julian_day/dekadGRID/3/{4/-1/roll/T/2/index/replaceGRID/exch}repeat/pop/4/-1/roll/1/index/.T/fig%3A/solid/verythin/grey/line/magenta/line/blue/line/medium/black/line/%3Afig//est_prcp/0/250/plotrange//plotborder/72/psdef//plotbordertop/15/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div><div class="dlimgtsbox">
<div>d)</div>
<img class="dlimgts regionwithinbbox" rel="iridl:hasFigureImage" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/%28bb%3A17%3A4.50%3A17.5%3A5%3Abb%29//region/parameter/geoobject%5BX/Y%5Dweighted-average/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.c0020/.est_prcp//region/get_parameter/geoobject%5BX/Y%5Dweighted-average/T/2/index/.T//pointwidth/0/def/a:/.first/cvsunits/:a:/.last/cvsunits/:a/RANGE/T/2/index/.T/replaceGRID/T/last/365/sub/last/RANGE/T/differential_div/T/integral//fullname/%28Cumulative%202000-20%20Avg%29def/2/1/roll/T/last/365/sub/last/RANGE/T/differential_div/T/integral//fullname/%28Cumulative%20Precipitation%29def/2/1/roll/2/copy/maskgt/1/index/T/fig:/blue/deltabars/brown/deltabars/|/black/medium/line/grey/line/:fig//plotborder/72/psdef//plotbordertop/20/psdef//plotborderbottom/62/psdef//plotaxislength/250/psdef//antialias/true/psdef//XOVY/null/psdef/+.gif" />
</div>
<p>
a) Precipitación decadal (p.ej. ~10-diariamente) estimada para la región seleccionada desde Dic 1999 hasta el presente.
</p><p>
b) Igual que (a) (línea sólida negra) con la adición del promedio reciente de precipitación a corto plazo para la misma región (línea punteada gris). Las barras azules (rojo) son indicativos de estimaciones que están por encima (por debajo) de la media a corto plazo. Note que los datos de precipitación a corto plazo han sido suviazados.
</p><p>
c) Igual que (a) para el año actual (línea gruesa negra), según lo indicado por las etiquetas de los ejes.
Precipitación estimada de años anteriores también se muestran (azul -1 año desde el presente; magenta-
2 años desde el presente; gris -3 años desde el presente).
</p><p>
d) Acumulados decadales de precipitación estimada obtenida por satélite (línea de color negro sólido) y el acumulado del promedio reciente a corto plazo de precipitación (línea gris punteada) para
el período de 12 meses más reciente en la región seleccionada. Las barras azules (rojo) son indicativos de estimaciones que están por encima (por debajo) de la media a corto plazo.
</p><p>
<b>NOTA:</b> <u> El promedio reciente a corto plazo de precipitación (2000-2020) no debe interpretarse como una normal climatológica</u>, la cual es típicamente basada en series de tiempo a largo plazo (p. ej., 30-años). La longitud de este promedio a corto plazo aumentará con el tiempo a medida que más datos estén disponibles. (Un año adicional de datos se incluirá en el promedio de los meses de enero de cada año.) A pesar de las limitaciones que el promedio a corto plazo impone, puede dar una idea de los cambios en el riesgo de malaria en las zonas donde las anomalías de precipitación son la principal causa de las epidemias de malaria proporcionando una referencia histórica reciente.
</p>
</fieldset>
<fieldset class="dlimage">
<a rel="iridl:hasFigure" href="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/">visit site</a>
<img class="dlimg" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/+.gif" />
<img class="dlauximg" src="/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/MEWSprcp_colors/SOURCES/.Features/.Epidemiological/.Malaria/.Risk_map/.the_geom/%7Best_prcp/risk%7D/ds/a-/.est_prcp/-a-/.risk/-a/X/Y/fig-/colors/red/stroke/grey/verythin/mews_dist/black/verythin/mews_prov/black/thin/countries_gaz/-fig//T/last/plotvalue//antialias/true/psdef//layers%5B//est_prcp//mews_prov//countries_gaz%5Dpsdef/+.auxfig/+.gif" />
</fieldset>
 <div id="tabs-1" class="ui-tabs-panel" about="">
 <h2 align="center" property="term:title">Precipitación decadal (p.ej. ~10-diariamente)</h2>
  <p align="left" property="term:description">SATM es un producto de monitoreo de  lluvias basado en estimaciones de precipitación decadal del Centro de Predicción del Clima. La interfaz permite a los usuarios ver las estimaciones recientes de lluvias con una perspectiva estacional e histórica reciente.  Los análisis de series de tiempo de los datos de precipitación son generados en base a los parámetros seleccionados por el usuario.</p>
<p>La lluvia, es gran medida responsable de crear condiciones favorables que permiten la
suficiente agua superficial para la formación de los criaderos de mosquitos y, por tanto,
es reconocida como uno de los principales factores que influyen la transmisión de malaria
en zonas cálidas semi-áridas y en las franjas desérticas. Epidemias graves a menudo ocurren
en estas regiones después de lluvias excesivas y, en donde períodos de sequía e inseguridad
alimentaria continúan, pueden ser especialmente graves.
</p><p>
En consecuencia, el monitoreo de precipitación constituye uno de los elementos esenciales
para el desarrollo de Sistemas integrados de Alerta Temprana contra la Malaria (SATM) en
el África Sub-Sahariana, como indica la Organización Mundial de la Salud.
</p><p>
El mapa en esta página muestra cantidades estimadas de precipitación  decadal (aproximadamente 10 días)
en África. El mapa por defecto muestra los totales de precipitación para la década más reciente  disponible,
pero los totales para décadas previas también pueden ser mostrados. Al hacer clic en una ubicación en el mapa,
el usuario puede generar cuatro gráficos de series de tiempo las cuales proporcionan análisis de promedios de
precipitación recientes sobre un distrito administrativo o en el área de tamaño seleccionada, con respecto a
la reciente estación y promedio multianual a corto plazo. Estos análisis de precipitación dentro de un
contexto histórico están destinados a proporcionar información útil para la alerta temprana de epidemias
en regiones propensas a epidemias.
</p>
<h4>Referencias</h4>
<p>
Grover-Kopec E., Kawano M., Klaver R. W., Blumenthal B., Ceccato P., Connor S. J. <a href="http://www.ncbi.nlm.nih.gov/pmc/articles/PMC548290/">An online operational rainfall-monitoring resource for epidemic malaria early warning systems in Africa</a>. <i>Malaria Journal</i>, 2005, <b>4</b>:6.
</p>
</div>
<div id="tabs-2" class="ui-tabs-panel">
<h2  align="center">Documentación de la Base de Datos</h2>
<h4>Precipitación Estimada</h4>
<dl class="datasetdocumentation">
<dt>Data</dt>
<dd>Precipitación decadal estimada sobre áreas de superficie terrestre a 0.1° x 0.1° lat/lon malla</dd>
<dt>Fuente de Datos</dt><dd> Climate Prediction Center/Famine Early Warning System
(<a href="http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.TEN-DAY/.RFEv2/.dataset_documentation.html">CPC/FEWS RFE2.0</a>)*</dd>
</dl>
<h4><span property="wn30:lexicalForm"  rel="wn30:hasSense" resource="[wn30:wordsense-epidemiological_mask-noun-1]">Máscara Epidemiológica</span></h4>
<dl class="datasetdocumentation">
<dt>Máscara</dt><dd>Ilustrando zonas donde la malaria es considerada epidémica. Áreas donde la transmisión de malaria es considerada ausente o endémica son por lo tanto excluidas. Esta máscara esta basada puramente en limitaciones climáticas para la transmisión de malaria, y todavía no representa los márgenes de zonas norte o sur del continente donde el control ha elimiado el riesgo a malaria.</dd>
<dt>Referencia</dt><dd>OMS: Reporte Final de la tercera reunión de la Red Técnica de Recuros RBM de Prevención y Control en Epidemias. Ginebra: Organización Mundial de la Salud; 2002.  </dd>
</dl>
<p>
<font size="-1">*Más datos decadales y diarios del FEWS estan disponibles en
<a href="http://igskmncnwb015.cr.usgs.gov/adds/">Africa Data Dissemination Service</a>.</font>
</p>
</div>
<div class="ui-tabs-panel-hidden">
<h2 align="center">Base de Datos</h2>
<p>
<a href="http://iridl.ldeo.columbia.edu/expert/SOURCES/.NOAA/.NCEP/.CPC/.FEWS/.Africa/.TEN-DAY/.RFEv2/.est_prcp/">Access the dataset used to create this map.</a>
</p>
</div>
<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Instructiónes</h2>
<div class="buttonInstructions"></div>
</div>
<div id="tabs-5"  class="ui-tabs-panel">
<h2  align="center">Servicios de Asistencia</h2>
<p>
Contacte <a href="mailto:help@iri.columbia.edu?subject=Maproom">help@iri.columbia.edu</a> con cualquier pregunta técnica o problema con este Cuarto de Mapas, por ejemplo, los pronósticos no se muestran o actualizan adecuadamente.
</p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>
