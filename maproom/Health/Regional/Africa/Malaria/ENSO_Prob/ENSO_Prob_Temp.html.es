<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.1"
      >

<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0;" />

  <title>Probabilidades de Condiciones Calientes o Frías según la Fase de ENSO</title>
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>
<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" />
  <link class="share" rel="canonical" href="ENSO_Prob_Temp.html" />
  <link class="altLanguage" rel="alternate" hreflang="en" href="ENSO_Prob_Temp.html?Set-Language=en" />
<link class="altLanguage" rel="alternate" hreflang="fr" href="ENSO_Prob_Temp.html?Set-Language=fr" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
  <link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
  <meta xml:lang="" property="maproom:Entry_Id" content="ENSO_Prob_Temp" />
  <meta xml:lang="" property="maproom:Sort_Id" content="e12b" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Health_Regional_Africa_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Atmosphere"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#ENSO_Prob_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#interactive"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
    <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/irigaz.owl#Africa_3327_1"/>
    <link rel="term:icon" href="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale/tmp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE//name//proba/def//long_name/%28probability%29/def//fullname//proba/def/a-/-a/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL//name//secondadmin/def/.the_geom/:a:/.firstOrder_GAUL//name//firstadmin/def/.the_geom/:a:/.Countries_GAUL//name//countries/def/.the_geom/:a/X/Y/fig-/colors/grey/thin/stroke/stroke/black/thinnish/stroke/-fig//antialias/true/psdef//layers%5B//proba//countries%5Dpsdef+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef/+.gif" />

  <style>
    div {
    padding-left:15px;
    padding-right:15px;
    text-align: justify;
          }

    p {
    text-align: justify;
      }

    .p1 {
    padding-left:5px;
    padding-right:20px;
    display: block;
        }

    a {
    text-decoration:none;
    font-weight:bold;
    color:#175e88;
      }

    a:hover {
    text-decoration:none;
    font-weight:bold;
    color:#FF8C00;
                }

    .titre1 {
    padding-top:15px;
    padding-bottom:15px;
    text-align:center
            }

    .buttonInstructions.bis {
    margin-left:-45px;
    padding-bottom:10px;
    margin-bottom:20px;
                            }

    .dlauximg.bis {
    padding-left:32px;
    width:93%;
                  }

    .dlimgts.bis1 {
    margin-right:10px;
    padding-top:25px;
                  }

    .dlimgts.bis2 {
    padding-bottom:5px;
    text-align:left;
    padding-left:40px;
    width: 91%;
                  }

    .dlimage.withMap  {
    margin-left:20px;
    margin-top:25px;
    padding-top:20px;
    padding-bottom:10px;
    margin-bottom:25px;
                      }

    .dlimg.bis  {
    width: 94%;
                }

    .regionwithinbbox.dlimage.bis {
    margin-top:25px;
    margin-bottom:15px;
    padding-right:30px;
    padding-bottom:25px;
                                  }

    body[resolution="irids:SOURCES:Features:Political:World:secondOrder_GAUL:ds"] #notgridbox {
    display: none;
    }
    body[resolution="irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds"] #notgridbox {
    display: none;
    }
    body[resolution="irids:SOURCES:Features:Political:World:Countries_GAUL:ds"] #notgridbox {
    display: none;
    }

  </style>
</head>

<body xml:lang="es">
  <form name="pageform" id="pageform">
  <input class="carryLanguage carryup carry" name="Set-Language" type="hidden" />
  <input class="carry dlimg dlimgloc share dlimglocclick admin" name="bbox" type="hidden" />
  <input class="carry dlimg dlimgts share" name="season" type="hidden" data-default="Jan-Mar" />
  <input class="carry dlimg dlimgts share" name="yearStart" type="hidden" data-default="1991" />
  <input class="carry dlimg dlimgts share" name="yearEnd" type="hidden" />
  <input class="carry share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
  <input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
  <input class="pickarea share admin bodyAttribute" name="resolution" type="hidden" data-default="irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds" />
  <input class="dlimg share" name="tercile" type="hidden" data-default="Cold" />
  <input class="dlimg share" name="ensoState" type="hidden" data-default="LaNina" />
  <input class="notused" name="plotaxislength" type="hidden" />

  <input class="dlimg share" name="layers" value="proba" checked="checked" type="checkbox" />
  <input class="dlimg share" name="layers" value="countries" checked="checked" type="checkbox" />
  <input class="dlimg share" name="layers" value="firstadmin" type="checkbox" />
  <input class="dlimg share" name="layers" value="secondadmin" type="checkbox" />
</form>

<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Clima y Salud</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/Health/Regional/Africa/Malaria/Historical.html">Malaria</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Historical_Analysis"><span property="term:label">Malaria Endémica</span></legend>
            </fieldset>

  <fieldset class="navitem"><legend>Temporada</legend><span class="selectvalue"></span><select class="pageformcopy" name="season"><option value="Jan-Mar">Ene-Mar</option><option value="Feb-Apr">Feb-Abr</option><option value="Mar-May">Mar-May</option><option value="Apr-Jun">Abr-Jun</option><option value="May-Jul">May-Jul</option><option value="Jun-Aug">Jun-Ago</option><option value="Jul-Sep">Jul-Sep</option><option value="Aug-Oct">Ago-Oct</option><option value="Sep-Nov">Sep-Nov</option><option value="Oct-Dec">Oct-Dic</option><option value="Nov-Jan">Nov-Ene</option><option value="Dec-Feb">Dic-Feb</option></select>
          para años desde
        <input class="pageformcopy" name="yearStart" type="text" value="1991" size="4" maxlength="4"/> hacia
        <input class="pageformcopy" name="yearEnd" type="text" size="4" maxlength="4"/>
  </fieldset>

  <fieldset class="navitem"><legend>Tercil</legend><span class="selectvalue"></span><select class="pageformcopy" name="tercile"><option value="Cold">Frío</option><option value="Normal">Normal</option><option value="Hot">Caliente</option></select></fieldset>

  <fieldset class="navitem"><legend>Fase ENSO</legend><span class="selectvalue"></span><select class="pageformcopy" name="ensoState"><option value="LaNina">La Niña</option><option value="Neutral">Neutral</option><option value="ElNino">El Niño</option></select></fieldset>

            <fieldset class="navitem"><legend>Promedio espacial por</legend><span class="selectvalue"></span>
          <select class="pageformcopy" name="resolution">
            <option value="0.05">punto de grilla</option>
            <option value="irids:SOURCES:Features:Political:World:secondOrder_GAUL:ds">2ndo nivel admin.</option>
            <option value="irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds">1o nivel admin.</option>
            <option value="irids:SOURCES:Features:Political:World:Countries_GAUL:ds">País</option>
          </select>
          <link class="admin" rel="iridl:hasJSON" href="/expert/(irids:SOURCES:Features:Political:World:firstOrder_GAUL:ds)//resolution/parameter/dup/(%3A)/search/%7Bpop/pop/pop/geoobject/(bb%3A-20%3A-40%3A55%3A40%3Abb)//bbox/parameter/geoobject/labelgeoIdintersects%7D%7Bpop/c%3A/exch/%3Ac//name//label/def/%28irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds%29//region/parameter/geoobject/geoId//region/renameGRID/.region/addGRID%7Difelse/info.json" />
          <select class="pageformcopy" name="region">
            <optgroup class="template" label="Label">
              <option class="iridl:values region@value label"></option>
            </optgroup>
          </select>

        </fieldset>

</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Descripción</a></li>
      <li><a href="#tabs-2" >Documentación</a></li>
      <li><a href="#tabs-3" >Instrucciones</a></li>
      <li><a href="#tabs-4" >Contáctanos</a></li>
    </ul>

<fieldset class="regionwithinbbox dlimage bis" about="">
<img class="dlimgloc" style="display: inline-block; float: left;" src="/SOURCES/.WORLDBATH/.bath/X/Y/(bb%3A-20%3A-40%3A55%3A40%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds)//region/parameter/geoobject/2/copy/rasterize/Y/cosd/mul/X/1/index/[X/Y]weighted-average/exch/Y/exch/[X/Y]weighted-average/SOURCES/.Features/.Political/.World/a%3A/.secondOrder_GAUL/.the_geom/%3Aa%3A/.Countries_GAUL/.the_geom/%3Aa/5/-3/roll/X/Y/fig-/lightgrey/mask/grey/stroke/black/stroke/red/fill/red/smallpushpins/-fig//antialias/true/psdef//plotaxislength/120/psdef//plotborder/8/psdef/+.gif" />
		<div style="display: inline-block; float: left;">
  <div class="valid">
    <a class="dlimgts" rel="iridl:hasJSON" href="/expert/%28irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds%29//region/parameter/geoobject/info.json"></a>
    <div class="template" align="center">Observations para <span class="bold iridl:long_name"></span>
    </div>
  </div>
            <div class="valid" id="notgridbox">
    <a class="dlimgts" rel="iridl:hasJSON" href="/expert/(irids%3ASOURCES%3AFeatures%3APolitical%3AWorld%3AsecondOrder_GAUL%3Ads)/geoobject/%28irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds%29//region/parameter/geoobject/labelgeoIdintersects/region/first/VALUE/region/removeGRID/info.json"></a>
    <div class="template">localizado en o cerca <span class="bold iridl:value"></span></div>
  </div>
  </div>
  <br />
  <br/>
  <br/>

<img class="dlimgts bis1" rel="iridl:hasFigureImage" src="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/%28irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Tercil%20Temp%29/def/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/T//yearStart/get_parameter//yearEnd/get_parameter/RANGE/%5Bsst%5Ddominant_class//long_name/%28Fase%20ENSO%29/def/startcolormap/DATA/1/3/RANGE/blue/blue/blue/grey/red/red/endcolormap/%7Btmp/tercile/nino34%7D/ds//name//ENSO_prob_temp/def/a-/.tmp//long_name/%28temperatura%20cerca%20superficie%29def/-a-/.nino34/-a-/.tercile//fullname/%28Tercil%20Temp%29/def/a:/percentile/last/VALUE/:a:/percentile/first/VALUE/:a/-a/T//long_name%28Tiempo%29def/fig-/colorbars2/medium/solid/green/line/black/line/-fig//antialias/true/def//XOVY/null/psdef/+.gif" />
<img class="dlimgts bis2" rel="iridl:hasFigureImage" src="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/%28irids:SOURCES:Features:Political:World:firstOrder_GAUL:gid@1:ds%29//region/parameter/geoobject/%5BX/Y%5D0.0/weighted-average/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/DATA/AUTO/AUTO/RANGE/dup/%5BT%5D0.33333/0.66667/0/replacebypercentile/1/index/0.0/mul/add//fullname/%28Tercil%20Temp%29/def/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/T//season/get_parameter/VALUES/T//yearStart/get_parameter//yearEnd/get_parameter/RANGE/%5Bsst%5Ddominant_class//long_name/%28Fase%20ENSO%29/def/startcolormap/DATA/1/3/RANGE/blue/blue/blue/grey/red/red/endcolormap/%7Btmp/tercile/nino34%7D/ds//name//ENSO_prob_temp/def/a-/.tmp//long_name/%28temperatura%20cerca%20superficie%29def/-a-/.nino34/-a-/.tercile//fullname/%28Tercil%20Temp%29/def/a:/percentile/last/VALUE/:a:/percentile/first/VALUE/:a/-a/T//long_name%28Tiempo%29def/fig-/colorbars2/medium/solid/green/line/black/line/-fig//antialias/true/def//XOVY/null/psdef/+.auxfig/+.gif" />

</fieldset>

<fieldset class="dlimage withMap">
<a rel="iridl:hasFigure" href="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale/tmp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE//name//proba/def//long_name/%28probabilidad%29/def//fullname//proba/def/a-/-a/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL//name//secondadmin/def/.the_geom/:a:/.firstOrder_GAUL//name//firstadmin/def/.the_geom/:a:/.Countries_GAUL//name//countries/def/.the_geom/:a/X//long_name%28Longitud%29def/Y//long_name%28Latitud%29def/fig-/colors/grey/thin/stroke/stroke/black/thinnish/stroke/-fig//antialias/true/psdef//layers%5B//proba//countries%5Dpsdef/">visit site</a>
<img class="dlimg bis" rel="iridl:hasFigureImage" src="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale/tmp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE//name//proba/def//long_name/%28probabilidad%29/def//fullname//proba/def/a-/-a/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL//name//secondadmin/def/.the_geom/:a:/.firstOrder_GAUL//name//firstadmin/def/.the_geom/:a:/.Countries_GAUL//name//countries/def/.the_geom/:a/X//long_name%28Longitud%29def/Y//long_name%28Latitud%29def/fig-/colors/grey/thin/stroke/stroke/black/thinnish/stroke/-fig//antialias/true/psdef//layers%5B//proba//countries%5Dpsdef/+.gif" />
<img class="dlauximg bis" rel="iridl:hasFigureImage" src="/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/X/-20/55/RANGE/Y/-40/40/RANGE/T/%281991%29//yearStart/parameter/last/cvsunits/4/4/getinterval//yearEnd/parameter/RANGE/T/%28Jan-Mar%29//season/parameter/seasonalAverage/%5BT%5Dpercentileover/%7BCold/0.33333/Normal/0.66667/Hot%7Dclassify/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/zlev/removeGRID/X/-170/-120/RANGE/Y/-5/5/RANGEEDGES/dup/T/12.0/splitstreamgrid/dup/T2/(1856)/last/RANGE/T2/30.0/12.0/mul/runningAverage/T2/12.0/5.0/mul/STEP/%5BT2%5DregridLB/nip/T2/12/pad1/T/unsplitstreamgrid/sub/%7BY/cosd%7D%5BX/Y%5Dweighted-average/T/3/1.0/runningAverage/%7BLaNina/-0.45/Neutral/0.45/ElNino%7Dclassify/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/5/flagge/T/-2/1/2/shiftdata/%5BT_lag%5Dsum/1.0/flagge/dup/a%3A/sst/(LaNina)/VALUE/%3Aa%3A/sst/(ElNino)/VALUE/%3Aa/add/1/maskge/dataflag/1/index/2/flagge/add/sst/(phil)/unitmatrix/sst_out/(Neutral)/VALUE/mul/exch/sst/(phil2)/unitmatrix/sst_out/(LaNina)/(ElNino)/VALUES/%5Bsst_out%5Dsum/mul/add/1/masklt/mul/%5BT%5Daverage/tercileclassesscale/tmp//Tercile/renameGRID/sst//ENSO/renameGRID/DATA/0/1/RANGE//name//proba/def//long_name/%28probabilidad%29/def//fullname//proba/def/a-/-a/ENSO/%28LaNina%29//ensoState/parameter/VALUE/Tercile/%28Cold%29//tercile/parameter/VALUE/SOURCES/.Features/.Political/.World/a:/.secondOrder_GAUL//name//secondadmin/def/.the_geom/:a:/.firstOrder_GAUL//name//firstadmin/def/.the_geom/:a:/.Countries_GAUL//name//countries/def/.the_geom/:a/X//long_name%28Longitud%29def/Y//long_name%28Latitud%29def/fig-/colors/grey/thin/stroke/stroke/black/thinnish/stroke/-fig//antialias/true/psdef//layers%5B//proba//countries%5Dpsdef/+.auxfig/+.gif" />
</fieldset>

<div id="tabs-1" class="ui-tabs-panel" about="">
  <h2 class="titre1" property="term:title" >Probabilidades de Condiciones Calientes o Frías según la Fase de ENSO</h2>

    <p property="term:description">Este mapa muestra la probablidad de encontrar condiciones calientes, normales o frías en la historia de las temperatura, en África, según la Fase de ENSO (El Niño, La Niña o Normal) durante la <i>misma temporada</i>.
  </p>

  <p>El NINO3.4 índice de anomalías de temperatura del mar superficial, en cuanto a normales de 30 años actualizadas cada cinco años, es un promedio espacial sobre la región 5°S a 5°N y 170°W a 120°W. Una temporada es definida como El Niño (La Niña) si hace parte de a lo menos 5 temporadas consecutivas traslapadas donde NINO3.4 es mas de 0.5˚C (menos de -0.5˚C). El analisis reproduce, utilizando la misma bade de datos de TSM, la <a href="http://www.cpc.ncep.noaa.gov/products/analysis_monitoring/ensostuff/ensoyears.shtml">definición siguiente de la NOAA</a>.
    </p>

  <p>
Se puede utilizar los controles en la parte superior de la página para seleccionar el periodo de tres meses de interés, la fase del ENSO y la condición (caliente, normal o fría). También permite visualizar la temperatura anual de un punto de interés, con indicación de los años 'El Niño', 'La Niña' y los años normales.
  </p>

  <p> Referencias para las definiciones de las fases de ENSO: <i> V. E. Kousky and R. W. Higgins, 2007: An Alert Classification System for Monitoring and Assessing the ENSO Cycle. Wea. Forecasting, 22, 353–371.
  doi: http://dx.doi.org/10.1175/WAF987.1</i>
  </p>

</div>

<div id="tabs-2" class="ui-tabs-panel">
  <h2  class="titre1">Documentación</h2>
  <dl class="datasetdocumentation">Temperatura cerca de la superficie de <a href="http://iridl.ldeo.columbia.edu/SOURCES/.UEA/.CRU/.TS4p05/.monthly/.tmp/">CRU TS4.05</a> con una resolución espacial de 0.5˚ x 0.5˚ (~50 km).
</dl>
  <dl class="datasetdocumentation"><a href="http://iridl.ldeo.columbia.edu/SOURCES/.NOAA/.NCDC/.ERSST/.version5/.sst/">Temperatura superficial del mar</a> reconstruida y extendida.
</dl>
</div>

<div id="tabs-3" class="ui-tabs-panel">
  <h2  class="titre1">Instrucciones</h2>
  <p style="padding-right:25px;">
    <div class="buttonInstructions bis"></div>
  </p>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
  <h2 class="titre1">Servicios de Asistencia</h2>
  <p>Contact <a href="mailto:help@iri.columbia.edu?subject=Probabilidades de Condiciones de temperatura según la Fase de ENSO" target="_blank;">help@iri.columbia.edu</a> si tienen alguna pregunta técnica o problema con este  Mapa.
  </p>
</div>
</div>
<br/>

<div class="optionsBar">
    <fieldset class="navitem" id="share"><legend>Compartir</legend></fieldset>
</div>

</body>
</html>
