{
"@context": {
"bb": "http://iridl.ldeo.columbia.edu/ontologies/gisuri/geobb/",
"irigaz": "http://iridl.ldeo.columbia.edu/ontologies/irigaz_frwk.owl#",
"term": "http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#",
"irigaz:id": { "@type": "@id" },
"irigaz:hasPart": { "@container": "@list", "@type": "irigaz:GazEntity"},
"term:label": {"@language": "en"}
},
"irigaz:hasPart": [
{ "irigaz:id": "bb:-79.5:20.5:-70:28:bb",
  "term:label": "Bahamas"
},
{ "irigaz:id": "bb:-90:15.6:-87.5:19:bb",
  "term:label": "Belice"
},
{ "irigaz:id": "bb:-79:-5:-67:14:bb",
  "term:label": "Colombia"
},
{ "irigaz:id": "bb:-86:7.5:-82.5:11.3:bb",
  "term:label": "Costa Rica"
},
{ "irigaz:id": "bb:-85:19.5:-73.5:23.5:bb",
  "term:label": "Cuba"
},
{ "irigaz:id": "bb:-128:24:-67:49:bb",
  "term:label": "Estados Unidos"
},
{ "irigaz:id": "bb:-93:13.5:-88:18:bb",
  "term:label": "Guatemala"
},
{ "irigaz:id": "bb:-75:17:-71.6:20.2:bb",
  "term:label": "Haití"
},
{ "irigaz:id": "bb:-89.5:12.6:-83:16:bb",
  "term:label": "Honduras"
},
{ "irigaz:id": "bb:-80:17:-75:19:bb",
  "term:label": "Jamaica"
},
{ "irigaz:id": "bb:-117.5:15:-86:33:bb",
  "term:label": "México"
},
{ "irigaz:id": "bb:-88:10.5:-83:15:bb",
  "term:label": "Nicaragua"
},
{ "irigaz:id": "bb:-83.5:4:-76:12:bb",
  "term:label": "Panamá"
},
{ "irigaz:id": "bb:-68:17:-65:19:bb",
  "term:label": "Puerto Rico"
},
{ "irigaz:id": "bb:-72:17:-68:20.2:bb",
  "term:label": "República Dominicana"
},
{ "irigaz:id": "bb:-90.5:12.6:-87.5:15:bb",
  "term:label": "El Salvador"
},
{ "irigaz:id": "bb:-62:10:-60.8:11:bb",
  "term:label": "Trinidad y Tobago"
},
{ "irigaz:id": "bb:-74:0:-55:13:bb",
  "term:label": "Venezuela"
}
]
}
