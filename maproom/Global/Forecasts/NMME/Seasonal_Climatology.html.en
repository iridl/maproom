<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://iridl.ldeo.columbia.edu/ontologies/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
      >
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0;" />
<title>NMME Hindcast Seasonal Climatologies</title>
<script type="text/javascript" src="/uicore/uicore.js"></script>
<script type="text/javascript" src="/localconfig/ui.js"></script>

<link rel="stylesheet" type="text/css" href="/uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="/localconfig/ui.css" /> 
<!-- <link class="altLanguage" rel="alternate" hreflang="sp" href="Climatology.html?Set-Language=en" /> -->
<link rel="canonical" href="Seasonal_Climatology.html" />
<link class="carryLanguage" rel="home" href="http://iri.columbia.edu/" title="IRI" />
<!-- <link rel="shortcut icon" href="/localconfig/icons/icpac.png" /> -->
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Entry_Id" content="Global_Forecast_NMME_Seasonal_Climatology" />
<meta property="maproom:Sort_Id" content="b04" />

<link rel="iridl:hasGlossary" href="/dochelp/definitions/index.html" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_term" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#maproom_term"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_NMME_Forecasts_term" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#seasonal"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Gridded"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#deg1.0x1.0"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Global"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Surface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#PlanetarySurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#WaterSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#SeaSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#LandSurface"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#temperature"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climatology" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#Precipitation"/>
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#precipitation_rate" />
  <link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#forecast"/>

<link rel="term:icon" href="/SOURCES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110%7D/if/%5B/(.)/(tref)//var/parameter/%5Dconcat/interp/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(prec)/eq/%7Bprecip_colors%7Dif/S//long_name/(Forecast%20Start%20Time)/def/pop/X/Y/(bb%3A-180%3A-90%3A180%3A90%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210/.T/%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110/.T%7D/if/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(sst)/eq/%7B/X/Y/fig%3A/colors/white/plotlabel/land/black/coasts_gaz/countries_gaz/%3Afig/%7D/%7B/X/Y/fig%3A/colors/plotlabel/black/coasts_gaz/countries_gaz/%3Afig/%7D/ifelse//S/first/plotvalue//plotborder/0/psdef//plotaxislength/220/psdef/(antialias)/true/psdef/(framelabel)/(Forecast%20Climatology%20for%20%25%3D%5BT%5D%20for%20Forecast%20Issued%20%25%3D%5BS%5D%20from%20)//model/get_parameter/append/psdef/+gif" />

<!-- 
<style>
.timescaleseasonal #monleadMenu {
display: none;
}
.timescalemonthly #seasleadMenu {
display: none;
}
</style>
-->

</head>
<body xml:lang="en">
<form name="pageform" id="pageform" class="carryup carry dlimg dlauximg dlimgts dlimgloc share">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="dlimg dlauximg dlimg2 share dlimgloc dlimglocclick" name="bbox" type="hidden" />
<input class="share dlimgts dlimgloc dlimglocclick" name="region" type="hidden" />
<input class="transformRegion dlimglocclick" name="clickpt" type="hidden" />
<input class="unused" name="plotaxislength" type="hidden" data-default="432" />
<input class="pickarea" name="resolution" type="hidden" data-default="1.0" />
<!-- <input class="carry dlimg dlauximg share bodyClass" name="timescale" type="hidden" data-default="seasonal" /> -->
<input class="dlimg share starttime" name="S" type="hidden" />
<!-- <input class="carry dlimg share leadtimeM" name="leadM" type="hidden" data-default="0.5" /> -->
<input class="carry dlimg share leadtimeS" name="leadS" type="hidden" data-default="1.5" />
<input class="carry dlimg share" name="model" type="hidden" data-default="CFSv2" />
<input class="carry dlimg dlauximg share" name="var" type="hidden" data-default="tref" />
</form>

<div class="controlBar">
           <fieldset class="navitem" id="toSectionList">
                <legend>Climate</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/Global/Forecasts/">Forecasts</a>
            </fieldset>
            <fieldset class="navitem" id="chooseSection">
                <legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Global_NMME_Forecasts_term"><span property="term:label">NMME Models</span></legend>
            </fieldset>

        <fieldset class="navitem">
          <legend>Region</legend>
          <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/globalregions.json"></a>
          <select class="RegionMenu" name="bbox">
            <option value="">Global</option>
            <optgroup class="template" label="Region">
              <option class="irigaz:hasPart irigaz:id@value term:label"></option>
            </optgroup>
          </select>
        </fieldset>


<!--
           <fieldset class="navitem valid">
             <legend>Timescale</legend>
             <select class="pageformcopy" name="timescale">
             <option value="seasonal">Seasonal</option>
             <option value="monthly">Monthly</option>
             </select>
           </fieldset>
-->

           <fieldset class="navitem" id="modelMenu">
            <legend>Model</legend>
            <select class="pageformcopy" name="model">
            <option value="CFSv2">CFSv2</option>
            <option value="CMC1">CMC1</option>
            <option value="CMC2">CMC2</option>
            <option value="CCSM3">CCSM3</option>
            <option value="CCSM4">CCSM4</option>
            <option value="GFDL_CM2p1">GFDL-CM2p1</option>
            <option value="GFDL_CM2p5_FLOR_A06">GFDL-CM2p5-FLOR-A06</option>
            <option value="GFDL_CM2p5_FLOR_B01">GFDL-CM2p5-FLOR-B01</option>
            <option value="NASA_GMAO">NASA-GMAO</option>
            </select>
           </fieldset>

           <fieldset class="navitem">
            <legend>Variable</legend>
            <select class="pageformcopy" name="var">
            <option value="tref">Temperature</option>
            <option value="sst">SST</option>
            <option value="prec">Precipitation</option>
            </select>
           </fieldset>

           <fieldset class="navitem valid" id="seasleadMenu">
            <legend>Seasonal Lead Time</legend>
            <select class="pageformcopy" name="leadS">
            <option value="1.5">Seasonal Lead 1</option>
            <option value="2.5">Seasonal Lead 2</option>
            <option value="3.5">Seasonal Lead 3</option>
            <option value="4.5">Seasonal Lead 4</option>
            <option value="5.5">Seasonal Lead 5</option>
            <option value="6.5">Seasonal Lead 6</option>
            <option value="7.5">Seasonal Lead 7</option>
            </select>
           </fieldset>

<!--
           <fieldset class="navitem valid" id="monleadMenu">
            <legend>Monthly Lead Time</legend>
            <select class="pageformcopy" name="leadM">
            <option value="0.5">0.5 Months</option>
            <option value="1.5">1.5 Months</option>
            <option value="2.5">2.5 Months</option>
            <option value="3.5">3.5 Months</option>
            <option value="4.5">4.5 Months</option>
            <option value="5.5">5.5 Months</option>
            <option value="6.5">6.5 Months</option>
            <option value="7.5">7.5 Months</option>
            <option value="8.5">8.5 Months</option>
            </select>
           </fieldset>
-->
</div>

<div class="ui-tabs">
    <ul class="ui-tabs-nav">
      <li><a href="#tabs-1" >Description</a></li>
<!--      <li><a href="#tabs-2" >Dataset Documentation</a></li> -->
      <li><a href="/SOURCES/.Models/.NMME/">Dataset</a></li>
      <li><a href="#tabs-3" >Instructions</a></li>
      <li><a href="#tabs-4" >Contact Us</a></li>
    </ul>

<fieldset class="dlimage">

<a rel="iridl:hasFigure" href="/SOURCES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110%7D/if/%5B/(.)/(tref)//var/parameter/%5Dconcat/interp/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(prec)/eq/%7Bprecip_colors%7Dif/S//long_name/(Forecast%20Start%20Time)/def/pop/X/Y/(bb%3A-180%3A-90%3A180%3A90%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210/.T/%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110/.T%7D/if/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(sst)/eq/%7B/X/Y/fig%3A/colors/white/plotlabel/land/black/coasts_gaz/countries_gaz/%3Afig/%7D/%7B/X/Y/fig%3A/colors/plotlabel/black/coasts_gaz/countries_gaz/%3Afig/%7D/ifelse//S/first/plotvalue//plotaxislength/550/psdef/(antialias)/true/psdef/(framelabel)/(Forecast%20Climatology%20for%20%25%3D%5BT%5D%20for%20Forecast%20Issued%20%25%3D%5BS%5D%20from%20)//model/get_parameter/append/psdef/"></a>

<img class="dlimg" src="/SOURCES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110%7D/if/%5B/(.)/(tref)//var/parameter/%5Dconcat/interp/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(prec)/eq/%7Bprecip_colors%7Dif/S//long_name/(Forecast%20Start%20Time)/def/pop/X/Y/(bb%3A-180%3A-90%3A180%3A90%3Abb)//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/(CFSv2)//model/parameter/(CFSv2)/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210/.T/%7D/if/(CFSv2)//model/parameter/(CMC1)/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CMC2)/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110/.T/%7D/if/(CFSv2)//model/parameter/(CCSM3)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(CCSM4)/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p1)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_A06)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(GFDL_CM2p5_FLOR_B01)/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110/.T%7D/if/(CFSv2)//model/parameter/(NASA_GMAO)/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110/.T%7D/if/L/1.5//leadS/parameter/VALUE/(tref)//var/parameter/(sst)/eq/%7B/X/Y/fig%3A/colors/white/plotlabel/land/black/coasts_gaz/countries_gaz/%3Afig/%7D/%7B/X/Y/fig%3A/colors/plotlabel/black/coasts_gaz/countries_gaz/%3Afig/%7D/ifelse//S/first/plotvalue//plotaxislength/550/psdef/(antialias)/true/psdef/(framelabel)/(Forecast%20Climatology%20for%20%25%3D%5BT%5D%20for%20Forecast%20Issued%20%25%3D%5BS%5D%20from%20)//model/get_parameter/append/psdef/+gif" border="0" />

<img class="dlauximg" src="/SOURCES/%28CFSv2%29//model/parameter/%28CFSv2%29/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210%7D/if/%28CFSv2%29//model/parameter/%28CMC1%29/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110%7D/if/%28CFSv2%29//model/parameter/%28CMC2%29/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110%7D/if/%28CFSv2%29//model/parameter/%28CCSM3%29/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210%7D/if/%28CFSv2%29//model/parameter/%28CCSM4%29/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p1%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p5_FLOR_A06%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p5_FLOR_B01%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110%7D/if/%28CFSv2%29//model/parameter/%28NASA_GMAO%29/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110%7D/if/%5B/%28.%29/%28tref%29//var/parameter/%5Dconcat/interp/L/1.5//leadS/parameter/VALUE/%28tref%29//var/parameter/%28prec%29/eq/%7Bprecip_colors%7Dif/S//long_name/%28Forecast%20Start%20Time%29/def/pop/X/Y/%28bb:-180:-90:180:90:bb%29//bbox/parameter/geoobject/geoboundingbox/BOXEDGES/%28CFSv2%29//model/parameter/%28CFSv2%29/eq/%7BSOURCES/.Models/.NMME/.NCEP-CFSv2/.HINDCAST/.PENTAD_SAMPLES/.sc8210/.T/%7D/if/%28CFSv2%29//model/parameter/%28CMC1%29/eq/%7BSOURCES/.Models/.NMME/.CMC1-CanCM3/.HINDCAST/.sc8110/.T/%7D/if/%28CFSv2%29//model/parameter/%28CMC2%29/eq/%7BSOURCES/.Models/.NMME/.CMC2-CanCM4/.HINDCAST/.sc8110/.T/%7D/if/%28CFSv2%29//model/parameter/%28CCSM3%29/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM3/.sc8210/.T%7D/if/%28CFSv2%29//model/parameter/%28CCSM4%29/eq/%7BSOURCES/.Models/.NMME/.COLA-RSMAS-CCSM4/.sc8210/.T%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p1%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p1-aer04/.sc8210/.T%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p5_FLOR_A06%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-A06/.sc8110/.T%7D/if/%28CFSv2%29//model/parameter/%28GFDL_CM2p5_FLOR_B01%29/eq/%7BSOURCES/.Models/.NMME/.GFDL-CM2p5-FLOR-B01/.sc8110/.T%7D/if/%28CFSv2%29//model/parameter/%28NASA_GMAO%29/eq/%7BSOURCES/.Models/.NMME/.NASA-GMAO-062012/.sc8110/.T%7D/if/L/1.5//leadS/parameter/VALUE/%28tref%29//var/parameter/%28sst%29/eq/%7B/X/Y/fig:/colors/white/plotlabel/land/black/coasts_gaz/countries_gaz/:fig/%7D/%7B/X/Y/fig:/colors/plotlabel/black/coasts_gaz/countries_gaz/:fig/%7D/ifelse//S/first/plotvalue//plotaxislength/550/psdef/%28antialias%29/true/psdef/%28framelabel%29/%28Forecast%20Climatology%20for%20%25=%5BT%5D%20for%20Forecast%20Issued%20%25=%5BS%5D%20from%20%29//model/get_parameter/append/psdef/L/last/plotvalue+.auxfig/+.gif" />


</fieldset>


 <div id="tabs-1" class="ui-tabs-panel" about="">

 <h2 align="center" property="term:title">NMME Hindcast Seasonal Climatology</h2>

  <p align="left" property="term:description">
These maps display climatological values of forecast 2-meter temperature, sea surface temperature, and precipitation at multiple leads and start times during the year for a selection of climate models.  The climatological base period is 1982-2010 for CFSv2, CCSM3, CCSM4, and GFDL-CM2p1 and 1981-2010 for CMC1, CMC2, GFDL-CM2p5-FLOR-A06, GFDL-CM2p5-FLOR-B01, and NASA-GMAO.
</p> 
  <p align="left">
Use the drop-down menus at the top of the page to select the model, variable, and seasonal lead.  Mouse over the map to select the forecast start time from a control that appears just above the map.  Select a combination of the forecast start time and the 3-month seasonal lead time to produce a map for a target season of the year.  The forecast starts occur at the beginning of a month of the year, and adding together the forecast start time and the lead time (3-month seasonal lead) determines the season for which the forecast is valid. </p>
 <p align="left">  For example, for a seasonal forecast, the combination of a forecast start time of 0000 1 Sep and a 3-month seasonal lead of 1.5 months (the first seasonal lead) will produce a forecast map valid for the September-November 3-month season.  The combination of a forecast start time of 0000 1 Sep and a 3-month seasonal lead of 2.5 months (the second seasonal lead) will produce a forecast map valid for the October-December season.  The climatological forecast target time (climatological season -- the displayed year of 1960 or 1961 is not relevant -- see the climatological base periods above) and start time will appear at the bottom of the map.
  </p>
<h4>Acknowledgments</h4>
  <p align="left">
In order to document NMME-Phase II data impact and enable continuing support, users of NMME data are expected to acknowledge NMME data and the participating modeling groups. The NMME model output should be referred to as "the NMME System Phase II data [https://www.earthsystemgrid.org/search.html?Project=NMME]." In publications, users should include a table (referred to below as Table XX) listing the models and institutions that provided model output used in the NMME-Phase II system, as well as the digital object identifier of publications documenting the models, where "Table XX" in the paper should list the models and modeling groups that provided the NMME data. In addition, an acknowledgement similar to the following should be included in any publication: "We acknowledge the agencies that support the NMME-Phase II system, and we thank the climate modeling groups (Environment Canada, NASA, NCAR, NOAA/GFDL, NOAA/NCEP, and University of Miami) for producing and making available their model output. NOAA/NCEP, NOAA/CTB, and NOAA/CPO jointly provided coordinating support and led development of the NMME-Phase II system." Besides the above acknowledgement, users should register any journal articles (or other scientific documents) that are based on NMME-Phase II results.
  </p>
<h4>References</h4>
  <p align="left">
Kirtman, Ben P., and Coauthors, 2014: The North American Multimodel Ensemble: Phase-1 seasonal-to-interannual prediction; Phase-2 toward developing intraseasonal prediction. Bull. Amer. Meteor. Soc., 95, 585–601. doi: http://dx.doi.org/10.1175/BAMS-D-12-00050.1
  </p>
</div>
<div class="ui-tabs-panel-hidden">
<h2 align="center">Dataset</h2>
<p>
<a href="/SOURCES/.Models/.NMME/">Access the dataset used to create this map.</a>
</p>
</div>
<div id="tabs-3" class="ui-tabs-panel">
<h2  align="center">How to use this interactive map</h2>
<p><i>Return to the menu page:</i>
Click the blue link called “Portal” at the top left corner of the page.</p>

<div class="buttonInstructions"></div>
</div>

<div id="tabs-4"  class="ui-tabs-panel">
<h2  align="center">Helpdesks</h2>
<p>
Contact <a href="mailto:help@iri.columbia.edu?subject=NMME Hindcast Seasonal Climatology">help@iri.columbia.edu</a> with any technical questions or problems with this Map Room.
 </p>
</div>
</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend></fieldset>
   <fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>
 </html>

