For instructions on compiling and testing maprooms, see `maproom_base <https://bitbucket.org/iridl/maproom_base/src/master/README.md>`_.

To convert a page into jade you may use html2jade as follows::

	html2jade --noemptypipe <foo.html >foo.jade.m
	html2jade --noemptypipe <boo.xhtml >boo.xjade.m


	
